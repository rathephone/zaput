<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Не найдена страница");
?>
    <style>
        .breadcrumbs, .breadcrumbs + h2.caption  {
            display: none;
        }
    </style>
    <div class="section__container container">
        <div class="not-found not-found_full"><span class="not-found__title">Ой!</span>
            <div class="not-found__content not-found__content_right">
                <div class="not-found__text">
                    <p>Похоже, мы не можем  найти нужную вам страницу</p>
                </div><span class="not-found__code">Код ошибки: 404</span>
            </div>
        </div>
    </div>
<?

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>