<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
$request = \Bitrix\Main\Context::getCurrent()->getRequest();
$xml = new \sletatru\XmlGate([
    'login' => 'sletat@6468822.ru',
    'password' => 'xml@sletattest2019zp03',
]);
if($_REQUEST["SourceId"] && $_REQUEST["OfferId"] && $_REQUEST["RequestId"]):
    $SourceId=(int)$_REQUEST["SourceId"];
    $OfferId=$_REQUEST["OfferId"];
    $requestId=(int)$_REQUEST["RequestId"];
    $resToursHotel = $xml->ActualizePrice($SourceId, $OfferId, $requestId);
    $hotel=$resToursHotel["TourInfo"];
    $RandomNumber=$resToursHotel["RandomNumber"];
endif;
if ($_REQUEST["oldprice"]>0) $hotel["RawPrice"] = intval($_REQUEST["oldprice"]);
else $hotel["RawPrice"] = $hotel["Price"];
$hotel = GetDiscount($hotel);

if ($request->get("action") == "create") {
    $turistObj = array();
    function GetDateReq($day, $mounthStr, $year, $format)
    {
        $mounth = "";
        switch (mb_convert_case($mounthStr, MB_CASE_UPPER)) {
            case "ЯНВАРЬ":
                $mounth = '01';
                break;
            case "ФЕВРАЛЬ":
                $mounth = '02';
                break;
            case "МАРТ":
                $mounth = '03';
                break;
            case "АПРЕЛЬ":
                $mounth = '04';
                break;
            case "МАЙ":
                $mounth = '05';
                break;
            case "ИЮНЬ":
                $mounth = '06';
                break;
            case "ИЮЛЬ":
                $mounth = '07';
                break;
            case "АВГУСТ":
                $mounth = '08';
                break;
            case "СЕНТЯБРЬ":
                $mounth = '09';
                break;
            case "ОКТЯБРЬ":
                $mounth = '10';
                break;
            case "НОЯБРЬ":
                $mounth = '11';
                break;
            case "ДЕКАБРЬ":
                $mounth = '12';
                break;
        }
        return date($format, strtotime($year . "-" . $mounth . "-" . $day));
    }
    $addIblock = array();
    foreach ($request->get('DATA') as $type => $dataType) {
        foreach ($dataType as $turist) {
            if ($type == "PEOPLE") {
                $turist["TYPE"] = "Взрослый";
            } else {
                $turist["TYPE"] = "Ребенок";
            }
            $addIblock[] = serialize($turist);
            $turistObj[] = (object)array(
                'BirthDate' => GetDateReq($turist['DAY_BIRTHDAY'], $turist['MOUNTH_BIRTHDAY'], $turist['YEAR_BIRTHDAY'], "Y-m-d"),
                'Citizenship' => $turist['COUNTRY'],
                'City' => 'СПБ',
                'Country' => $turist['COUNTRY'],
                'DateOfIssue' => GetDateReq($turist['DAY_VID'], $turist['MOUNTH_VID'], $turist['YEAR_VID'], "c"),
                'Expires' => GetDateReq($turist['DAY_OKONCH'], $turist['MOUNTH_OKONCH'], $turist['YEAR_OKONCH'], "c"),
                'FirstName' => $turist['NAME'],
                'PassportNumber' => $turist['NUMBER_PASSPORT'],
                'PassportSeries' => $turist['SERIA_PASSPORT'],
                'Patronymic' => 'V',
                'Phone' => $turist['SERIA_PASSPORT'],
                'Surname' => $turist['LAST_NAME']
            );
        }
    }
    $searchParam = array(
        'request' => array(
            'WorkflowType' => 'TwoStepsHolding',
            'Comments' => $hotel["discpuntName"],
            'Customer' => (object)array(
                'Address' => $request->get('COUNTRY') . " " . $request->get('PASSPORT_ADDRESS'),
                'Email' => $request->get('EMAIL'),
                'FullName' => $request->get('NAME') . " " . $request->get('SECOND_NAME') . " " . $request->get('LAST_NAME'),
                'Passport' => $request->get('NUMBER_PASSPORT') . " " . $request->get('SERIA_PASSPORT'),
                'Phone' => $request->get('PHONE'),
                'IssuedBy' => $request->get('PASSPORT_VID'),
            ),
            'InitialURL' => 'http://zapytevky.bitseo.ru',
            'OfferId' => $request->get('OfferId'),
            'RequestId' => $request->get('RequestId'),
            'SourceId' => $request->get('SourceId'),
            'Actualize' => false,
            'PriceFromCustomer' => $hotel["NewPrice"],
            'DiscountAmount' => '0',
            'DiscountPercentage' => '0',
            'Tourists' => array(
                'Tourist' => $turistObj
            )
        )
    );
    $soapClient = new SoapClient('https://claims.sletat.ru/xmlgate.svc?wsdl');

    $soapClient->__setSoapHeaders(new SoapHeader("urn:SletatRu:DataTypes:AuthData:v1", "AuthInfo", array(
        "Login" => 'sletat@6468822.ru',
        "Password" => 'xml@sletattest2019zp03'
    )));

    $result = $soapClient->CreateClaim($searchParam);
    if ($result->CreateClaimResult->OperationStatus) {
        \Bitrix\Main\Loader::includeModule("iblock");
        $objOrder = new CIBlockElement();
        if ($request->get('SEX') == "male") {
            $gender = 14;
        } else {
            $gender = 15;
        }
        $objOrder->Add(array(
            "NAME" => "Заказ №" . $result->CreateClaimResult->OrderIdentity,
            "IBLOCK_ID" => 17,
            "PROPERTY_VALUES" => array(
                "TUR" => $RandomNumber." (OfferId=".$request->get('OfferId') . " RequestId=" . $request->get('RequestId') . " SourceId=" . $request->get('SourceId').")",
                "FIO" => $request->get('NAME') . " " . $request->get('SECOND_NAME') . " " . $request->get('LAST_NAME'),
                "BIRTHDAY" => $request->get('DATE_BIRTHDAY') . " " . $request->get('MOUNTH_BIRTHDAY') . " " . $request->get('YEAR_BIRTHDAY'),
                "GENDER" => $gender,
                "NATIONALITY" => $request->get('COUNTRY'),
                "PASPORT" => $request->get('NUMBER_PASSPORT') . " " . $request->get('SERIA_PASSPORT'),
                "ADDRESS" => $request->get('PASSPORT_ADDRESS'),
                "EMAIL" => $request->get('EMAIL'),
                "PHONE" => $request->get('PHONE'),
                "FOREIGHN_PASSPORT" => $addIblock
            )
        ));
    }
    echo json_encode($result->CreateClaimResult->ClaimIdentity);
}
elseif($request->get("action") == "getorder"){
    $getClaimInfoParam = array(
        'request' => array(
            'ClaimIdentity' => $request->get("claim")
        )
    );

    $soapClient = new SoapClient('https://claims.sletat.ru/xmlgate.svc?wsdl');

    $soapClient->__setSoapHeaders(
        new SoapHeader("urn:SletatRu:DataTypes:AuthData:v1", "AuthInfo",
            array(
                "Login" => 'sletat@6468822.ru',
                "Password" => 'xml@sletattest2019zp03'
            )));
    $result = $soapClient->UpdateClaimToWasSentState(array('request'=>array('ClaimIdentity' => $request->get("claim"), 'WasSentDate' => date("c"))));
    $result = $soapClient->UpdateClaimToWasViewedState(array('request'=>array('ClaimIdentity' => $request->get("claim"), 'WasViewedDate' => date("c"))));
    $result = $soapClient->getClaimInfo($getClaimInfoParam);
    echo $result->GetClaimInfoResult->RedirectToPaymentURL;
}