<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Официальный сайт турфирмы в Санкт-Петербурге с удобным поиском туров по всем туроператорам");
$APPLICATION->SetPageProperty("keywords", "туристическая фирма, туристические услуги, бронирование туров, подбор тура, онлайн оплата тура");
$APPLICATION->SetPageProperty("title", "Онлайн оплата туров на сайте «ЗаПутевкой.рф»");
$APPLICATION->SetTitle("Оплата онлайн");
?>
    <div class="section__container container container_flex">
        <div class="section__block section__block_main-online">
            <?$hotel = NULL?>
            <?require_once "include/tour.php" ?>
            <? if ($hotel != NULL): ?>
                <ul class="path" data-step="0">
                    <li class="path__step">
                        <div class="path__text"><span>Данные плательщика</span>
                        </div>
                    </li>
                    <li class="path__step">
                        <div class="path__text"><span>Сведения о туристах</span>
                        </div>
                    </li>
                    <li class="path__step">
                        <div class="path__text"><span>Условия оферты</span>
                        </div>
                    </li>
                    <li class="path__step">
                        <div class="path__text"><span>Оплата</span>
                        </div>
                    </li>
                </ul>
                <div class="block block_online block_tabs">
                    <div class="block__tabs">
                        <div class="tabs" data-tabs="online">
                            <ul class="tabs__list">
                                <li class="tabs__item" data-tab="0"><span>Данные плательщика</span>
                                </li>
                                <li class="tabs__item" data-tab="1"><span>Инструкция он-лайн оплаты тура</span>
                                </li>
                                <li class="tabs__item" data-tab="2"><span>Образцы документов</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="block__tabs-content" data-tabs-content="online">
                        <div data-tab="0">
                            <form class="js_form_rder" onsubmit="return false;">
                                <input type="hidden" name="city" value="<?=$hotel["SysCityFromId"]?>">
                                <input type="hidden" name="country" value="<?=$hotel["SysCountryId"]?>">
                                <input type="hidden" name="hotel" value="<?=$hotel["SysHotelId"]?>">
                                <input type="hidden" name="touroperator" value="<?=$hotel["SourceId"]?>">
                                <div class="step-content step-content_current" data-step-content="0">
                                    <?require_once "include/step1.php" ?>
                                </div>
                                <div class="step-content" data-step-content="1">
                                    <?require_once "include/step2.php" ?>
                                </div>
                                <div class="step-content" data-step-content="2">
                                    <?require_once "include/step3.php" ?>
                                </div>
                                <div class="step-content" id="order_result" data-step-content="finish">

                                </div>
                            </form>
                        </div>
                        <div data-tab="1">
                            <h3 class="caption">Инструкция он-лайн оплаты тура
                            </h3>
                        </div>
                        <div data-tab="2">
                            <h3 class="caption">Образцы документов
                            </h3>
                        </div>
                    </div>
                </div>
                <ul class="path" data-step="0">
                    <li class="path__step">
                        <div class="path__text"><span>Данные плательщика</span>
                        </div>
                    </li>
                    <li class="path__step">
                        <div class="path__text"><span>Сведения о туристах</span>
                        </div>
                    </li>
                    <li class="path__step">
                        <div class="path__text"><span>Условия оферты</span>
                        </div>
                    </li>
                    <li class="path__step">
                        <div class="path__text"><span>Оплата</span>
                        </div>
                    </li>
                </ul>

            <?endif;?>
        </div>
        <div class="section__block section__block_sidebar">
            <div class="sidebar">
                <div class="sidebar__header sidebar__header_ico"><i class="ico">
                        <svg>
                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-info"></use>
                        </svg>
                    </i><span class="caption caption_size-s">Уважаемые клиенты</span>
                </div>
                <div class="sidebar__content sidebar__content_info">
                    <div class="sidebar__text">
                        <p>
                            Обращаем Ваше внимание, что наша компания не несет ответственности за неверно
                            предоставленные паспортные данные. В случае ошибки в одной букве или цифре в
                            указанных данных после оплаты тура Вам могут отказать в: выдаче виз, посадке на рейс,
                            трансфере, регистрации в отеле и других составляющих тура. Просим быть внимательными
                            при введении данных. Также обращаем Ваше внимание, что к стоимости туристского
                            продукта могут быть добавлены топливные сборы или доплаты за авиабилеты.
                            Окончательную стоимость туристского продукта Вы можете уточнить у наших
                            специалистов по телефону перед оформлением заявки. Спасибо за использование
                            нашего сервиса он-лайн оплаты.
                        </p>
                    </div>
                    <div class="sidebar__image"><img src="<?= SITE_TEMPLATE_PATH ?>/img/payment.jpg"
                                                     alt="Способы оплаты"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>