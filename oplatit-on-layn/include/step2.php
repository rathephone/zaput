<?
/**
 * @var $hotel array
 */
?>
<div class="step">
    <div class="step__state"><strong>2</strong><span>шаг</span>
    </div>
    <div class="step__title">
        <h3 class="caption">Заполните данные о туристах из загранспаспортов
        </h3>
        <p>все данные вводятся так, как указано в паспорте</p>
    </div>
</div>
<div class="form form_online" data-step-num="2">
    <? for ($people = 1; $people <= $hotel["Adults"]; $people++): ?>
        <div class="form__line-caption">
            <span><?= $people ?> взрослый</span>
        </div>
        <div class="form__line">
            <label class="field field_size-full"><span class="field__caption">Фамилия - Латиницей</span>
                <div class="field__input"><input type="text" name="DATA[PEOPLE][<?=$people?>][LAST_NAME]" class="required"/></div>
            </label>
        </div>
        <div class="form__line">
            <label class="field field_size-full"><span
                        class="field__caption">Имя - Латиницей</span>
                <div class="field__input"><input type="text" name="DATA[PEOPLE][<?=$people?>][NAME]" class="required"/>
                </div>
            </label>
        </div>
        <div class="form__line form__line_triple">
            <label class="field"><span class="field__caption">Дата рождения</span><span class="select select_field">
                      <select name="DATA[PEOPLE][<?=$people?>][DAY_BIRTHDAY]" class="required">
                        <option disabled="disabled" selected="selected">День</option>
                          <? for ($i = 1; $i <= 31; $i++): ?>
                              <option value="<?= $i ?>"><?= $i ?></option>
                          <? endfor; ?>
                      </select></span>
            </label>
            <label class="field"><span class="select select_field">
                      <select name="DATA[PEOPLE][<?=$people?>][MOUNTH_BIRTHDAY]" class="required">
                        <option disabled="disabled" selected="selected">Месяц</option>
                            <option>Январь</option>
                          <option>Февраль</option>
                          <option>Март</option>
                          <option>Апрель</option>
                          <option>Май</option>
                          <option>Июнь</option>
                          <option>Июль</option>
                          <option>Август</option>
                          <option>Сентябрь</option>
                          <option>Октябрь</option>
                          <option>Ноябрь</option>
                          <option>Декабрь</option>
                      </select></span>
            </label>
            <label class="field"><span class="select select_field">
                      <select name="DATA[PEOPLE][<?=$people?>][YEAR_BIRTHDAY]" class="required">
                        <option disabled="disabled" selected="selected">Год</option>
                          <? for ($i = (date("Y") - 18); $i >= (date("Y") - 80); $i--): ?>
                              <option value="<?= $i ?>"><?= $i ?></option>
                          <? endfor; ?>
                      </select></span>
            </label>
        </div>
        <div class="form__line">
            <div class="field"><span class="field__caption">Пол</span>
                <div class="field__input field__input_checkboxes">
                    <label class="checkbox">
                        <input type="radio" name="DATA[PEOPLE][<?=$people?>][SEX]" checked="checked" value="male"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>Мужской
                            </p>
                        </div>
                    </label>
                    <label class="checkbox">
                        <input type="radio" name="DATA[PEOPLE][<?=$people?>][SEX]" value="female"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>Женский
                            </p>
                        </div>
                    </label>
                </div>
            </div>
        </div>
        <div class="form__line form__line_double">
            <label class="field"><span class="field__caption">Гражданство</span><span class="select select_field select_full">
                      <select name="DATA[PEOPLE][<?=$people?>][COUNTRY]" class="required">
                        <option>Российская Федерация</option>
                        <option>Украина</option>
                        <option>США</option>
                      </select></span>
            </label>
            <label class="field"><span class="field__caption">Номер загранпаспорта</span><span
                        class="field__inputs">
                      <div class="field__input field__input_size-s"><input type="text" name="DATA[PEOPLE][<?=$people?>][NUMBER_PASSPORT]" class="required"/>
                      </div>
                      <div class="field__input field__input_size-m"><input type="text" name="DATA[PEOPLE][<?=$people?>][SERIA_PASSPORT]" class="required"/>
                      </div></span>
            </label>
        </div>
        <div class="form__line form__line_triple">
            <label class="field"><span class="field__caption">Дата выдачи</span><span class="select select_field">
                      <select name="DATA[PEOPLE][<?=$people?>][DAY_VID]" class="required">
                        <option disabled="disabled" selected="selected">День</option>
                          <? for ($i = 1; $i <= 31; $i++): ?>
                              <option value="<?= $i ?>"><?= $i ?></option>
                          <? endfor; ?>
                      </select></span>
            </label>
            <label class="field"><span class="select select_field">
                      <select name="DATA[PEOPLE][<?=$people?>][MOUNTH_VID]" class="required">
                        <option disabled="disabled" selected="selected">Месяц</option>
                            <option>Январь</option>
                          <option>Февраль</option>
                          <option>Март</option>
                          <option>Апрель</option>
                          <option>Май</option>
                          <option>Июнь</option>
                          <option>Июль</option>
                          <option>Август</option>
                          <option>Сентябрь</option>
                          <option>Октябрь</option>
                          <option>Ноябрь</option>
                          <option>Декабрь</option>
                      </select></span>
            </label>
            <label class="field"><span class="select select_field">
                      <select name="DATA[PEOPLE][<?=$people?>][YEAR_VID]" class="required">
                        <option disabled="disabled" selected="selected">Год</option>
                          <? for ($i = (date("Y")); $i >= (date("Y") - 10); $i--): ?>
                              <option value="<?= $i ?>"><?= $i ?></option>
                          <? endfor; ?>
                      </select></span>
            </label>
        </div>
        <div class="form__line form__line_triple">
            <label class="field"><span class="field__caption">Действителен до</span><span class="select select_field">
                      <select name="DATA[PEOPLE][<?=$people?>][DAY_OKONCH]" class="required">
                        <option disabled="disabled" selected="selected">День</option>
                          <? for ($i = 1; $i <= 31; $i++): ?>
                              <option value="<?= $i ?>"><?= $i ?></option>
                          <? endfor; ?>
                      </select></span>
            </label>
            <label class="field"><span class="select select_field">
                      <select name="DATA[PEOPLE][<?=$people?>][MOUNTH_OKONCH]" class="required">
                        <option disabled="disabled" selected="selected">Месяц</option>
                            <option>Январь</option>
                          <option>Февраль</option>
                          <option>Март</option>
                          <option>Апрель</option>
                          <option>Май</option>
                          <option>Июнь</option>
                          <option>Июль</option>
                          <option>Август</option>
                          <option>Сентябрь</option>
                          <option>Октябрь</option>
                          <option>Ноябрь</option>
                          <option>Декабрь</option>
                      </select></span>
            </label>
            <label class="field"><span class="select select_field">
                      <select name="DATA[PEOPLE][<?=$people?>][YEAR_OKONCH]" class="required">
                        <option disabled="disabled" selected="selected">Год</option>
                          <? for ($i = (date("Y")); $i <= (date("Y")+10); $i++): ?>
                              <option value="<?= $i ?>"><?= $i ?></option>
                          <? endfor; ?>
                      </select></span>
            </label>
        </div>
        <div class="form__line">
            <label class="field field_size-full"><span class="field__caption">Номер моб. телефона</span>
                <div class="field__input"><input type="tel" name="DATA[PEOPLE][<?=$people?>][PHONE]" class="required" placeholder="+7 ( ___ ) - ___ - __ - __"/>
                </div>
            </label>
        </div>
    <? endfor; ?>
    <? if ($hotel["Kids"] > 0): ?>
        <? for ($kids = 1; $kids <= $hotel["Kids"]; $kids++): ?>
            <div class="form__line-caption">
                <span><?= $kids ?> ребенок</span>
            </div>
            <div class="form__line">
                <label class="field field_size-full"><span class="field__caption">Фамилия - Латиницей</span>
                    <div class="field__input"><input type="text" name="DATA[CHILD][<?=$kids?>][LAST_NAME]" class="required"/></div>
                </label>
            </div>
            <div class="form__line">
                <label class="field field_size-full"><span
                            class="field__caption">Имя - Латиницей</span>
                    <div class="field__input"><input type="text" name="DATA[CHILD][<?=$kids?>][NAME]" class="required"/>
                    </div>
                </label>
            </div>
            <div class="form__line form__line_triple">
                <label class="field"><span class="field__caption">Дата рождения</span><span class="select select_field">
                      <select name="DATA[CHILD][<?=$kids?>][DAY_BIRTHDAY]" class="required">
                        <option disabled="disabled" selected="selected">День</option>
                          <? for ($i = 1; $i <= 31; $i++): ?>
                              <option value="<?= $i ?>"><?= $i ?></option>
                          <? endfor; ?>
                      </select></span>
                </label>
                <label class="field"><span class="select select_field">
                      <select name="DATA[CHILD][<?=$kids?>][MOUNTH_BIRTHDAY]" class="required">
                        <option disabled="disabled" selected="selected">Месяц</option>
                            <option>Январь</option>
                          <option>Февраль</option>
                          <option>Март</option>
                          <option>Апрель</option>
                          <option>Май</option>
                          <option>Июнь</option>
                          <option>Июль</option>
                          <option>Август</option>
                          <option>Сентябрь</option>
                          <option>Октябрь</option>
                          <option>Ноябрь</option>
                          <option>Декабрь</option>
                      </select></span>
                </label>
                <label class="field"><span class="select select_field">
                      <select name="DATA[CHILD][<?=$kids?>][YEAR_BIRTHDAY]" class="required">
                        <option disabled="disabled" selected="selected">Год</option>
                          <? for ($i = (date("Y")); $i >= (date("Y") - 18); $i--): ?>
                              <option value="<?= $i ?>"><?= $i ?></option>
                          <? endfor; ?>
                      </select></span>
                </label>
            </div>
            <div class="form__line">
                <div class="field"><span class="field__caption">Пол</span>
                    <div class="field__input field__input_checkboxes">
                        <label class="checkbox">
                            <input type="radio" name="DATA[CHILD][<?=$kids?>][SEX]" checked="checked" value="male"/>
                            <div class="checkbox__box">
                            </div>
                            <div class="checkbox__content">
                                <p>Мужской
                                </p>
                            </div>
                        </label>
                        <label class="checkbox">
                            <input type="radio" name="DATA[CHILD][<?=$kids?>][SEX]" value="female"/>
                            <div class="checkbox__box">
                            </div>
                            <div class="checkbox__content">
                                <p>Женский
                                </p>
                            </div>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form__line form__line_double">
                <label class="field"><span class="field__caption">Гражданство</span><span class="select select_field select_full">
                      <select name="DATA[CHILD][<?=$kids?>][COUNTRY]" class="required">
                        <option>Российская Федерация</option>
                        <option>Украина</option>
                        <option>США</option>
                      </select></span>
                </label>
                <label class="field"><span class="field__caption">Номер загранпаспорта</span><span
                            class="field__inputs">
                      <div class="field__input field__input_size-s"><input type="text" name="DATA[CHILD][<?=$kids?>][NUMBER_PASSPORT]" class="required"/>
                      </div>
                      <div class="field__input field__input_size-m"><input type="text" name="DATA[CHILD][<?=$kids?>][SERIA_PASSPORT]" class="required"/>
                      </div></span>
                </label>
            </div>
            <div class="form__line form__line_triple">
                <label class="field"><span class="field__caption">Дата выдачи</span><span class="select select_field">
                      <select name="DATA[CHILD][<?=$kids?>][DAY_VID]" class="required">
                        <option disabled="disabled" selected="selected">День</option>
                          <? for ($i = 1; $i <= 31; $i++): ?>
                              <option value="<?= $i ?>"><?= $i ?></option>
                          <? endfor; ?>
                      </select></span>
                </label>
                <label class="field"><span class="select select_field">
                      <select name="DATA[CHILD][<?=$kids?>][MOUNTH_VID]" class="required">
                        <option disabled="disabled" selected="selected">Месяц</option>
                            <option>Январь</option>
                          <option>Февраль</option>
                          <option>Март</option>
                          <option>Апрель</option>
                          <option>Май</option>
                          <option>Июнь</option>
                          <option>Июль</option>
                          <option>Август</option>
                          <option>Сентябрь</option>
                          <option>Октябрь</option>
                          <option>Ноябрь</option>
                          <option>Декабрь</option>
                      </select></span>
                </label>
                <label class="field"><span class="select select_field">
                      <select name="DATA[CHILD][<?=$kids?>][YEAR_VID]" class="required">
                        <option disabled="disabled" selected="selected">Год</option>
                          <? for ($i = (date("Y")); $i >= (date("Y") - 10); $i--): ?>
                              <option value="<?= $i ?>"><?= $i ?></option>
                          <? endfor; ?>
                      </select></span>
                </label>
            </div>
            <div class="form__line form__line_triple">
                <label class="field"><span class="field__caption">Действителен до</span><span class="select select_field">
                      <select name="DATA[CHILD][<?=$kids?>][DAY_OKONCH]" class="required">
                        <option disabled="disabled" selected="selected">День</option>
                          <? for ($i = 1; $i <= 31; $i++): ?>
                              <option value="<?= $i ?>"><?= $i ?></option>
                          <? endfor; ?>
                      </select></span>
                </label>
                <label class="field"><span class="select select_field">
                      <select name="DATA[CHILD][<?=$kids?>][MOUNTH_OKONCH]" class="required">
                        <option disabled="disabled" selected="selected">Месяц</option>
                            <option>Январь</option>
                          <option>Февраль</option>
                          <option>Март</option>
                          <option>Апрель</option>
                          <option>Май</option>
                          <option>Июнь</option>
                          <option>Июль</option>
                          <option>Август</option>
                          <option>Сентябрь</option>
                          <option>Октябрь</option>
                          <option>Ноябрь</option>
                          <option>Декабрь</option>
                      </select></span>
                </label>
                <label class="field"><span class="select select_field">
                      <select name="DATA[CHILD][<?=$kids?>][YEAR_OKONCH]" class="required">
                        <option disabled="disabled" selected="selected">Год</option>
                          <? for ($i = (date("Y")); $i <= (date("Y")+10); $i++): ?>
                              <option value="<?= $i ?>"><?= $i ?></option>
                          <? endfor; ?>
                      </select></span>
                </label>
            </div>
            <div class="form__line">
                <label class="field field_size-full"><span class="field__caption">Номер моб. телефона</span>
                    <div class="field__input"><input type="tel" name="DATA[CHILD][<?=$kids?>][PHONE]" class="required" placeholder="+7 ( ___ ) - ___ - __ - __"/>
                    </div>
                </label>
            </div>
        <? endfor; ?>
    <? endif; ?>
    <div class="form__submit form__submit_flex">
        <button class="button button_highlight button_size-wide" data-step-button="prev" onclick="CheckOrder(1,this)"><span>Предыдущий шаг</span>
        </button>
        <button class="button button_highlight button_size-wide" data-step-button="next" onclick="CheckOrder(2,this)"><span>Следующий шаг</span>
        </button>
    </div>
</div>