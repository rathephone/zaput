<?
$xml = new \sletatru\XmlGate([
    'login' => 'sletat@6468822.ru',
    'password' => 'xml@sletattest2019zp03',
]);
if ($_REQUEST["SourceId"] > 0 && $_REQUEST["OfferId"] > 0 && $_REQUEST["RequestId"] > 0 && $_REQUEST["hot"] == "N") {
    $SourceId = (int)$_REQUEST["SourceId"];//
    $OfferId = $_REQUEST["OfferId"];//
    $requestId = (int)$_REQUEST["RequestId"];//
    $resToursHotel = $xml->ActualizePrice($SourceId, $OfferId, $requestId);
    $hotel = $resToursHotel["TourInfo"];
    $RandomNumber=$resToursHotel["RandomNumber"];
    if ($_REQUEST["oldprice"]>0) $hotel["RawPrice"] = intval($_REQUEST["oldprice"]);
    else $hotel["RawPrice"] = $hotel["Price"];
    $hotel = GetDiscount($hotel);
}
else{
    $hotel = NULL;
}
?>
<? if ($hotel == NULL): ?>
    <div class="block block_ticket block_online"><span class="caption caption_strong">Внимание: тур не выбран!</span>
        <p>Для он-лайн оплаты Вам необходимо подобрать тур на нашем сайте.</p><a class="button button_highlight" href="/search-tour/">Подобрать тур</a>
    </div>
<? else: ?>
    <div class="block block_ticket block_online block_card">
        <div class="card card_ticket">
            <div class="card__header">
                <div class="card__block"><span class="caption caption_size-s"><?= $hotel["HotelName"] ?></span>
                    <? if (in_array($hotel["SysStarName"], array("1*", "2*", "3*", "4*", "5*"))): ?>
                        <div class="rating rating_no-more">
                            <ul class="rating__list" data-value="3">
                                <? for ($i = 0; $i < intval($hotel["SysStarName"]); $i++): ?>
                                    <li class="rating__star">
                                        <svg>
                                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                        </svg>
                                    </li>
                                <? endfor; ?>
                            </ul>
                        </div>
                    <? endif; ?>
                </div>
                <div class="card__block">
                    <p>Номер тура: <?=$RandomNumber?></p>
                    <input type="hidden" name="RandomNumber" value="<?=$RandomNumber?>" />
                </div>
            </div>
            <div class="card__main">
                <div class="card__image">
                    <? if ($hotel['HotelPhotosCount']): ?>
                        <img src="https://hotels.sletat.ru/i/f/<?= $hotel["SysHotelId"] ?>_0.jpg"/>
                    <? endif; ?>
                </div>
                <div class="card__info">
                    <ul class="list">
                        <li class="list__item"><i class="ico">
                                <svg>
                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-plain"></use>
                                </svg>
                            </i><span><?= $hotel["CityFromName"] ?></span>
                        </li>
                        <li class="list__item"><i class="ico">
                                <svg>
                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-calendar"></use>
                                </svg>
                            </i><span><?= date("d.m", strtotime($hotel["CheckIn"])) ?> - <?= date("d.m", strtotime($hotel["CheckOut"])) ?> <?= $hotel["Nights"] ?> (ночей)</span>
                        </li>
                        <li class="list__item"><i class="ico">
                                <svg>
                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-place"></use>
                                </svg>
                            </i><span><?= $hotel["CountryName"] ?>, <?= $hotel["ResortName"] ?></span>
                        </li>
                        <li class="list__item"><i class="ico">
                                <svg>
                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-kitchen"></use>
                                </svg>
                            </i><span><?= $hotel["MealName"] ?></span>
                        </li>
                        <li class="list__item"><i class="ico">
                                <svg>
                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-hotel"></use>
                                </svg>
                            </i><span><?= $hotel["HotelName"] ?></span>
                        </li>
                        <li class="list__item"><i class="ico">
                                <svg>
                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-people"></use>
                                </svg>
                            </i><span> <? if ($hotel["Adults"] > 0): ?><?= $hotel["Adults"] ?> (взрослых)<? endif; ?><? if ($hotel["Kids"] >0): ?> + <?= $hotel["Kids"] ?><? if ($hotel["Kids"] == 1): ?> (ребенок)<? else: ?> (детей)<? endif; ?><? endif; ?></span>
                        </li>
                    </ul>
                    <div class="price">
                        <div class="price__value"><?=number_format($hotel["NewPrice"],0,"."," ")?> р</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? endif; ?>