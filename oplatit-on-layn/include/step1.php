<div class="step">
    <div class="step__state"><strong>1</strong><span>шаг</span>
    </div>
    <div class="step__title">
        <h3 class="caption">Введите данные гражданского паспорта
        </h3>
        <p>все данные вводятся так, как указано в паспорте</p>
    </div>
</div>
<input type="hidden" name="SourceId" value="<?=$_REQUEST["SourceId"]?>">
<input type="hidden" name="OfferId" value="<?=$_REQUEST["OfferId"]?>">
<input type="hidden" name="RequestId" value="<?=$_REQUEST["RequestId"]?>">
<input type="hidden" name="hot" value="<?=$_REQUEST["hot"]?>">
<div class="form form_online" data-step-num="1">
    <div class="form__line">
        <label class="field field_size-full"><span class="field__caption">Фамилия</span>
            <div class="field__input"><input type="text" name="LAST_NAME" class="required"/></div>
        </label>
    </div>
    <div class="form__line">
        <label class="field field_size-full"><span class="field__caption">Имя</span>
            <div class="field__input"><input type="text" name="NAME" class="required"/>
            </div>
        </label>
    </div>
    <div class="form__line">
        <label class="field field_size-full"><span class="field__caption">Отчество</span>
            <div class="field__input"><input type="text" name="SECOND_NAME" class="required"/>
            </div>
        </label>
    </div>
    <div class="form__line form__line_triple">
        <label class="field"><span class="field__caption">Дата рождения</span><span
                    class="select select_field">
                  <select name="DATE_BIRTHDAY" class="required">
                    <option disabled="disabled" selected="selected">День</option>
                      <? for ($i = 1; $i <= 31; $i++): ?>
                          <option value="<?= $i ?>"><?= $i ?></option>
                      <? endfor; ?>
                  </select></span>
        </label>
        <label class="field"><span class="select select_field">
                  <select  name="MOUNTH_BIRTHDAY" class="required">
                    <option disabled="disabled" selected="selected">Месяц</option>
                        <option>Январь</option>
                      <option>Февраль</option>
                      <option>Март</option>
                      <option>Апрель</option>
                      <option>Май</option>
                      <option>Июнь</option>
                      <option>Июль</option>
                      <option>Август</option>
                      <option>Сентябрь</option>
                      <option>Октябрь</option>
                      <option>Ноябрь</option>
                      <option>Декабрь</option>
                  </select></span>
        </label>
        <label class="field"><span class="select select_field">
                  <select name="YEAR_BIRTHDAY" class="required">
                    <option disabled="disabled" selected="selected">Год</option>
                      <? for ($i = (date("Y") - 18); $i >= (date("Y") - 80); $i--): ?>
                          <option value="<?= $i ?>"><?= $i ?></option>
                      <? endfor; ?>
                  </select></span>
        </label>
    </div>
    <div class="form__line">
        <div class="field"><span class="field__caption">Пол</span>
            <div class="field__input field__input_checkboxes">
                <label class="checkbox">
                    <input type="radio" name="SEX" value="male" checked="checked" class="required"/>
                    <div class="checkbox__box"></div>
                    <div class="checkbox__content"><p>Мужской</p></div>
                </label>
                <label class="checkbox">
                    <input type="radio" name="SEX" value="female" class="required"/>
                    <div class="checkbox__box"></div>
                    <div class="checkbox__content"><p>Женский</p></div>
                </label>
            </div>
        </div>
    </div>
    <div class="form__line form__line_double">
        <label class="field"><span class="field__caption">Гражданство</span><span class="select select_field select_full">
                  <select name="COUNTRY" class="required">
                    <option>Российская Федерация</option>
                    <option>Украина</option>
                    <option>США</option>
                  </select></span>
        </label>
        <label class="field"><span class="field__caption">Номер паспорта</span>
            <span class="field__inputs">
                  <div class="field__input field__input_size-s"><input name="NUMBER_PASSPORT" type="text" class="required"/></div>
                  <div class="field__input field__input_size-m"><input name="SERIA_PASSPORT" type="text" class="required"/></div>
            </span>
        </label>
    </div>
    <div class="form__line">
        <label class="field field_size-full"><span class="field__caption">Кем выдан</span>
            <div class="field__input"><input name="PASSPORT_VID" type="text" class="required"/></div>
        </label>
    </div>
    <div class="form__line">
        <label class="field field_size-full"><span
                    class="field__caption">Адрес по прописке</span>
            <div class="field__input"><input name="PASSPORT_ADDRESS" type="text" class="required"/></div>
        </label>
    </div>
    <div class="form__line form__line_double">
        <label class="field"><span class="field__caption">E-mail</span>
            <div class="field__input"><input name="EMAIL" type="email" class="required"/>
            </div>
        </label>
        <label class="field"><span class="field__caption">Номер моб. телефона</span>
            <div class="field__input"><input name="PHONE" type="tel" class="required" placeholder="+7 ( ___ ) - ___ - __ - __"/></div>
        </label>
    </div>
    <div class="form__submit">
        <button class="button button_highlight button_size-wide" data-step-button="next" onclick="CheckOrder(1,this)">
            <span>Следующий шаг</span>
        </button>
    </div>
</div>