<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
$currentPage = $APPLICATION->GetCurPage();
$currentDir = $APPLICATION->GetCurDir();

use Bitrix\Main\Page\Asset;
//Asset::getInstance()->addString('<script src="https://api-maps.yandex.ru/2.1/?apikey=669dcec3-3a8d-4676-ad47-f1c78ae06db0&amp;lang=ru_RU" type="text/javascript"></script>');
//Asset::getInstance()->addString('<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>');
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/style.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/fix.css");
Asset::getInstance()->addString('<script src="'.SITE_TEMPLATE_PATH .'/js/jquery.min.js"></script>');
Asset::getInstance()->addString('<script src="'.SITE_TEMPLATE_PATH .'/js/libs.js"></script>');
Asset::getInstance()->addString('<script src="'.SITE_TEMPLATE_PATH .'/js/script.js"></script>');
Asset::getInstance()->addString('<script src="'.SITE_TEMPLATE_PATH .'/js/custom.js"></script>');
Asset::getInstance()->addString('<script src="'.SITE_TEMPLATE_PATH .'/js/sletat_module.js"></script>');
Asset::getInstance()->addString('<script src="'.SITE_TEMPLATE_PATH .'/js/jquery.cookie.js"></script>');
Asset::getInstance()->addString('<script src="https://api-maps.yandex.ru/2.1/?apikey=669dcec3-3a8d-4676-ad47-f1c78ae06db0&lang=ru_RU"></script>');

//Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.min.js");
//Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/libs.js");
//Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/script.js");
//Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/custom.js");
//Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/sletat_module.js");
//Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.cookie.js");
//Asset::getInstance()->addJs("https://api-maps.yandex.ru/2.1/?apikey=669dcec3-3a8d-4676-ad47-f1c78ae06db0&lang=ru_RU");
?>
<!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>" style="min-width: 1240px;">
    <head>
		<title><?$APPLICATION->ShowTitle();?></title>
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
        <meta charset="<?=LANG_CHARSET?>">
		
        <?$APPLICATION->ShowHead();?>
	</head>
	<body class="page">
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MQRBMJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MQRBMJ');</script>
<!-- End Google Tag Manager -->

		<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>

        <header class="header" role="banner">
            <div class="header__top">
                <div class="header__container container">
                    <div class="header__block">
                        <h1 class="logotype">
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/include/header_logo.php"
                                )
                            );?>
                        </h1>
                    </div>
                    <div class="header__block">
                        <div class="header__contacts">
                            <span><?$APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "AREA_FILE_SHOW" => "file",
                                        "AREA_FILE_SUFFIX" => "inc",
                                        "EDIT_TEMPLATE" => "",
                                        "PATH" => "/include/header_work.php"
                                    )
                                );?></span>
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:main.include",
                                "",
                                Array(
                                    "AREA_FILE_SHOW" => "file",
                                    "AREA_FILE_SUFFIX" => "inc",
                                    "EDIT_TEMPLATE" => "",
                                    "PATH" => "/include/header_phone.php"
                                )
                            );?>
                        </div>
                    </div>
                    <div class="header__block">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => "/include/socials.php"
                            )
                        );?>
                    </div>
                    <div class="header__block">
                        <button class="button button_highlight" data-modal-open="order"><span>Оставить заявку</span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="header__main">
                <div class="header__container container">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "top_menu",
                        array(
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "left",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "1",
                            "MENU_CACHE_GET_VARS" => array(
                            ),
                            "MENU_CACHE_TIME" => "3600000",
                            "MENU_CACHE_TYPE" => "Y",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "top",
                            "USE_EXT" => "N",
                            "COMPONENT_TEMPLATE" => "top_menu"
                        ),
                        false
                    );?>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "countries_block",
                        array(
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "N",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "N",
                            "DISPLAY_PREVIEW_TEXT" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "FIELD_CODE" => array(
                                0 => "",
                                1 => "",
                            ),
                            "FILTER_NAME" => "",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "IBLOCK_ID" => "14",
                            "IBLOCK_TYPE" => "zapytevky",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "MESSAGE_404" => "",
                            "NEWS_COUNT" => "25",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Новости",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "PROPERTY_CODE" => array(
                                0 => "XML_ID",
                                1 => "",
                            ),
                            "SET_BROWSER_TITLE" => "N",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "N",
                            "SET_META_KEYWORDS" => "N",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "N",
                            "SHOW_404" => "N",
                            "SORT_BY1" => "SORT",
                            "SORT_BY2" => "show_counter",
                            "SORT_ORDER1" => "ASC",
                            "SORT_ORDER2" => "DESC",
                            "STRICT_SECTION_CHECK" => "N",
                            "COMPONENT_TEMPLATE" => "countries_block"
                        ),
                        false
                    );?>
                    <? $page = $APPLICATION->GetCurPage(false);?>
                    <?if($page!="/tour/"):?>
                        <?$APPLICATION->IncludeFile('/include/form_search.php',array(),array())?>
                    <?endif;?>
                </div>
            </div>
        </header>

        <main class="page__content" role="main">

            <?if($currentPage != "/"){?>
            <section class="section">
                <div class="section__container container">
                    <div class="section__header">
                        <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumb", Array(
                            "PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
                            "SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
                            "START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
                        ),
                            false
                        );?>
                        <h2 class="caption"><?=$APPLICATION->ShowTitle(false);?></h2>
                    </div>
                </div>
                <?}?>
