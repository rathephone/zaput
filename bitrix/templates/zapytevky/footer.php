<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
?>
<?if($currentDir != "/"){?>
    </section>
<?}?>
</main>
<footer class="footer">
    <div class="footer__top">
        <div class="footer__container container">
            <div class="footer__block">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "bottom_menu",
                    array(
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "left",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => array(
                        ),
                        "MENU_CACHE_TIME" => "3600000",
                        "MENU_CACHE_TYPE" => "Y",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => "bottom",
                        "USE_EXT" => "N",
                        "COMPONENT_TEMPLATE" => "bottom_menu"
                    ),
                    false
                );?>
            </div>
            <div class="footer__block">
                <?$APPLICATION->IncludeComponent("bitrix:search.form", "footer_search", Array(
                    "PAGE" => "#SITE_DIR#search/index.php",	// Страница выдачи результатов поиска (доступен макрос #SITE_DIR#)
                        "USE_SUGGEST" => "N",	// Показывать подсказку с поисковыми фразами
                    ),
                    false
                );?>
                <button class="button" data-modal-open="claim"><span>Жалоба директору</span>
                </button>
            </div>
        </div>
    </div>
    <div class="footer__main">
        <div class="footer__container container">
            <div class="footer__block">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/include/footer_logo.php"
                    )
                );?>
            </div>
            <div class="footer__block">
                <div class="footer__contacts">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => "/include/footer_phone.php"
                        )
                    );?><span><?$APPLICATION->IncludeComponent(
                            "bitrix:main.include",
                            "",
                            Array(
                                "AREA_FILE_SHOW" => "file",
                                "AREA_FILE_SUFFIX" => "inc",
                                "EDIT_TEMPLATE" => "",
                                "PATH" => "/include/footer_work.php"
                            )
                        );?></span>
                </div>
            </div>
            <div class="footer__block"><span>Мы в соц сетях</span>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:main.include",
                    "",
                    Array(
                        "AREA_FILE_SHOW" => "file",
                        "AREA_FILE_SUFFIX" => "inc",
                        "EDIT_TEMPLATE" => "",
                        "PATH" => "/include/socials.php"
                    )
                );?>
            </div>
            <div class="footer__block">
                <button class="button button_transparent" data-modal-open="order"><span>Оставить заявку</span>
                </button>
                <p><?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => "/include/footer_copyright.php"
                        )
                    );?></p>
            </div>
        </div>
    </div>
</footer>
    <div class="page__modals">
        <?$APPLICATION->IncludeFile('/include/form_feedback.php',array(),array())?>
        <?$APPLICATION->IncludeFile('/include/form_claim.php',array(),array())?>
        <div class="modal modal_gallery" data-modal="gallery">
            <div class="modal__controls">
                <button class="arrow arrow_prev" data-gallery-controls="0">
                    <svg>
                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-arrow"></use>
                    </svg>
                </button>
                <button class="arrow arrow_next" data-gallery-controls="1">
                    <svg>
                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-arrow"></use>
                    </svg>
                </button>
            </div>
            <div class="modal__content">
            </div>
        </div>
        <div class="modal" data-modal="map">
            <button class="modal__close">
            </button>
            <header class="modal__head"><span class="caption caption_size-l js_caption_map">Санкт-Петербург</span>
            </header>
            <div class="modal__content modal__content_map">
            </div>
        </div>
    </div>
    <div class="printSelection"></div>
<!-- BEGIN JIVOSITE CODE -->
<script type='text/javascript'>
(function(){ var widget_id = 'i7hs2fZF4d';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- END JIVOSITE CODE -->
</body>
</html>