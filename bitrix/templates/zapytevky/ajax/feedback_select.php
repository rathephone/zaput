<?php
define("STOP_STATISTICS", true);
define("PUBLIC_AJAX_MODE", true);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
header('Content-Type: application/x-javascript; charset=' . LANG_CHARSET);

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Mail\Event;

$request = Application::getInstance()->getContext()->getRequest();
$user_text = htmlspecialcharsEx($request->get('PREVIEW_TEXT'));
$user_name = $request->get('NAME');
$user_phone = $request->get('PHONE');

$arResult = array();
if (check_bitrix_sessid()) {

    if(strlen($user_text) <= 1000) {

        // bitrix api include
        Loader::includeModule("iblock");
        global $USER;

        // формируем массив с полями для создания заявки в инфоблок "Сомневаетесь в выборе?" id = 13
        $arProperties = [
            "PHONE" => $user_phone,
            "NAME" => $user_name
        ];
        $arFields = Array(
            "MODIFIED_BY" => ($USER->IsAuthorized() ? $USER->GetID() : false),
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID" => IBLOCK_ID_FEEDBACK_SELECT,
            "PROPERTY_VALUES" => $arProperties,
            "NAME" => ($user_name) ? $user_name : ' ',
            "ACTIVE" => "Y",
            "PREVIEW_TEXT" => $user_text
        );

        $el = new CIBlockElement;
        $ID = $el->Add($arFields);
        if ($ID > 0) {
            $arResult['status'] = 'ok';
            $arResult['msg'] = 'Благодарим за пожелание';

            Event::send(array(
                "EVENT_NAME" => "FEEDBACK_FORM",
                "LID" => SITE_ID,
                "C_FIELDS" => array(
                    "AUTHOR" => $user_name,
                    "PHONE" => $user_phone,
                    "TEXT" => $user_text
                ),
                "MESSAGE_ID" => 31
            ));
        } else {
            $arResult['status'] = 'error';
            $arResult['msg'] = str_replace("<br>", "", $el->LAST_ERROR);
        }
    }
    else {
        $arResult['status'] = 'error';
        $arResult['msg'] = 'Максимум 1000 символов текста';
    }
} else {
    $arResult['status'] = 'error';
    $arResult['msg'] = 'Заполните корректные данные';
}
echo json_encode($arResult);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
die();
?>