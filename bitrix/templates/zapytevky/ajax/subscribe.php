<?php
define("STOP_STATISTICS", true);
define("PUBLIC_AJAX_MODE", true);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
header('Content-Type: application/x-javascript; charset=' . LANG_CHARSET);

use Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();
$user_email = htmlspecialcharsEx($request->get('EMAIL'));
$rub_id = $request->get('RUB_ID');

$arResult = array();
if ($user_email && filter_var($user_email, FILTER_VALIDATE_EMAIL) && $rub_id){

    // bitrix api include
    CModule::IncludeModule('subscribe');
    global $USER;
    // запрос всех рубрик
    $rub = CRubric::GetList(
        array("LID"=>"ASC","SORT"=>"ASC","NAME"=>"ASC"),
        array("ACTIVE"=>"Y", "LID"=>LANG)
    );
    /*$arRubIDS = array();
    while ($arRub = $rub->Fetch()){
        $arRubIDS[] = $arRub['ID'];
    }*/

    $thank = "";
    if($rub_id == 1) $thank = "Благодарим за подписку на новости";
    if($rub_id == 2) $thank = "Благодарим за подписку";

    $subscr = new CSubscription;

    if($sub = CSubscription::GetByEmail($user_email)->Fetch()) {
        $str_ID = (integer)$sub["ID"];
        $arRubIDS = [];
        $subscr_rub = CSubscription::GetRubricList($str_ID);
        while($subscr_rub_arr = $subscr_rub->Fetch()) $arRubIDS[] = $subscr_rub_arr["ID"];

        if(in_array($rub_id, $arRubIDS)){
            $arResult['status'] = 'error';
            $arResult['msg'] = 'Адрес подписки уже существует. Пожалуйста, укажите другой адрес.';
        } else {
            $arRubIDS[] = $rub_id;
            // Update
            $arFields = Array(
                "RUB_ID" => $arRubIDS,
                "SEND_CONFIRM" => 'N',
                "CONFIRMED" => "Y"
            );
            $ID = $subscr->Update($str_ID, $arFields);
            if ($ID > 0){
                $arResult['status'] = 'ok';
                $arResult['msg'] = $thank;
            } else {
                $arResult['status'] = 'error';
                $arResult['msg'] = str_replace("<br>","",$subscr->LAST_ERROR);
            }
        }
    } else
        {
            // Add
        $arFields = Array(
            "USER_ID" => ($USER->IsAuthorized() ? $USER->GetID() : false),
            "FORMAT" => "html",
            "EMAIL" => $user_email,
            "ACTIVE" => "Y",
            "RUB_ID" => array($rub_id),
            "SEND_CONFIRM" => 'N',
            "CONFIRMED" => "Y"
        );
        $ID = $subscr->Add($arFields);
        if ($ID > 0){
            $arResult['status'] = 'ok';
            $arResult['msg'] = $thank;
        } else {
            $arResult['status'] = 'error';
            $arResult['msg'] = str_replace("<br>","",$subscr->LAST_ERROR);
        }
    }
} else {
    $arResult['status'] = 'error';
    $arResult['msg'] = 'Некорректный Email';
}
echo json_encode($arResult);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
die();
?>