<?php
define("STOP_STATISTICS", true);
define("PUBLIC_AJAX_MODE", true);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
header('Content-Type: application/x-javascript; charset=' . LANG_CHARSET);

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Mail\Event;

$request = Application::getInstance()->getContext()->getRequest();
$user_email = htmlspecialcharsEx($request->get('EMAIL'));
$user_name = $request->get('NAME');
$user_phone = $request->get('PHONE');
$turDeskript = $request->get('turDeskript');


if ($user_name && $user_phone && check_bitrix_sessid()) {
    $arResult = array();
//if ($user_email && filter_var($user_email, FILTER_VALIDATE_EMAIL)) {

    // bitrix api include
    Loader::includeModule("iblock");
    global $USER;

    // формируем массив с полями для создания заявки в инфоблок "Заказ с сайта" id = 20
    $arProperties = [
        "NAME" => $user_name,
        "EMAIL" => $user_email,
        "PHONE" => $user_phone,
        "DESCRIPT"=>$turDeskript

    ];
    $arFields = Array(
        "MODIFIED_BY" => ($USER->IsAuthorized() ? $USER->GetID() : false),
        "IBLOCK_SECTION_ID" => false,
        "IBLOCK_ID" => IBLOCK_ORDER_ZAKAZ,
        "PROPERTY_VALUES" => $arProperties,
        "NAME" => $user_name . ' ' . $user_email,
        "PREVIEW_TEXT"   => $turDeskript,
        "ACTIVE" => "Y",
    );

    $el = new CIBlockElement;
    $ID = $el->Add($arFields);
    if ($ID > 0) {
        $arResult['status'] = 'ok';
        $arResult['msg'] = 'Благодарим за заявку';

        Event::send(array(
            "EVENT_NAME" => "FEEDBACK_FORM",
            "LID" => "s1",
            "C_FIELDS" => array(
                "AUTHOR" => $user_name,
                "AUTHOR_EMAIL" => $user_email,
                "PHONE" => $user_phone,
                "DESCRIPT"=>$turDeskript
            ),
            "MESSAGE_ID" => 34
        ));
    } else {
        $arResult['status'] = 'error';
        $arResult['msg'] = str_replace("<br>", "", $el->LAST_ERROR);

    }
} else {
    $arResult['status'] = 'error';
    $arResult['msg'] = 'Заполните корректные данные';
}
echo json_encode($arResult);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
die();
?>