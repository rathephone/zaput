<?php
define("STOP_STATISTICS", true);
define("PUBLIC_AJAX_MODE", true);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
header('Content-Type: application/x-javascript; charset=' . LANG_CHARSET);

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Mail\Event;

$request = Application::getInstance()->getContext()->getRequest();
$user_name = $request->get('NAME');
$user_phone = $request->get('PHONE');
$user_list_claim_id = $request->get('TITLE_CLAIM');
$pr_text = htmlspecialcharsEx($request->get('PREVIEW_TEXT'));

$arResult = array();
if (check_bitrix_sessid()) {
    // bitrix api include
    Loader::includeModule("iblock");
    global $USER;

    $claim = "";
    $property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>IBLOCK_ID_CLAIM, "CODE"=>"TITLE_CLAIM", "ID"=>$user_list_claim_id));
    if($enum_fields = $property_enums->GetNext()) $claim = $enum_fields["VALUE"];

    // формируем массив с полями для создания заявки в инфоблок "Жалоба директору" id = 16
    $arProperties = [
        "NAME" => $user_name,
        "PHONE" => $user_phone,
        "TITLE_CLAIM" => $user_list_claim_id
    ];
    $arFields = Array(
        "MODIFIED_BY" => ($USER->IsAuthorized() ? $USER->GetID() : false),
        "IBLOCK_SECTION_ID" => false,
        "IBLOCK_ID" => IBLOCK_ID_CLAIM,
        "PROPERTY_VALUES" => $arProperties,
        "NAME" => ($user_name) ? $user_name : ' ',
        "ACTIVE" => "Y",
        "PREVIEW_TEXT" => $pr_text,
    );

    $el = new CIBlockElement;
    $ID = $el->Add($arFields);
    if ($ID > 0) {
        $arResult['status'] = 'ok';
        $arResult['msg'] = 'Благодарим за обращение';

        Event::send(array(
            "EVENT_NAME" => "FEEDBACK_FORM",
            "LID" => "s1",
            "C_FIELDS" => array(
                "AUTHOR" => $user_name,
                "TITLE_CLAIM" => $claim,
                "TEXT" => $pr_text
            ),
            "MESSAGE_ID" => 32
        ));
    } else {
        $arResult['status'] = 'error';
        $arResult['msg'] = str_replace("<br>", "", $el->LAST_ERROR);
    }
} else {
    $arResult['status'] = 'error';
    $arResult['msg'] = 'Заполните корректные данные';
}
echo json_encode($arResult);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
die();
?>