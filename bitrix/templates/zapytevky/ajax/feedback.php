<?php
define("STOP_STATISTICS", true);
define("PUBLIC_AJAX_MODE", true);

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
header('Content-Type: application/x-javascript; charset=' . LANG_CHARSET);

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Mail\Event;

$request = Application::getInstance()->getContext()->getRequest();
$user_email = htmlspecialcharsEx($request->get('EMAIL'));
$user_name = $request->get('NAME');
$user_phone = $request->get('PHONE');

$arResult = array();
if (check_bitrix_sessid()) {
//if ($user_email && filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
    // bitrix api include
    Loader::includeModule("iblock");
    global $USER;

    // формируем массив с полями для создания заявки в инфоблок "Заказать обратный звонок" id = 8
    $arProperties = [
        "NAME" => $user_name,
        "EMAIL" => $user_email,
        "PHONE" => $user_phone
    ];

    $arFields = Array(
        "MODIFIED_BY" => ($USER->IsAuthorized() ? $USER->GetID() : false),
        "IBLOCK_SECTION_ID" => false,
        "IBLOCK_ID" => IBLOCK_ID_FEEDBACK,
        "PROPERTY_VALUES" => $arProperties,
        "NAME" => ($user_name || $user_email) ? $user_name . ' ' . $user_email : ' ',
        "ACTIVE" => "Y",
    );

    $el = new CIBlockElement;
    $ID = $el->Add($arFields);
    if ($ID > 0) {
        $arResult['status'] = 'ok';
        $arResult['msg'] = 'Благодарим Вас за заявку';

        Event::send(array(
            "EVENT_NAME" => "FEEDBACK_FORM",
            "LID" => "s1",
            "C_FIELDS" => array(
                "AUTHOR" => $user_name,
                "AUTHOR_EMAIL" => $user_email,
                "PHONE" => $user_phone
            ),
            "MESSAGE_ID" => 30
        ));
    } else {
        $arResult['status'] = 'error';
        $arResult['msg'] = str_replace("<br>", "", $el->LAST_ERROR);
    }
} else {
    $arResult['status'] = 'error';
    $arResult['msg'] = 'Заполните корректные данные';
}
echo json_encode($arResult);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
die();
?>