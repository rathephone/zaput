<?php
define("STOP_STATISTICS", true);
define("PUBLIC_AJAX_MODE", true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
header('Content-Type: application/x-javascript; charset=' . LANG_CHARSET);

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
//use Bitrix\Main\Mail\Event;

$request = Application::getInstance()->getContext()->getRequest();

$name = $request->getPost('NAME');
$phone = $request->getPost('PHONE');
$number_of_contract = $request->getPost('NUMBER_OF_CONTRACT');
$city = $request->getPost('CITY');
$country = $request->getPost('COUNTRY');
$resort = $request->getPost('RESORT');
$hotel = $request->getPost('HOTEL');
$pr_text = htmlspecialcharsEx($request->getPost('PREVIEW_TEXT'));

$arResult = array();
if (check_bitrix_sessid()) {
    if(strlen($pr_text) <= 4000) {
        // bitrix api include
        Loader::includeModule("iblock");
        global $USER;

        // формируем массив с полями для создания заявки в инфоблок "Отзывы" id = 9
        $arProperties = [
            "PHONE" => $phone,
            "NUMBER_OF_CONTRACT" => $number_of_contract,
            "CITY" => $city,
            "COUNTRY" => $country,
            "RESORT" => $resort,
            "HOTEL" => $hotel,
            "NAME" => $name
        ];
        $arFields = Array(
            "MODIFIED_BY" => ($USER->IsAuthorized() ? $USER->GetID() : false),
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID" => IBLOCK_ID_REVIEW,
            "PROPERTY_VALUES" => $arProperties,
            "NAME" => ($name) ? $name : ' ',
            "PREVIEW_TEXT" => $pr_text,
            "ACTIVE" => "N",
            "ACTIVE_FROM" => ConvertTimeStamp(time(), "FULL")
        );

        $el = new CIBlockElement;
        $ID = $el->Add($arFields);
        if ($ID > 0) {
            $arResult['status'] = 'ok';
            $arResult['msg'] = 'Благодарим за отзыв';

            /*Event::send(array(
                "EVENT_NAME" => "FEEDBACK_FORM",
                "LID" => "s1",
                "C_FIELDS" => array(
                    "AUTHOR" => $user_name,
                    "AUTHOR_EMAIL" => $user_email,
                    "PHONE" => $user_phone
                ),
                "MESSAGE_ID" => 30
            ));*/
        } else {
            $arResult['status'] = 'error';
            $arResult['msg'] = str_replace("<br>", "", $el->LAST_ERROR);
        }
    } else {
        $arResult['status'] = 'error';
        $arResult['msg'] = 'Максимум 4000 символов текста';
    }
} else {
    $arResult['status'] = 'error';
    $arResult['msg'] = 'Заполните все данные';
}
echo json_encode($arResult);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
die();
?>