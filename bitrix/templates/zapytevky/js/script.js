'use strict';

function _toConsumableArray(arr) {
    if (Array.isArray(arr)) {
        for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
            arr2[i] = arr[i];
        }
        return arr2;
    } else {
        return Array.from(arr);
    }
}

(function (root) {

    // svg for all
    svg4everybody();

    function phoneMask() {
        document.querySelectorAll('input[type="tel"]').forEach(function (input, k) {
            input.addEventListener('input', function (e) {
                //var v = input.value.replace('+7', '').trim();
                var v = input.value.trim();
                if (v == '+' || v == '7' || v == '8') {
                    v = '+7';
                } else {
                    if (v.length == 1 && (v != '+' && v != '7' || v != '8')) {
                        v = '+7' + v;
                    }
                }
                input.value = VMasker.toPattern(v, {pattern: "+9 (999) 999-99-99"});
            });
        });
    }

    phoneMask();

    document.querySelectorAll('a[href^="#"]').forEach(anchor => {
        anchor.addEventListener('click', function (e) {
            e.preventDefault();

            document.querySelector(this.getAttribute('href')).scrollIntoView({
                behavior: 'smooth'
            });
        });
    });

    // slider options
    var sliderOptions = {
        'banner': {
            freeScroll: false,
            cellAlign: 'left',
            contain: true,
            wrapAround: true,
            pageDots: true,
            prevNextButtons: false,
            lazyLoad: true
        },
        'full': {
            freeScroll: false,
            cellAlign: 'left',
            contain: true,
            wrapAround: true,
            pageDots: false,
            prevNextButtons: false,
            adaptiveHeight: true
        },
        'six-items': {
            items: 6,
            freeScroll: false,
            cellAlign: 'left',
            contain: true,
            wrapAround: true,
            pageDots: false,
            prevNextButtons: false,
            adaptiveHeight: true
        },
        'reviews': {
            autoPlay: 3000,
            contain: true,
            wrapAround: true,
            controls: false,
            prevNextButtons: false,
            adaptiveHeight: true
        },
        'gallery': {
            cellAlign: 'left',
            prevNextButtons: false,
            pageDots: false
        },
        'contact': {
            items: 3,
            prevNextButtons: false,
            pageDots: false
        }
    };

    document.querySelectorAll('[data-slider]').forEach(function (slider, i) {
        var slides = slider.querySelector('[data-slider-slides]'),
            slidesCount = slides.children.length,
            slideWidth = slides.children[0].offsetWidth,
            sliderWidth = slider.offsetWidth,
            slidesCapacity = Math.round(sliderWidth / slideWidth),
            controls = slider.querySelector('[data-slider-controls]'),
            controlsPrev = controls.querySelector('[data-slider-controls-prev]'),
            controlsNext = controls.querySelector('[data-slider-controls-next]');
            if (slidesCount > slidesCapacity) {
                var flkty = new Flickity(slides, sliderOptions[slider.dataset.slider]);

                controlsPrev.addEventListener('click', function (e) {
                    e.preventDefault();
                    flkty.previous();
                });

                controlsNext.addEventListener('click', function (e) {
                    e.preventDefault();
                    flkty.next();
                });
            }
            else if(slidesCount>3){

            }
            else {
                controls.remove();
            }
    });
    document.querySelectorAll('[data-slider]').forEach(function (slider, i) {
        var slides = slider.querySelector('[data-slider-slides]');
            controls = slider.querySelector('[data-slider-main-controls]');
        if (controls != null) {
            var controlsPrev = controls.querySelector('[data-slider-controls-prev]'),
                controlsNext = controls.querySelector('[data-slider-controls-next]');
            controlsPrev.addEventListener('click', function (e) {
                e.preventDefault();
                var selectSlider = slides.querySelector('li.gallery__item_selected'),
                    setSlider = selectSlider.previousElementSibling;
                if (setSlider) {
                    slider.querySelector(".gallery__view img").src = setSlider.querySelector("img").src;
                    setSlider.classList.add("gallery__item_selected");
                    selectSlider.classList.remove("gallery__item_selected");
                }

            });

            controlsNext.addEventListener('click', function (e) {
                e.preventDefault();
                var selectSlider = slides.querySelector('li.gallery__item_selected'),
                    setSlider = selectSlider.nextElementSibling;
                if (setSlider) {
                    slider.querySelector(".gallery__view img").src = setSlider.querySelector("img").src;
                    setSlider.classList.add("gallery__item_selected");
                    selectSlider.classList.remove("gallery__item_selected");
                }
            });
        }
    });

    $(document).on('click', '[data-more]', function (e) {
        e.preventDefault();
        $(this).parents('[data-more-action]').toggleClass('show-more');
        var text = $(this).text();
        if (text == "Свернуть") {
            $(this).text("Подробнее");
        } else {
            $(this).text("Свернуть");
            var sliderQuery = $(this).parents(".office").find("[data-slider]");
            setTimeout(function () {
                if (sliderQuery.length){
                    var slider = sliderQuery[0];
                    console.log(slider);
                    var slides = slider.querySelector('[data-slider-slides]'),
                        slidesCount = slides.children.length,
                        slideWidth = slides.children[0].offsetWidth,
                        sliderWidth = slider.offsetWidth,
                        slidesCapacity = Math.round(sliderWidth / slideWidth),
                        controls = slider.querySelector('[data-slider-controls]'),
                        controlsPrev = controls.querySelector('[data-slider-controls-prev]'),
                        controlsNext = controls.querySelector('[data-slider-controls-next]');
                    if (slidesCount > slidesCapacity) {
                        var flkty = new Flickity(slides, sliderOptions[slider.dataset.slider]);

                        controlsPrev.addEventListener('click', function (e) {
                            e.preventDefault();
                            flkty.previous();
                        });

                        controlsNext.addEventListener('click', function (e) {
                            e.preventDefault();
                            flkty.next();
                        });
                    }
                    else {
                        controls.remove();
                    }
                }
            },500)
        }
    });

    document.querySelectorAll('[data-toggle]').forEach(function (el, i) {
        el.addEventListener('click', function (e) {
            e.preventDefault();

            var text = el.dataset.toggle;
            var t = el;

            if (t.tagName == 'BUTTON') {
                var span = t.querySelector('span');
                t.dataset.toggle = t.textContent.trim();
                t = span;
            }

            t.textContent = text;
        });
    });

    document.querySelectorAll('[data-tabs]').forEach(function (tabs, i) {
        var data = tabs.dataset.tabs,
            content = document.querySelector('[data-tabs-content=' + data + ']');

        tabs.querySelectorAll('[data-tab]').forEach(function (tab, k) {
            tab.addEventListener('click', function (e) {
                e.preventDefault();

                if (tab.parentNode.dataset.tabsContent) return;

                var index = tab.dataset.tab,
                    showing = content.querySelector('.showing'),
                    selected = tabs.querySelector('.selected');

                if (showing) showing.classList.remove('showing');
                if (selected) selected.classList.remove('selected');
                tab.classList.add('selected');
                content.querySelector('[data-tab="' + index + '"]').classList.add('showing');
            });
        });
        tabs.querySelector('[data-tab="0"]').click();
    });

    // select
    document.querySelectorAll('select').forEach(function (select, i) {
        if (select.closest('.modal')) {
            return;
        }

        new CustomSelect({
            elem: select
        });
    });

    document.querySelectorAll('[data-drop]').forEach(function (select, i) {
        if (select.querySelector('button.js-Dropdown-title')) {
            select.querySelector('button.js-Dropdown-title').addEventListener('click', function (e) {
                e.preventDefault();

                if ([].concat(_toConsumableArray(select.classList)).includes('select_open')) {
                    select.classList.remove('select_open');
                } else {
                    document.querySelectorAll('.select_open').forEach(function (select, k) {
                        select.classList.remove('select_open');
                    });
                    select.classList.add('select_open');

                    // new SimpleBar(select.querySelector('.select__dropdown'))
                }
            });
        } else {
            select.querySelector('input.js-Dropdown-title').addEventListener('focus', function (e) {
                e.preventDefault();
                select.classList.add('select_open');
            });
        }
    });

    // datepickers
    var calendar = document.querySelector('.calendar');

    if (calendar) {
        var months = calendar.querySelectorAll('.calendar__item .month'),
            controls = calendar.querySelectorAll('[data-calendar-controls]'),
            monthsList = calendar.querySelector('.calendar__months-list').children;

        months.forEach(function (month, i) {
            var now = new Date(),
                date = new Date(now.getFullYear(), now.getMonth() + i);

            var customOptions = {
                rangeFrom: null,
                rangeTo: null
            };

            var datepicker = $(month).datepicker({
                startDate: date,
                selectOtherMonths: !1,
                keyboardNav: !1,
                multipleDatesSeparator: '',
                navTitles: {
                    days: 'MM',
                    months: 'yyyy',
                    years: 'yyyy1 - yyyy2'
                },

                onRenderCell: function onRenderCell(date, cellType) {
                    if (typeof date == 'undefined') return;
                    var y = date.getFullYear(),
                        m = date.getMonth(),
                        d = date.getDate(),
                        day = date.getDay(),
                        from = calendar.dataset.from,
                        to = calendar.dataset.to,
                        fromCell = month.querySelector('.-range-from-'),
                        toCell = month.querySelector('.-range-to-'),
                        rangeCells = month.querySelectorAll('.-in-range-');

                    if (fromCell) {
                        fromCell.classList.remove('-range-from-');
                    }

                    if (toCell) {
                        toCell.classList.remove('-range-to-');
                    }

                    rangeCells.forEach(function (cell, i) {
                        cell.classList.remove('-in-range-');
                    });

                    if (date.getTime() == from) {
                        return {
                            classes: '-range-from-'
                        };
                    } else if (date.getTime() == to) {
                        return {
                            classes: '-range-to-'
                        };
                    } else if (date.getTime() > from && date.getTime() < to) {
                        return {
                            classes: '-in-range-'
                        };
                    }
                },
                onSelect: function onSelect(formattedDate, date, inst) {
                    if (typeof date == 'undefined') return;
                    var y = date.getFullYear(),
                        m = date.getMonth(),
                        d = date.getDate(),
                        day = date.getDay();

                    var from = calendar.dataset.from,
                        to = calendar.dataset.to,
                        timeStamp = date.getTime();
                    if (from && !to) {
                        if (from > timeStamp) {
                            calendar.dataset.to = from;
                            calendar.dataset.from = timeStamp;
                        } else {
                            calendar.dataset.to = timeStamp;
                        }
                    } else {
                        calendar.dataset.from = timeStamp;
                        calendar.removeAttribute('data-to');
                    }
                    $("[name='date[min]']").val(calendar.dataset.from);
                    $("[name='date[max]']").val(calendar.dataset.to);
                }
            }).data('datepicker');

            controls.forEach(function (button, i) {
                button.addEventListener('click', function (e) {
                    e.preventDefault();

                    var direction = Number(button.closest('[data-calendar-controls]').dataset.calendarControls),
                        currentDate = datepicker.currentDate;
                    switch (direction) {
                        case 0:
                            datepicker.date = new Date(currentDate.getFullYear(), currentDate.getMonth() - 3);
                            break;
                        case 1:
                            datepicker.prev();
                            break;
                        case 2:
                            datepicker.next();
                            break;
                        case 3:
                            datepicker.date = new Date(currentDate.getFullYear(), currentDate.getMonth() + 3);
                            break;
                    }
                });
            });

            if (i == 0) {
                var monthIndex = datepicker.currentDate.getMonth();
                var monthLocale = datepicker.loc.monthsShort;

                for (var k = 0; k < 12; k++) {
                    if (monthLocale[monthIndex] == undefined) monthIndex = 0;
                    monthsList[k].textContent = monthLocale[monthIndex];
                    ++monthIndex;
                }
            }

            datepicker.rangeOptions = customOptions;
            document.addEventListener('updateCalendar', function (e) {
                e.preventDefault();
                calendar.dataset.to = $('[name="date[max]"]').val();
                calendar.dataset.from = $('[name="date[min]"]').val();
                datepicker.update();
            });
            /*document.querySelector('[data-calendar-clear]').addEventListener('click', function (e) {
              e.preventDefault();
              calendar.removeAttribute('data-from');
              calendar.removeAttribute('data-to');
              datepicker.clear();
            });*/

            calendar.addEventListener('click', function (e) {
                if ($('[name="date[max]"]').val() > 0 && $('[name="date[min]"]').val() > 0) {
                    $('[name="date[min]"]').change();
                }
                datepicker.update();
            });
        });

        controls.forEach(function (button, i) {
            button.addEventListener('click', function (e) {
                e.preventDefault();

                var direction = Number(button.closest('[data-calendar-controls]').dataset.calendarControls),
                    progress = calendar.querySelector('.calendar__progress'),
                    monthsItems = calendar.querySelector('.calendar__months-list').children.length - 3,
                    monthWidth = calendar.querySelector('.calendar__months-item').offsetWidth,
                    progressLeft = progress.style.left == '' ? 0 : parseInt(progress.style.left),
                    progressEnd = monthWidth * monthsItems;

                switch (direction) {
                    case 0:
                        progress.style.left = progressEnd + 'px';
                        button.closest('[data-calendar-controls]').dataset.calendarControls = 1;
                        calendar.querySelector('[data-calendar-controls="2"]').dataset.calendarControls = 3;
                        break;
                    case 1:
                        if (progressLeft == monthWidth) {
                            button.closest('[data-calendar-controls]').dataset.calendarControls = 0;
                        }
                        progress.style.left = progressLeft - monthWidth + 'px';
                        calendar.querySelector('[data-calendar-controls="3"]').dataset.calendarControls = 2;
                        break;
                    case 2:
                        if (progressLeft == progressEnd - monthWidth) {
                            button.closest('[data-calendar-controls]').dataset.calendarControls = 3;
                        }
                        progress.style.left = progressLeft + monthWidth + 'px';
                        calendar.querySelector('[data-calendar-controls="0"]').dataset.calendarControls = 1;
                        break;
                    case 3:
                        progress.style.left = 0;
                        button.closest('[data-calendar-controls]').dataset.calendarControls = 2;
                        calendar.querySelector('[data-calendar-controls="1"]').dataset.calendarControls = 0;
                        break;
                }
            });
        });
    }

    // selector

    document.querySelectorAll('[data-selector]').forEach(function (selector, i) {
        var list = selector.querySelector('.selector__list'),
            input = selector.querySelector('.selector__input');

        var count = list.children.length;

        input.value = count;

        selector.querySelectorAll('[data-value]').forEach(function (item, k) {
            item.addEventListener('click', function (e) {
                e.preventDefault();

                var value = item.dataset.value,
                    valueYear = item.dataset.children,
                    selectorItem = document.createElement('li');

                count = list.children.length;

                selectorItem.classList.add('selector__item');
                selectorItem.innerHTML = '<span>' + value + '</span><input type="hidden" name="children[]" value="' + valueYear + '"><button class="selector__remove"></button>';

                list.append(selectorItem);
                input.value = ++count;
                reloadSearch();
            });
        });
    });

    // toggle
    document.querySelectorAll('.toggle__header').forEach(function (toggle, i) {
        toggle.addEventListener('click', function (e) {
            e.preventDefault();

            toggle.closest('.toggle').classList.toggle('toggle_open');
        });
    });

    //counter
    document.querySelectorAll('.counter').forEach(function (counter, i) {
        counter.querySelectorAll('[data-counter-control]').forEach(function (button, k) {
            button.addEventListener('click', function (e) {
                e.preventDefault();

                var counterValue = counter.querySelector('[data-counter-value]'),
                    counterValueInput = counter.querySelector('[data-counter-value-input]'),
                    currentValue = Number(counterValue.dataset.counterValue);

                switch (Number(button.dataset.counterControl)) {
                    case 0:
                        if (currentValue != 1) counterValue.dataset.counterValue = --currentValue;
                        break;
                    case 1:
                        counterValue.dataset.counterValue = ++currentValue;
                        break;
                }
                $(counterValueInput).val(currentValue);
                reloadSearch();
            });
        });
    });

    //reviews
    document.querySelectorAll('[data-limit]').forEach(function (container, i) {
        var limit = container.dataset.limit,
            list = container.querySelector('[data-limit-list]'),
            button = container.querySelector('[data-limit-disable]');

        Array.from(list.children).forEach(function (el, k) {
            if (k >= limit) el.style.display = 'none';
        });

        if (button) {
            button.addEventListener('click', function (e) {
                e.preventDefault();

                Array.from(list.children).forEach(function (el, k) {
                    if (k >= limit) el.style.display = '';
                });

                button.remove();
            });
        }
    });

    //total click
    $(document).on('click', function (e) {
        var select = e.target.closest('.select_open'),
            galleryItem = e.target.closest('.gallery__item');

        if (!select && ![].concat(_toConsumableArray(e.target.classList)).includes('selector__remove') && ![].concat(_toConsumableArray(e.target.classList)).includes('datepicker--cell')) {
            document.querySelectorAll('.select_open').forEach(function (select, i) {
                select.classList.remove('select_open');
            });
        }

        if ([].concat(_toConsumableArray(e.target.classList)).includes('selector__remove')) {
            e.preventDefault();

            var input = e.target.closest('.selector').querySelector('.selector__input');

            input.value = --input.value;
            e.target.parentNode.remove();
            reloadSearch();
        }

        if (!e.target.closest('.drop_show')) {
            if (!e.target.closest('[data-droping]')) {
                var show = document.querySelector('.drop_show');
                if (show) show.classList.remove('drop_show');
            }
        }

        // gallery
        if (galleryItem) {
            var gallery = galleryItem.closest('.gallery'),
                view = gallery.querySelector('.gallery__view'),
                image = galleryItem.dataset.img,
                selected = gallery.querySelector('.gallery__item_selected'),
                count = gallery.querySelector('.gallery__count');

            if (selected) selected.classList.remove('gallery__item_selected');
            galleryItem.classList.add('gallery__item_selected');
            view.querySelector('img').src = image;

            if (count) {
                count.dataset.galleryCountCurrent = Number(galleryItem.dataset.index) + 1;
            }
        }
    });

    // gallery trigger
    document.querySelectorAll('.gallery').forEach(function (gallery, i) {
        var count = gallery.querySelector('.gallery__count'),
            galleryListCount = gallery.querySelector('.gallery__list').children.length;

        if (count) {
            count.dataset.galleryCountAll = galleryListCount;

            gallery.querySelector('[data-gallery-controls]').addEventListener('click', function (e) {
                var direction = Number(e.target.closest('[data-gallery-controls]').dataset.galleryControls);
                var index = gallery.querySelector('.gallery__item_selected').dataset.index;

                switch (direction) {
                    case 0:
                        if (index == 0) {
                            index = galleryListCount - 1;
                        } else {
                            --index;
                        }
                        break;
                    case 1:
                        if (index == galleryListCount - 1) {
                            index = 0;
                        } else {
                            ++index;
                        }
                        break;
                }

                gallery.querySelector('.gallery__item[data-index="' + index + '"]').click();
            });
        }

        gallery.querySelector('.gallery__item:first-child').click();
    });

    document.querySelectorAll('[data-modal-open]').forEach(function (trigger, i) {
        trigger.addEventListener('click', function (e) {
            e.preventDefault();
            var t = e.target.closest('[data-modal-open]');

            var data = t.dataset.modalOpen,
                modalElement = document.querySelector('[data-modal="' + data + '"]'),
                scroll = $(window).scrollTop();

            if (data == 'gallery') {
                modalElement.querySelector('.modal__content').innerHTML = t.innerHTML;
            }

            var modalContent = modalElement.innerHTML;

            var modal = new tingle.modal({
                closeMethods: ['overlay', 'escape'],
                onClose: function onClose() {
                    $(window).scrollTop(scroll);
                    try {
                        this.remove();
                    } catch (e) {
                    }
                },
                cssClass: modalElement.classList
            });

            modal.setContent(modalContent);
            modal.open();

            if (data == 'map') {
                var coords = t.dataset.coords,
                    contact_id = t.dataset.id;
                if (coords !== undefined && contact_id !== undefined && placemarksIds !== undefined) {
                    var modalMap = new ymaps.Map($('.modal__content_map').get(0), {
                            center: coords.split(','),
                            zoom: 16,
                            controls: []
                        }),
                        myPlacemark = new ymaps.Placemark(placemarksIds[contact_id].coordPoint, {
                            balloonContentHeader: placemarksIds[contact_id].title,
                            balloonContentBody: placemarksIds[contact_id].content,
                            balloonContentFooter: '',
                            hintContent: placemarksIds[contact_id].title
                        }, {
                            preset: "islands#icon",
                            iconColor: '#FF0E19'
                        });
                    modalMap.geoObjects.add(myPlacemark);
                    $('.js_caption_map').text(placemarksIds[contact_id].title);
                }
            }

            if (data == 'gallery') {
                var g = e.target.closest('.gallery');

                modal.modalBoxContent.querySelectorAll('[data-gallery-controls]').forEach(function (arrow, k) {
                    arrow.addEventListener('click', function (e) {
                        var direction = Number(e.target.closest('[data-gallery-controls]').dataset.galleryControls),
                            selected = g.querySelector('.gallery__item_selected');
                        var newSelected = void 0;

                        switch (direction) {
                            case 0:
                                newSelected = selected.previousElementSibling;

                                if (!newSelected) {
                                    newSelected = selected.parentNode.children[selected.parentNode.children.length - 1];
                                }
                                break;

                            case 1:
                                newSelected = selected.nextElementSibling;

                                if (!newSelected) {
                                    newSelected = selected.parentNode.children[0];
                                }
                                break;
                        }

                        var img = newSelected.dataset.img;
                        modal.modalBoxContent.querySelector('img').src = img;
                        selected.classList.remove('gallery__item_selected');
                        newSelected.classList.add('gallery__item_selected');
                    });
                });
            }

            var forms = modal.modalBoxContent.querySelectorAll('form');

            forms.forEach(function (form, i) {
                form.querySelectorAll('select').forEach(function (select, i) {
                    console.log(i);

                    new CustomSelect({
                        elem: select
                    });

                    form.querySelector('.select button').addEventListener('click', function (e) {
                        e.preventDefault();
                    });
                });
            });

            phoneMask();

            try {
                document.querySelector('.modal__close').addEventListener('click', function (e) {
                    e.preventDefault();
                    modal.close();
                });
            } catch (e) {
            }
        });
    });

    //drop
    document.querySelectorAll('[data-droping]').forEach(function (drop, i) {
        drop.addEventListener('click', function (e) {
            e.preventDefault();

            if (!e.target.dataset.droping) return;

            var data = drop.dataset.droping,
                dropped = document.querySelector('[data-dropped="' + data + '"]');

            dropped.classList.toggle('drop_show');
        });
    });

    //rating
    var trip = document.querySelector('.rating_trip');

    if (trip) {
        var tripValue = Number(trip.querySelector('[data-value]').dataset.value) * 10 * 2,
            tripProgress = trip.querySelector('.rating__progress');

        tripProgress.style.width = tripValue + '%';
    }

    document.querySelectorAll('[data-image]').forEach(function (image, i) {
        image.addEventListener('click', function (e) {
            e.preventDefault();

            var src = image.dataset.image,
                img = document.createElement('img');

            img.src = src;

            var modal = new tingle.modal({
                closeMethods: ['overlay', 'escape'],
                onClose: function onClose() {
                    try {
                        this.remove();
                    } catch (e) {
                    }
                },
                cssClass: ['modal', 'modal_gallery']
            });

            modal.setContent(img);
            modal.open();
        });
    });

    // Шаги


    // Печать
    document.querySelectorAll('[data-print]').forEach(function (print, i) {
        print.addEventListener('click', function (e) {
            var printHTML = e.target.closest('[data-print-content]').innerHTML,
                printContainer = document.querySelector('.printSelection');

            printContainer.innerHTML = printHTML;
            document.querySelector('body').classList.add('printSelected');

            window.print();

            setTimeout(function () {
                document.querySelector('body').classList.remove('printSelected');
                printContainer.innerHTML = "";
            }, 0);
        });
    });

    // address searchda
    /*var addressSearchForm = $('#address-search');

    if (addressSearchForm) {
        var field = $(addressSearchForm).find('input');

        $(field).on('keyup', function (e) {
            var value = $(this).val().toLowerCase();

            $('[data-address]').each(function (i, address) {
                if ($(address).text().toLowerCase().includes(value)) {
                    $(address).closest('.office').show();
                } else {
                    $(address).closest('.office').hide();
                }
            });

            $('[data-tab="0"]').trigger('click');
        });
    }*/

    //Имитация загрузки
})(document);
$(document).on("keyup", ".search_in_list", function () {
    var textFind = $(this).val();
    $(this).parents(".select__dropdown").find("ul.options li").each(function (num, elem) {
        var textLi = $(".checkbox__content p", elem).text();
        var findPos = textLi.toUpperCase().indexOf(textFind.toUpperCase()) + 1;
        if (findPos == 0 || findPos > 1) {
            $(elem).addClass("hide");
        } else {
            $(elem).removeClass("hide");
        }
    })
});

$(document).on("keyup", ".search_in_list_2", function () {
    var textFind = $(this).val();
    $(this).parents(".block_list").find("ul.options li").each(function (num, elem) {
        var textLi = $(".checkbox__content p", elem).text();
        var findPos = textLi.toUpperCase().indexOf(textFind.toUpperCase()) + 1;
        if (findPos == 0 || findPos > 1) {
            $(elem).addClass("hide_2");
        } else {
            $(elem).removeClass("hide_2");
        }
    })
});
$(document).on("click",".show_more",function () {
    if ($(this).text() == $(this).attr("data-hide")) {
        $(this).parents(".max_height_2").find(".options__item").removeClass("hide");
        $(this).text($(this).attr("data-show"));
        console.log($(this).attr("data-show"));
    }
    else{
        $(this).parents(".max_height_2").find(".options__item").each(function ($i,$elem) {
            if ($i>=10) $($elem).addClass("hide");
        });
        $(this).text($(this).attr("data-hide"));
        console.log($(this).attr("data-hide"));
    }
});
$(document).on("click",".cleare_all",function () {
    $(this).parents(".block_list").find("[type='checkbox'][value='']").prop("checked",true).trigger("change");
});
$(document).on("change","[name=\"hotel[]\"],[name=\"town[]\"],[name=\"operator[]\"]",function () {
    if ($(this).val()>0){
        $(this).parents("ul").find("[type='checkbox'][value='']").prop("checked",false);
        $(this).parents(".block_list").find(".cleare_all").show();
    }
    else{
        $(this).parents("ul").find("[type='checkbox']").not("[value='']").prop("checked",false);
        $(this).parents(".block_list").find(".cleare_all").hide();
    }
});
function reloadSearch($bool) {
    var dataSearch = $(".js__search__form").serializeArray();
    var dataFilter = [];
    if ($(".js__filter__form").length) {
        var dataUseFilter = [{name: "FILTER", value: "Y"}];
        dataFilter = $(".js__filter__form").serializeArray();
        dataFilter.concat(dataUseFilter);
    } else {
        dataSearch.push({name: "inc_perelet", value: "on"});
    }
    if (typeof window.dataAjaxLoadData != "undefined") window.dataAjaxLoadData.abort();
    window.dataAjaxLoadData = $.ajax({
        url: "/include/prepare_data_search.php?AJAX_PAGE=Y",
        data: dataSearch.concat(dataFilter),
        dataType: "json",
        success: function (data) {
            window.SearchTour = data;
            setDataSearch();
            if ($bool == true) {
                searchTur();
            }
            //if($('#result_search_hot_tours_block').length > 0) getHotToursResult();
        }
    })
}

$(document).on("change", ".js__search__form input,.js__filter__form input", function () {
    if ($(this).attr("name") == "country") {
        var setname;
        setname = $(this).parents(".checkbox").find(".checkbox__content p").html();
        $(this).parents(".search__select").find("input.js-Dropdown-title").val(setname);
        $(this).parents(".search__select").find("input.js-Dropdown-title").attr("data-prev", setname);
        $(this).parents(".search__select").removeClass("select_open");
        $(this).parents(".search__select").find("ul.options li").removeClass("hide");
    }
    reloadSearch()
});
$(document).on("click", ".section__block_sort .js-Dropdown-list li", function () {
    var typesort = $(this).attr("data-value");
    var result = $(this).parents(".section__block_sort").attr("data-result");
    $.cookie("TYPE_SORT", typesort, {path: '/'});
    clearInterval(window.processLoading);
    delete window.processLoading;
    getToursResult();
    loadProcess(99);
    getResult(result, 1);
})

function SetValues() {
    $.each(window.SearchTour, function (key, data) {
        switch (key) {
            case "COUNTRY":
                $("[data-name='" + key + "'] .js-Dropdown-title").val(data.TEXT);
                $("[data-name='" + key + "'] .js-Dropdown-title").attr("data-prev", data.TEXT);
                break;
            case "DATE":
                $("[data-name='" + key + "'] .js-Dropdown-title").html(data.TEXT);
                break;
            case "PEOPLE":
                $("[data-name='" + key + "'] .js-Dropdown-title").html(data.TEXT + " " + window.SearchTour.CHILDREN.TEXT);
                break;
            case "NIGHT":
                $("[data-name='" + key + "'] .js-Dropdown-title").html(data.TEXT);
                break;
        }

    })
}

function setDataSearch() {
    $.each(window.SearchTour, function (key, data) {
        switch (key) {
            case "COUNTRY":
                $("[data-name='" + key + "'] .js-Dropdown-title").text(data.TEXT);
                $("[name='" + key.toLowerCase() + "'][value='" + data.VALUE + "']").prop("checked", true);
                break;
            case "DATE":
                $("[data-name='" + key + "'] .js-Dropdown-title").text(data.TEXT);
                $("[name='" + key.toLowerCase() + "[min]']").val(data.VALUE.MIN);
                $("[name='" + key.toLowerCase() + "[max]']").val(data.VALUE.MAX);
                var event = new Event("updateCalendar");
                document.dispatchEvent(event);
                break;
            case "PEOPLE":
                $("[data-name='" + key + "'] .counter__value").attr("data-counter-value", data.VALUE);
                $("[data-name='" + key + "'] .js-Dropdown-title").text(data.TEXT);
                $("[name='" + key.toLowerCase() + "']").val(data.VALUE);
                break;
            case "NIGHT":
                $("[data-name='" + key + "'] .counter__value").attr("data-counter-value", data.VALUE);
                $("[data-name='" + key + "'] .js-Dropdown-title").text(data.TEXT);
                $("[name='" + key.toLowerCase() + "']").val(data.VALUE);
                break;
            case "CHILDREN":
                var parent = $('[data-name="PEOPLE"] .selector__list');
                $(parent).html("");
                var childName;
                $.each(data.VALUE, function (i, data) {
                    if (data == 1) childName = "Ребенок 1 год";
                    else if (data > 1 && data < 5) childName = "Ребенок " + data + " года";
                    else childName = "Ребенок " + data + " лет";
                    $(parent).append('<li class="selector__item"><span>' + childName + '</span><input type="hidden" name="children[]" value="' + data + '"><button class="selector__remove"></button></li>');
                })
                break;
        }
    });
    $.each(window.SearchTour, function (key, data) {
        var iCounter = 1,
            hideOther = false,
            linkMore = null;
        switch (key) {
            case "TOWN":
                var TownList = $('ul[data-name="TOWN"]');
                iCounter = 0;
                hideOther = false;
                linkMore = TownList.parents(".max_height_2").find(".show_more");
                TownList.html("");
                $.each(data.VARIANT, function (i, data) {
                    if (linkMore.text() != linkMore.attr("data-show")) iCounter++;
                    if (iCounter>10){
                        hideOther = true;
                    }
                    if (hideOther){
                        TownList.append('<li class="options__item hide"><label class="checkbox"><input type="checkbox" name="town[]" value="' + data.Id + '"/><div class="checkbox__box"></div><div class="checkbox__content"><p>' + data.Name + '</p></div></label></li>');
                    }
                    else{
                        TownList.append('<li class="options__item"><label class="checkbox"><input type="checkbox" name="town[]" value="' + data.Id + '"/><div class="checkbox__box"></div><div class="checkbox__content"><p>' + data.Name + '</p></div></label></li>');
                    }
                });
                if (hideOther){
                    linkMore.text(linkMore.attr("data-hide"));
                }
                if (data.VALUE.length>0) {
                    $.each(data.VALUE, function (i, data) {
                        $("[name='town[]'][value='" + data + "']").prop("checked", true);
                    });
                }
                else{
                    $("[name='town[]'][value='']").prop("checked", true);
                }
                break;
            case "HOTEL":
                var HotelList = $('ul[data-name="HOTEL"]');
                iCounter = 0;
                hideOther = false;
                linkMore = HotelList.parents(".max_height_2").find(".show_more");
                HotelList.html("");
                var counterPopular = 20;
                $.each(data.VARIANT, function (i, data) {
                    if (linkMore.text() != linkMore.attr("data-show")) iCounter++;
                    if (iCounter>10){
                        hideOther = true;
                    }
                    if (hideOther){
                        HotelList.append('<li class="options__item hide"><label class="checkbox"><input type="checkbox" name="hotel[]" value="' + data.Id + '"/><div class="checkbox__box"></div><div class="checkbox__content"><p>' + data.Name + '</p></div></label></li>');
                    }
                    else{
                        HotelList.append('<li class="options__item"><label class="checkbox"><input type="checkbox" name="hotel[]" value="' + data.Id + '"/><div class="checkbox__box"></div><div class="checkbox__content"><p>' + data.Name + '</p></div></label></li>');
                    }
                });
                if (hideOther){
                    linkMore.text(linkMore.attr("data-hide"));
                }
                if (data.VALUE.length>0) {
                    $.each(data.VALUE, function (i, data) {
                        $("[name='hotel[]'][value='" + data + "']").prop("checked", true);
                    });
                }
                else{
                    $("[name='hotel[]'][value='']").prop("checked", true);
                }
                break;
            case "OPERATOR":
                var Operatorlist = $('ul[data-name="OPERATOR"]');
                iCounter = 0;
                hideOther = false;
                linkMore = Operatorlist.parents(".max_height_2").find(".show_more");
                Operatorlist.html("");
                $.each(data.VARIANT, function (i, data) {
                    if (linkMore.text() != linkMore.attr("data-show")) iCounter++;
                    if (iCounter>10){
                        hideOther = true;
                    }
                    if (hideOther){
                        Operatorlist.append('<li class="options__item hide"><label class="checkbox"><input type="checkbox" name="operator[]" value="' + data.Id + '"/><div class="checkbox__box"></div><div class="checkbox__content"><p>' + data.Name + '</p></div></label></li>');
                    }
                    else{
                        Operatorlist.append('<li class="options__item"><label class="checkbox"><input type="checkbox" name="operator[]" value="' + data.Id + '"/><div class="checkbox__box"></div><div class="checkbox__content"><p>' + data.Name + '</p></div></label></li>');
                    }
                });
                if (hideOther){
                    linkMore.text(linkMore.attr("data-hide"));
                }
                if (data.VALUE.length>0) {
                    $.each(data.VALUE, function (i, data) {
                        $("[name='operator[]'][value='" + data + "']").prop("checked", true);
                    });
                }
                else{
                    $("[name='operator[]'][value='']").prop("checked", true);
                }
                break;
            case "MIALS":
                $("[name='classmeals[]']").prop("checked", false);
                $.each(data.VALUE, function (i, data) {
                    $("[name='classmeals[]'][value='" + data + "']").prop("checked", true);
                });
                break;
            case "CLASS_HOTEL":
                $("[name='classhotel[]']").prop("checked", false);
                $.each(data.VALUE, function (i, data) {
                    $("[name='classhotel[]'][value='" + data + "']").prop("checked", true);
                });
                break;
        }
    });
    SetValues();
}

function loadProcess(percent) {
    var loading = document.querySelector('.loading');
    var valueElement = loading.querySelector('.loading__value');
    var value = parseInt(valueElement.textContent);
    var setPercent;
    valueElement.dataset.percent = percent;
    loading.classList.add('loading_process');
    if (typeof window.processLoading == "undefined") {
        window.processLoading = setInterval(function () {
            setPercent = parseInt(valueElement.dataset.percent);
            if (value < setPercent) {
                valueElement.innerHTML = ++value;
            }
            if (value >= 100) {
                clearInterval(window.processLoading);
                delete window.processLoading;
                loading.classList.add('loading_finish');
                document.querySelectorAll('.loading-process').forEach(function (el, i) {
                    el.classList.add('loading-process_finish');
                });
            }
        }, 20);
    }
}

function getToursResult() {
    var dataAjax = window.SearchTour;
    var loading = '<div class="loading"><span class="loading__value" data-percent="0">0</span><div class="loading__plain"><svg><use xlink:href="/bitrix/templates/zapytevky/img/symbols.svg#svg-loading"></use></svg></div></div>';
    $(document.body).animate({
        'scrollTop': $('h2.caption').offset().top
    }, 1000);
    $("#result_search_block").html(loading);
}

function getOrderResult() {
    var dataAjax = window.SearchTour;
    var loading = '<div class="loading"><span class="loading__value" data-percent="0">0</span><div class="loading__plain"><svg><use xlink:href="/bitrix/templates/zapytevky/img/symbols.svg#svg-loading"></use></svg></div></div>';
    $(document.body).animate({
        'scrollTop': $('h2.caption').offset().top
    }, 1000);
    $("#order_result").html(loading);
    loadProcess(99);
}

function PrepareDataAjax() {
    var data = {};
    $.each(window.SearchTour, function (key, dataVal) {
        data[key] = dataVal.VALUE;
    })
    return data;
}

function getResult(idRequest, $page) {
    $.ajax({
        url: "/search-tour/search-tur.php?RESULT=Y&PAGE=" + $page + "&ID_RESULT=" + idRequest,
        data: PrepareDataAjax(),
        success: function (data) {
            $('#result_search_block').html(data);
            new CustomSelect({
                elem: document.getElementById('type_sort')
            });
        }
    })
}

function getResultStatus(idRequest, lastPercent) {
    loadProcess(lastPercent);

    window.ajaxSearchBreack = $.ajax({
        url: "/search-tour/search-tur.php?ID_RESULT=" + idRequest,
        dataType: "json",
        success: function (data) {

            if (data['success']) {
                delete window.ajaxSearchBreack;
                getResult(idRequest, 1);
            } else {

                if (window.limitGet-- == 0) {
                    getResult(idRequest, 1);
                } else {

                    sleep(500).then(() => {
                        getResultStatus(idRequest, data['percent'])
                    });
                }
            }
        }
    })
}

function searchTur($bool) {
    if ($("#result_search_block").length == 0) return;
    if (location.pathname == '/hot-tours/') return false;
    if ($bool && (location.pathname.indexOf("search-tour") == -1) && (location.pathname.indexOf("tury-") == -1)) {
        location.href = '/search-tour/';
    } else {
        if (typeof window.processLoading != "undefined") clearInterval(window.processLoading);
        delete window.processLoading;
    }
    var $_GET = {};

    document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
        function decode(s) {
            return decodeURIComponent(s.split("+").join(" "));
        }
        $_GET[decode(arguments[1])] = decode(arguments[2]);
        console.log($_GET);
    });
    var listPost = PrepareDataAjax();
    $.ajax({
        url: "/search-tour/chekc_page_relink.php",
        data: {extLocation: listPost["COUNTRY"]},
        dataType: "json",
        success: function (data) {
            window.history.pushState('', '', data.LINK);
            $(".section__header h2").html(data.NAME);
            $(".breadcrumbs__item>span").html(data.NAME);
            if (data.IS_EXIST) {
                $(".section_information").html(data.DETAL_HTML).addClass("section_light");
            } else {
                $(".section_information").html("").removeClass("section_light");
            }
        }
    });
    window.limitGet = 100;
    getToursResult();
    $.ajax({
        url: "/search-tour/search-tur.php?PREPARE=Y",
        data: PrepareDataAjax(),
        dataType: "json",
        success: function (data) {
            if (data["success"]) {
                if (typeof window.ajaxSearchBreack == "object") {
                    window.ajaxSearchBreack.abort();
                    if (typeof window.processLoading != "undefined") {
                        clearInterval(window.processLoading);
                        delete window.processLoading;
                    }
                }
                getResultStatus(data["result_id"], 0);
            }
        }
    });
    return false;
}
function sleep(time) {
    return new Promise((resolve) => setTimeout(resolve, time));
}

$(document).on("click", ".pages__list a", function () {
    clearInterval(window.processLoading);
    delete window.processLoading;
    getToursResult();
    loadProcess(99);
    var page, result;
    page = $(this).attr("data-page");
    result = $(this).attr("data-result");
    getResult(result, page)
});

function CheckOrder($step, __this) {
    var error = false;
    var data = $(__this).attr("data-step-button"),
        current = document.querySelector('.step-content_current'),
        step = document.querySelectorAll('[data-step]');
    if (data == 'next') {
        switch ($step) {
            case 1:
                $('[data-step-num="1"] input,[data-step-num="1"] select').each(function (i, el) {
                    valueSet = $('[name="' + $(el).attr("name") + '"]').val();
                    if ($(el).hasClass("required") && ((typeof valueSet == "object" && valueSet == null) || (typeof valueSet == "string" && valueSet == ""))) {
                        error = true;
                        $(el).parents(".field").addClass("error");
                    }
                })
                break;
            case 2:
                $('[data-step-num="2"] input,[data-step-num="2"] select').each(function (i, el) {
                    valueSet = $('[name="' + $(el).attr("name") + '"]').val();
                    if ($(el).hasClass("required") && ((typeof valueSet == "object" && valueSet == null) || (typeof valueSet == "string" && valueSet == ""))) {
                        error = true;
                        $(el).parents(".field").addClass("error");
                    }
                })
                break;
        }
    }
    if (!error) {
        current.classList.remove('step-content_current');
        var index = Number(current.dataset.stepContent);
        var valueSet;
        switch (data) {
            case 'next':
                document.querySelector('[data-step-content="' + ++index + '"]').classList.add('step-content_current');
                break;
            case 'prev':
                document.querySelector('[data-step-content="' + --index + '"]').classList.add('step-content_current');
                break;
        }

        step.forEach(function (step, k) {
            step.dataset.step = index;
        });
    }
}

function GetOrderInfo($claim) {
    $.ajax({
        url: "/oplatit-on-layn/order.php?action=getorder&claim=" + $claim,
        dataType: "html",
        success: function (data) {
            loadProcess(100);
            $("#order_result").html(data);
        }
    })
}

function CreateOrder() {
    if ($("[name='USE_PD']").prop("checked")) {
        var current = document.querySelector('.step-content_current'),
            step = document.querySelectorAll('[data-step]');
        current.classList.remove('step-content_current');
        document.querySelector('[data-step-content="finish"]').classList.add('step-content_current');
        $("[data-step]").attr("data-step", 3);
        getOrderResult();
        $.ajax({
            url: "/oplatit-on-layn/order.php?action=create",
            data: $('.js_form_rder').serializeArray(),
            dataType: "json",
            success: function (data) {
                GetOrderInfo(data)
            }
        })
    } else {
        $("[name='USE_PD']").parents(".checkbox").addClass("error");
    }
}

$(document).on("click", ".field.error", function () {
    $(this).removeClass("error");
})
$(document).on("click", "label.error", function () {
    $(this).removeClass("error");
})
$(document).on("click", ".search__location li", function () {
    $.cookie("USER_SITY", $(this).attr("data-value"), {path: '/'});
})
/*$(document).on("change","#type_sort",function () {
    $.cookie("TYPE_SORT", $(this).val(),{ path: '/'});
    var result = $("option:selected",this).attr("data-result");
    clearInterval(window.processLoading);
    delete window.processLoading;
    getToursResult();
    loadProcess(99);
    getResult(result,1);
})*/

$(document).on("focus", "input.js-Dropdown-title", function () {
    $(this).val("");
});
$(document).on("keyup", "input.js-Dropdown-title", function () {
    var textFind = $(this).val();
    $(this).parents(".search__select").find("ul.options li").each(function (num, elem) {
        var textLi = $(".checkbox__content p", elem).text();
        var findPos = textLi.toUpperCase().indexOf(textFind.toUpperCase()) + 1;
        if (findPos == 0 || findPos > 1) {
            $(elem).addClass("hide");
        } else {
            $(elem).removeClass("hide");
        }
    })
})