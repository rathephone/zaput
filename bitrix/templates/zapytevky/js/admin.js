$(document).ready(function() {
    updSelectSity();
    updSelectCountry();
})
function updSelectSity() {
    $("select [data-if-city]").removeClass('show_if_city').addClass("hide_if_city");
    $console = [];
    $("select [data-select-city]:selected").each(function (iSelect,elSelect) {
        $("select [data-if-city]").each(function (iIf,elIf) {
            if ($(elIf).data('if-city').toString().split('|').indexOf($(elSelect).data('select-city').toString()) != -1){
                $(elIf).removeClass("hide_if_city").addClass("show_if_city");
            }
        })
    })
    console.log($console);
}
function updSelectCountry() {
    $("select [data-if-country]").removeClass('show_if_country').addClass("hide_if_country");
    $("select [data-select-country]:selected").each(function (iSelect,elSelect) {
        $("select [data-if-country]").each(function (iIf,elIf) {
            if ($(elIf).data('if-country').toString().split('|').indexOf($(elSelect).data('select-country').toString()) != -1){
                $(elIf).removeClass("hide_if_country").addClass("show_if_country");
            }
        })
    })
}