sletatModule = {};
(function (module, $) {
    module.urlPathAjax = "/sletat/ajax.php?";
    module.init = function () {
        module.elements = {
            blockCountries: $('.countries__list') || '',
            blockMainOffers: $('.main__offers') || ''
        };
        if (module.elements.blockCountries.length > 0) {
            module.getShowcaseReview()
        }
    };

    module.priceformat = function (data) {
        return String(Math.ceil(parseInt(data))).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ");
    };

    module.getShowcaseReview = function () {
        //console.log('getShowcaseReview');
        $.ajax({
            url: module.urlPathAjax + "f=GetShowcaseReview",
            dataType: "json",
            success: function (json) {
                if (json.status === 'ok') {
                    if (json.res) {
                        var countries = json.res.Rows;
                        if (countries) {
                            module.blockCountriesList(countries);
                            if (module.elements.blockMainOffers.length > 0) {
                                module.blockMainOffers(countries);
                            }
                        }
                    }
                } else {
                }
            }
        });
    };

    module.compareSort = function (pA, pB) {
        var nameA = pA.CountryName.toLowerCase(), nameB = pB.CountryName.toLowerCase()
        if (nameA < nameB) //сортируем строки по возрастанию
            return -1;
        if (nameA > nameB)
            return 1;
        return 0; // Никакой сортировки
    };

    module.blockMainOffers = function (countries) {
        //console.log(countries);
        countries.sort(module.compareSort);
        var card_items = $('<div class="items items_quadruple"></div>');
        var len = 8;
        if (countries.length < len) len = countries.length;
        for (var i = 0; i < len; i++) {

            var param = "?SourceId=" + countries[i].SourceId + "&OfferId=" + countries[i].OfferId + "&requestId=0&hot=1";
            var NewPrice = module.priceformat(countries[i].NewPrice);
            var OldPrice = '';
            if (countries[i].OldPrice > 0)
                OldPrice = '<div class="price__value price__value_old"><span>' + module.priceformat(countries[i].OldPrice) + 'р</span></div>';
            else
                OldPrice = '';

            card_items.append(
                '<div class="card card_hot">' +
                '<div class="card__image"><img src="' + countries[i].ImageHotel + '"/></div>' +
                '<div class="card__info"><span class="caption caption_size-s">' + countries[i].CountryName + ', ' + countries[i].ResortName + '</span>' +
                '<div class="card__text"><p>' + countries[i].HotelName + '</p><p> ' + countries[i].CheckInDate + ' - ' + countries[i].CheckOutDate + '  (' + module.count_lex(countries[i].Nights, "ночь", "ночи", "ночей") + ')</p></div>' +
                '<div class="card__footer">' +
                '<div class="price">' +
                OldPrice +
                '<div class="price__value"><span>' + NewPrice + 'р</span></div>' +
                '</div><a class="button" href="/tour/' + param + '"><span>Подробнее</span></a></div></div>' +
                '</div>'
            );
        }
        module.elements.blockMainOffers.append(card_items);
        module.elements.blockMainOffers.append(
            '<div class="section__footer"><a class="button button_highlight button_size-wide" href="/hot-tours">Смотреть все предложения</a></div>'
        );
    };

    module.blockCountriesList = function (countries) {
        var itemsList = $('<ul class="countries__list"></ul>');
        var len = 25;
        if (countries.length < len) len = countries.length;
        for (var i = 0; i < len; i++) {
            var price = module.priceformat(countries[i].NewPrice);
            itemsList.append(
                '<li class="countries__item" data-country="' + countries[i].CountryId + '">' +
                '<a class="country" href="' + countries[i].DetailPageUrl + '">' +
                '<div class="country__title">' +
                '<div class="country__flag">' +
                '<img src="https://static.sletat.ru/images/flags/' + countries[i].CountryId + '.png" alt="' + countries[i].CountryName + '">' +
                '</div>' +
                '<span>' + countries[i].CountryName + '</span>' +
                '</div>' +
                '<div class="country__price"><span>от <strong>' + price + '</strong> р</span></div>' +
                '</a>' +
                '</li>'
            );
        }
        $('.countries__list').html(itemsList.children());
        $(".countries__list").slideDown(400,function () {
            $(".countries__more").show()
        });
    };

    module.count_lex = function (c, a, b, d) {
        if (c % 10 == 1 && c % 100 != 11) return (c + ' ' + a);
        if (c % 10 > 1 && c % 10 < 5 && !(c % 100 > 11 && c % 100 < 15)) return (c + ' ' + b);
        return (c + ' ' + d);
    };
})(sletatModule, jQuery);

sletatModule.init();