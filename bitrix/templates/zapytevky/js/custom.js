document.addEventListener('submit', function (e) {
    var _form = $(e.target),
        _data = _form.serialize();

    if(_form.hasClass('form__feedback')) {
        $.ajax({
            type: "get",
            url: "/bitrix/templates/zapytevky/ajax/feedback.php",
            data: _data,
            dataType: 'json',
            success: function(data)
            {
                if (data.status === 'ok') {
                    _form.html('<h2 style="text-align:center;">'+data.msg+'</h2>');
                } else {
                    _form.find('.form__feedback-error').text(data.msg);
                }
            }
        });
        e.preventDefault();
    }

    if(_form.hasClass('form__feedback-select')) {
        $.ajax({
            type: "get",
            url: "/bitrix/templates/zapytevky/ajax/feedback_select.php",
            data: _data,
            dataType: 'json',
            success: function(data)
            {
                if (data.status === 'ok') {
                    _form.html('<div class="form__subcribe_success">'+data.msg+'</div>');
                } else {
                    _form.find('.form__feedback-error').text(data.msg);
                }
            }
        });
        e.preventDefault();
    }

    if(_form.hasClass('form__subscribe')) {
        $.ajax({
            type: "get",
            url: "/bitrix/templates/zapytevky/ajax/subscribe.php",
            data: _data,
            dataType: 'json',
            success: function(data)
            {
                if (data.status === 'ok') {
                    _form.html('<div class="form__subcribe_success">'+data.msg+'</div>');
                    /*setTimeout(function () {
                        location.href = window.location.href;
                    }, 2000);*/
                } else {
                    _form.find('.form__subscribe-error').text(data.msg);
                }
            }
        });
        e.preventDefault();
    }

    if(_form.hasClass('form__review')) {
        $.ajax({
            type: "post",
            url: "/bitrix/templates/zapytevky/ajax/review.php",
            data: _data,
            dataType: 'json',
            success: function(data)
            {
                if (data.status === 'ok') {
                    _form.parents('.block_form').find('.caption').text(data.msg);
                    _form.remove();
                } else {
                    _form.find('.form__feedback-error').text(data.msg);
                }
            }
        });
        e.preventDefault();
    }

    if(_form.hasClass('form__order_zakaz')) {
        $.ajax({
            type: "get",
            url: "/bitrix/templates/zapytevky/ajax/order_zakaz.php",
            data: _data,
            dataType: 'json',
            success: function(data)
            {
                if (data.status === 'ok') {
                    _form.html('<h2 style="text-align:center;">'+data.msg+'</h2>');
                } else {
                    _form.find('.form__feedback-error').text(data.msg);
                }

            }

        });
        e.preventDefault();
    }

    if(_form.hasClass('form__claim')) {
        $.ajax({
            type: "get",
            url: "/bitrix/templates/zapytevky/ajax/claim.php",
            data: _data,
            dataType: 'json',
            success: function(data)            {
                if (data.status === 'ok') {
                    _form.html('<h2 style="text-align:center;">'+data.msg+'</h2>');
                } else {
                    _form.find('.form__feedback-error').text(data.msg);
                }
            }
        });
        e.preventDefault();
    }
});
function showAllTour(dataId) {
    $('.showHotel'+dataId).fadeIn();
    $(".fadeBtn"+dataId).fadeOut();
}