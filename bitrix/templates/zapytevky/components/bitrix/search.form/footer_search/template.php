<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<form class="form" action="<?=$arResult["FORM_ACTION"]?>">
    <label class="field field_btn">
        <div class="field__input field__input_with-button">
            <input type="text" name="q" value=""  placeholder="Поиск по сайту"/>
            <button class="field__btn">
                <svg>
                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-search"></use>
                </svg>
            </button>
        </div>
    </label>
</form>