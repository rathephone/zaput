<?php
global $APPLICATION;
if(isset($arResult["PREV_LINK"]) && $arResult["PREV_LINK"]["URL"] && $arResult["PREV_LINK"]["NAME"]){
    $APPLICATION->AddChainItem($arResult["PREV_LINK"]["NAME"], $arResult["PREV_LINK"]["URL"]);
}
