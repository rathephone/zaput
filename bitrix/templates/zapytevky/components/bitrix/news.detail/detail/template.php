<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$arMenu = $arResult["MENU"];
?>
<section class="section section_light section_information">
    <div class="section__container container container_flex">
        <div class="section__block section__block_sidebar">
            <? if(count($arMenu)){?>
                <ul class="sidebar-links">
                    <?foreach ($arMenu as $menu){?>
                        <li class="sidebar-links__item"><a href="<?=$menu["URL"];?>"><?=$menu["NAME"];?></a><i class="ico">
                                <svg>
                                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow-small"></use>
                                </svg></i>
                        </li>
                    <?}?>
                </ul>
            <?}?>
        </div>
        <div class="section__block section__block_main">
            <!--<h3 class="caption"><?/*=$arResult["NAME"]*/?></h3>-->
            <?=$arResult["PREVIEW_TEXT"];?>
            <? if(count($arMenu)){?>
                <ul class="content-links">
                    <?foreach ($arMenu as $menu){?>
                        <li class="content-links__item"><a href="<?=$menu["URL"];?>"><?=$menu["NAME"];?></a></li>
                    <?}?>
                </ul>
            <?}?>

        </div>
    </div>
</section>