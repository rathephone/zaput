<?
$arMenu = [];
if (isset($arResult["DISPLAY_PROPERTIES"]["COUNTRY"]) && $arResult["DISPLAY_PROPERTIES"]["COUNTRY"]["DISPLAY_VALUE"]) {
    $arSelectText = Array("ID", "NAME", "PROPERTY_IS_SHOW", "DETAIL_PAGE_URL");
    $arFilterText = Array(
        "IBLOCK_ID" => IBLOCK_ID_COUNTRIES_SEO,
        "ACTIVE" => "Y",
        "PROPERTY_COUNTRY" => $arResult["DISPLAY_PROPERTIES"]["COUNTRY"]["VALUE"],
        "!ID" => $arResult["ID"]
    );
    $resCountryText = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilterText, false, Array("nPageSize" => 5), $arSelectText);
    while ($resText = $resCountryText->GetNext()) {
        if ($resText["PROPERTY_IS_SHOW_VALUE"] == "Да") {

            $res = CIBlockElement::GetByID($arResult["DISPLAY_PROPERTIES"]["COUNTRY"]["VALUE"]);
            if ($ar_res = $res->GetNext()) {
                $arMenu[] = [
                    "URL" => $ar_res["DETAIL_PAGE_URL"],
                    "NAME" => $resText["NAME"]
                ];
                $arResult["PREV_LINK"] = [
                    "URL" => $ar_res["DETAIL_PAGE_URL"],
                    "NAME" => $ar_res["NAME"]
                ];
            }
        } else {
            $arMenu[] = [
                "URL" => $resText["DETAIL_PAGE_URL"],
                "NAME" => $resText["NAME"]
            ];
        }
    }
}
$arResult["MENU"] = $arMenu;
$this->__component->SetResultCacheKeys(array(
    "PREV_LINK",
));