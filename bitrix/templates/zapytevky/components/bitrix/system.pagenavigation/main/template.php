<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */
$this->setFrameMode(true);
if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

$navNum = $arResult["NavNum"];
$navPageNomer = $arResult["NavPageNomer"];
$url = $arResult["sUrlPath"];
$startPage = $arResult["nStartPage"];
?>
<ul class="pages__list">
	<?if ($navPageNomer > 1):?>
		<?if($arResult["bSavePage"]):?>
            <li class="pages__item pages__item_previous">
                <a href="<?=$url?>?<?=$strNavQueryString?>PAGEN_<?=$navNum?>=<?=($navPageNomer-1)?>">
                    <svg>
                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow-small"></use>
                    </svg>
                </a>
            </li>
			<li class="pages__item pages__item_text pages__item_text_left"><a href="<?=$url?>?<?=$strNavQueryString?>PAGEN_<?=$navNum?>=<?=($navPageNomer-1)?>"><?echo GetMessage("round_nav_back")?></a></li>
			<li class="pages__item"><a href="<?=$url?>?<?=$strNavQueryString?>PAGEN_<?=$navNum?>=1">1</a></li>
		<?else:?>
			<?if ($navPageNomer > 2):?>
                <li class="pages__item pages__item_previous">
                    <a href="<?=$url?>?<?=$strNavQueryString?>PAGEN_<?=$navNum?>=<?=($navPageNomer-1)?>">
                        <svg>
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow-small"></use>
                        </svg>
                    </a>
                </li>
				<li class="pages__item pages__item_text pages__item_text_left"><a href="<?=$url?>?<?=$strNavQueryString?>PAGEN_<?=$navNum?>=<?=($navPageNomer-1)?>"><?echo GetMessage("round_nav_back")?></a></li>
			<?else:?>
                <li class="pages__item pages__item_previous">
                    <a href="<?=$url?><?=$strNavQueryStringFull?>">
                        <svg>
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow-small"></use>
                        </svg>
                    </a>
                </li>
				<li class="pages__item pages__item_text pages__item_text_left"><a href="<?=$url?><?=$strNavQueryStringFull?>"><?echo GetMessage("round_nav_back")?></a></li>
			<?endif?>
			<li class="pages__item"><a href="<?=$url?><?=$strNavQueryStringFull?>">1</a></li>
		<?endif?>
	<?else:?>
            <li class="pages__item pages__item_previous pages__item_disabled">
                <a href="#previous">
                    <svg>
                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow-small"></use>
                    </svg>
                </a>
            </li>
			<li class="pages__item pages__item_current">1</li>
	<?endif?>

	<?
	$startPage++;
	while($startPage <= $arResult["nEndPage"]-1):
	?>
		<?if ($startPage == $navPageNomer):?>
			<li class="pages__item pages__item_current"><?=$startPage?></li>
		<?else:?>
			<li class="pages__item"><a href="<?=$url?>?<?=$strNavQueryString?>PAGEN_<?=$navNum?>=<?=$startPage?>"><?=$startPage?></a></li>
		<?endif?>
		<?$startPage++?>
	<?endwhile?>

	<?if($navPageNomer < $arResult["NavPageCount"]):?>
		<?if($arResult["NavPageCount"] > 1):?>
			<li class="pages__item"><a href="<?=$url?>?<?=$strNavQueryString?>PAGEN_<?=$navNum?>=<?=$arResult["NavPageCount"]?>"><?=$arResult["NavPageCount"]?></a></li>
		<?endif?>
            <li class="pages__item pages__item_text"><a href="<?=$url?>?<?=$strNavQueryString?>PAGEN_<?=$navNum?>=<?=($navPageNomer+1)?>"><?echo GetMessage("round_nav_forward")?></a></li>
            <li class="pages__item pages__item_next">
                <a href="<?=$url?>?<?=$strNavQueryString?>PAGEN_<?=$navNum?>=<?=($navPageNomer+1)?>">
                    <svg>
                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow-small"></use>
                    </svg>
                </a>
            </li>
	<?else:?>
		<?if($arResult["NavPageCount"] > 1):?>
			<li class="pages__item pages__item_current"><?=$arResult["NavPageCount"]?></li>
		<?endif?>
            <li class="pages__item pages__item_next pages__item_disabled">
                <a href="#page-next">
                    <svg>
                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow-small"></use>
                    </svg>
                </a>
            </li>
	<?endif?>
</ul>