<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="section section_backimage">
    <div class="section__container container">
        <div class="slider slider_banner" data-slider="banner">
            <div class="slider__controls" data-slider-controls="data-slider-controls">
                <button class="arrow arrow_prev" data-slider-controls-prev="data-slider-controls-prev">
                    <svg>
                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow"></use>
                    </svg>
                </button>
                <button class="arrow arrow_next" data-slider-controls-next="data-slider-controls-next">
                    <svg>
                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow"></use>
                    </svg>
                </button>
            </div>
            <ul class="slider__slides" data-slider-slides="data-slider-slides">
                <?foreach($arResult["ITEMS"] as $arItem):
                    $is_link = ($arItem["PROPERTIES"]["LINK"]["VALUE"]) ? 1 : 0;
                    ?>
                    <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
                        <li class="slider__slide">
                            <?if($is_link){?><a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"];?>"><?}?>
                            <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["DESCRIPTION"]?>"/>
                            <?if($is_link){?></a><?}?>
                        </li>
                    <?endif;?>
                <?endforeach;?>
            </ul>
        </div>
    </div>
</section>