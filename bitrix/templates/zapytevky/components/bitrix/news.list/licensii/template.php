<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h3 class="caption caption_padding">И сотрудничаем весьма успешно</h3>
<div class="slider slider_certificates" data-slider="six-items">
    <div class="slider__controls" data-slider-controls="data-slider-controls">
        <button class="arrow arrow_prev" data-slider-controls-prev="data-slider-controls-prev">
            <svg>
                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow"></use>
            </svg>
        </button>
        <button class="arrow arrow_next" data-slider-controls-next="data-slider-controls-next">
            <svg>
                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow"></use>
            </svg>
        </button>
    </div>
    <ul class="slider__slides" data-slider-slides="data-slider-slides">
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):
                $file_min = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width'=>182, 'height'=>250), BX_RESIZE_IMAGE_EXACT);
                /*$file_max = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width'=>182, 'height'=>250), BX_RESIZE_IMAGE_EXACT);

                if ($USER->IsAdmin() && $_SERVER["REMOTE_ADDR"] == "109.195.53.74")
                {
                    echo '<font style="text-align: left; font-size: 10px"><pre>';
                    print_r($file_max);
                    echo '</pre></font>';
                }*/
                $file_max = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]["ID"], array('width'=>1200, 'height'=>800), BX_RESIZE_IMAGE_PROPORTIONAL);
                ?>
                <li class="slider__slide">
                    <div class="item item_certificate" data-image="<?=$file_max["src"]?>">
                        <div class="item__image"><img src="<?=$file_min["src"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["DESCRIPTION"]?>"/></div>
                    </div>
                </li>
            <?endif;?>
        <?endforeach;?>
    </ul>
</div>