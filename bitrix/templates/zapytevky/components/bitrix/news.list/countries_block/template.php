<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if(!count($arResult)) return;
?>
<div class="countries" data-more-action="data-more-action">
    <ul class="countries__list">
        <?foreach($arResult["ITEMS"] as $arItem){
            $xml_id = $arItem["PROPERTIES"]["XML_ID"]["VALUE"];
            ?>
        <li class="countries__item" data-country="<?=$xml_id;?>">
            <a class="country" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                <div class="country__title">
                    <div class="country__flag">
                        <img src="https://static.sletat.ru/images/flags/<?=$xml_id;?>.png" alt="<?=$arItem["NAME"];?>"/>
                    </div>
                    <span><?=$arItem["NAME"];?></span>
                </div>
                <div class="country__price">
                    <!--<span>от <strong></strong> р</span>-->
                </div>
            </a>
        </li>
        <?}?>
    </ul>
    <button class="countries__more button button_size-s" data-toggle="Свернуть" data-more="data-more">
        <span>Еще страны</span>
    </button>
</div>