<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h2 class="title title_centered">Путешествуйте! Мы сделали это доступным!</h2>
<div class="feautures">
    <ul class="feautures__list">
        <?foreach($arResult["ITEMS"] as $arItem): ?>
        <li class="feautures__item">
            <div class="feauture">
                <div class="feauture__header">
                    <div class="feauture__ico">
                        <svg>
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-<?=$arItem["PROPERTIES"]["ICO"]["VALUE"];?>"></use>
                        </svg>
                    </div><span class="caption"><?=$arItem["NAME"]?></span>
                </div>
                <div class="feauture__content">
                    <p><?=$arItem["PREVIEW_TEXT"]?></p>
                </div>
            </div>
        </li>
        <?endforeach;?>
    </ul>
</div>