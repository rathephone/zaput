<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h3 class="caption caption_padding">Мы сотрудничаем с самыми крупными туроператорами</h3>
<div class="slider slider_operators" data-slider="full">
    <div class="slider__controls" data-slider-controls="data-slider-controls">
        <button class="arrow arrow_prev" data-slider-controls-prev="data-slider-controls-prev">
            <svg>
                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow"></use>
            </svg>
        </button>
        <button class="arrow arrow_next" data-slider-controls-next="data-slider-controls-next">
            <svg>
                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow"></use>
            </svg>
        </button>
    </div>
    <ul class="slider__slides" data-slider-slides="data-slider-slides">
        <li class="slider__slide">
            <? $c = 1;
            foreach($arResult["ITEMS"] as $arItem):
                if($c % 10 == 0){?></li><li class="slider__slide"><? $c = 1;}?>
                    <div class="item item_operator">
                        <div class="item__image"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["DESCRIPTION"]?>"/></div>
                    </div>
                <?
                $c++;
            endforeach;?>
        </li>
    </ul>
</div>