<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h3 class="caption caption_padding">Мы являемся партнером и спонсором наших друзей</h3>
<div class="slider slider_partners" data-slider="six-items">
    <div class="slider__controls" data-slider-controls="data-slider-controls">
        <button class="arrow arrow_prev" data-slider-controls-prev="data-slider-controls-prev">
            <svg>
                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow"></use>
            </svg>
        </button>
        <button class="arrow arrow_next" data-slider-controls-next="data-slider-controls-next">
            <svg>
                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow"></use>
            </svg>
        </button>
    </div>
    <ul class="slider__slides" data-slider-slides="data-slider-slides">
        <?foreach($arResult["ITEMS"] as $arItem):
        $is_link = (strlen($arItem["PROPERTIES"]["LINK"]["VALUE"])>0) ? true : false;
        ?>
            <li class="slider__slide">
                <a target="_blank" class="item item_partner<?if($is_link){?> item_hover<?}?>" <?if($is_link){?> href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"];?>"<?}?> >
                    <span class="item__image">
                        <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["DESCRIPTION"]?>"/>
                    </span>
                    <?if($is_link){?>
                        <span class="item__hoverlink">Читать подробнее</span>
                    <?}?>
                </a>
            </li>
        <?endforeach;?>
    </ul>
</div>