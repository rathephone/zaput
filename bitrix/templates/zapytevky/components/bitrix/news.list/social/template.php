<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="social">
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
    <a class="social__link" href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"];?>" target="_blank"><i class="ico">
            <svg>
                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-social-<?=$arItem["PROPERTIES"]["ICON"]["VALUE"];?>"></use>
            </svg>
        </i>
    </a>
    <? endforeach; ?>
</div>