<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$noEmptyAdres = (count($arResult["ITEMS"]) > 0) ? true : false;
$noFindAdres = (!count($arResult["ITEMS"]) && isset($_GET["q"])) ? true : false;
?>
<div class="section__container container" <?if($noEmptyAdres){?>data-tabs="contacts"<?}?>>
    <h3 class="caption caption_centered">Офисы в Санкт - Петербурге</h3>
    <div class="block block_form block_address">
        <form class="address form__address-contact" id="address-search">
            <div class="address__field">
                <label class="field field_size-wide">
                    <div class="field__input"><input type="text" placeholder="Введите адрес" id="suggest" value="<?=(isset($_GET["q"])) ? $_GET["q"] : "";?>"/>
                        <button class="field__btn field__btn_gray" type="submit" id="button_search">
                            <svg>
                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-search"></use>
                            </svg>
                        </button>
                    </div>
                </label>
            </div>
            <?if($noEmptyAdres){?>
                <div class="address__actions">
                    <button class="button button_ico" data-tab="0"><i class="ico">
                            <svg>
                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-list"></use>
                            </svg></i><span>Список</span>
                    </button>
                    <button class="button button_ico" data-tab="1"><i class="ico">
                            <svg>
                                <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-place"></use>
                            </svg></i><span>Карта</span>
                    </button>
                </div>
            <?}?>
        </form>
        <?$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            Array(
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "inc",
                "EDIT_TEMPLATE" => "",
                "PATH" => "/include/contact_text.php"
            )
        );?>
    </div>
    <?if($noEmptyAdres){?>
    <div data-tabs-content="contacts">
        <div data-tab="0">
            <div class="items" id="contacts-list">
                <?
                $is_show_more = true;
                ?>
                <? foreach ($arResult["ITEMS"] as $arItem):
                    $phone = $arItem["PROPERTIES"]["PHONE"]["VALUE"];
                    $phone_add = $arItem["PROPERTIES"]["PHONE_ADD"]["VALUE"];
                    $metro = '';
                    if(!empty($arItem['DISPLAY_PROPERTIES']['METRO']['VALUE']) && count($arItem['DISPLAY_PROPERTIES']['METRO']['VALUE']) > 0){
                        $arMetro = $arItem['DISPLAY_PROPERTIES']['METRO']['DISPLAY_VALUE'];
                        if(is_array($arMetro)){
                            foreach ($arMetro as $m){
                                $metro .= strip_tags($m) . '<br>';
                            }
                        } else {
                            $metro = strip_tags($arMetro);
                        }
                    }
                    ?>
                    <div class="office<?if($is_show_more){?> show-more<?}?>" data-print-content="data-print-content" data-more-action="data-more-action" data-officeid="<?=$arItem["ID"];?>">
                        <div class="office__header">
                            <div class="office__label">
                                <h4>Офис “<?= $arItem["NAME"]; ?>”</h4>
                            </div>
                            <div class="office__info">
                                <div class="office__block">
                                    <div class="office__title">
                                        <span><?= $arItem["NAME"] ?></span>
                                    </div>
                                    <p><?=$metro?></p>
                                </div>
                                <div class="office__block">
                                    <div class="office__caption"><i class="ico">
                                            <svg>
                                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-place"></use>
                                            </svg>
                                        </i><span>Адрес:</span>
                                    </div>
                                    <p class="office__address" data-address="data-address">Санкт-Петербург, <?= $arItem["PROPERTIES"]["ADRES"]["VALUE"]; ?></p>
                                </div>
                                <div class="office__block">
                                    <div class="office__caption"><i class="ico">
                                            <svg>
                                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-time"></use>
                                            </svg>
                                        </i><span>Режим работы:</span>
                                    </div>
                                    <p>
                                        <?= $arItem["PROPERTIES"]["WORKING"]["~VALUE"]["TEXT"]; ?>
                                    </p>
                                </div>
                                <div class="office__block">
                                    <div class="office__caption"><i class="ico">
                                            <svg>
                                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-tel"></use>
                                            </svg>
                                        </i><span>Телефон</span>
                                    </div>
                                    <p><span><strong><?= $phone; ?></strong> (доб. <?= $phone_add; ?>)</span></p>
                                </div>
                            </div>
                            <div class="office__more">
                                <button class="button button_highlight button_size-wide"
                                        data-toggle="<?if(!$is_show_more){?>Свернуть<?}else{?>Подробнее<?}?>"
                                        data-more="data-more"><span><?if($is_show_more){?>Свернуть<?$is_show_more=false;}else{?>Подробнее<?}?></span>
                                </button>
                            </div>
                        </div>
                        <div class="office__content">
                            <div class="office__title"><span><?= $arItem["NAME"]; ?></span>
                            </div>
                            <div class="office__info office__info_full">
                                        <? if ($arItem['GALLERY'] && count($arItem['GALLERY']) > 0) {
                                            ?>

                                            <div class="gallery office__gallery" data-slider="gallery">
                                                <div class="gallery__controls" data-slider-controls="data-slider-controls">
                                                    <button class="arrow arrow_prev" data-slider-controls-prev="data-slider-controls-prev">
                                                        <svg>
                                                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-arrow"></use>
                                                        </svg>
                                                    </button>
                                                    <button class="arrow arrow_next" data-slider-controls-next="data-slider-controls-next">
                                                        <svg>
                                                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-arrow"></use>
                                                        </svg>
                                                    </button>
                                                </div>
                                                <div>
                                                    <div class="gallery__view" data-modal-open="gallery">
                                                        <img src="#"/>
                                                    </div>
                                                    <div class="gallery__controls" data-slider-main-controls>
                                                        <button class="arrow arrow_prev" data-slider-controls-prev="data-slider-controls-prev">
                                                            <svg>
                                                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-arrow"></use>
                                                            </svg>
                                                        </button>
                                                        <button class="arrow arrow_next" data-slider-controls-next="data-slider-controls-next">
                                                            <svg>
                                                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-arrow"></use>
                                                            </svg>
                                                        </button>
                                                    </div>
                                                </div>
                                                <ul class="gallery__list" data-slider-slides="data-slider-slides">
                                                    <? $c = 0;
                                                    foreach ($arItem['GALLERY'] as $gallery) {
                                                        ?>
                                                        <li class="gallery__item" data-img="<?= $gallery["SRC"] ?>"><img src="<?= $gallery["SRC"] ?>"/></li>
                                                    <? } ?>
                                                </ul>
                                            </div>
                                        <? } ?>
                                    <?if(false){?>
                                    <div class="gallery gallery_office">
                                            <div class="gallery__count" data-gallery-count-current="1"
                                                 data-gallery-count-all="0">
                                            </div>
                                            <div class="gallery__controls" data-gallery-controls="data-gallery-controls">
                                                <button class="arrow arrow_prev" data-gallery-controls="0">
                                                    <svg>
                                                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-arrow"></use>
                                                    </svg>
                                                </button>
                                                <button class="arrow arrow_next" data-gallery-controls="1">
                                                    <svg>
                                                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-arrow"></use>
                                                    </svg>
                                                </button>
                                            </div>

                                            <div class="gallery__view" data-modal-open="gallery"><img src="#"/></div>
                                            <ul class="gallery__list"><?$c=0; foreach ($arItem['GALLERY'] as $gallery){?><li class="gallery__item" data-img="<?=$gallery["SRC"]?>" data-index="<?=$c;?>">
                                                        <img src="<?=$gallery["SRC"]?>"/>
                                                    </li><?$c++;}?>
                                            </ul>
                                    </div>
                                    <?}?>
                                <div class="office__description">
                                    <ul class="office__description-list">
                                        <li class="office__line">
                                            <div class="office__caption"><i class="ico">
                                                    <svg>
                                                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-place"></use>
                                                    </svg>
                                                </i><span>Адрес:</span>
                                            </div>
                                            <p>Санкт-Петербург, <?= $arItem["PROPERTIES"]["ADRES"]["VALUE"]; ?></p>
                                        </li>
                                        <? if ($arItem["PROPERTIES"]["LOCATION"]["~VALUE"]["TEXT"]) {
                                            ?>
                                            <li class="office__line">
                                                <div class="office__caption"><i class="ico">
                                                        <svg>
                                                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-place"></use>
                                                        </svg>
                                                    </i><span>Расположение:</span>
                                                </div>
                                                <p>
                                                    <?= $arItem["PROPERTIES"]["LOCATION"]["~VALUE"]["TEXT"]; ?>
                                                </p>
                                            </li>
                                        <? } ?>
                                        <li class="office__line">
                                            <div class="office__caption"><i class="ico">
                                                    <svg>
                                                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-time"></use>
                                                    </svg>
                                                </i><span>Режим работы:</span>
                                            </div>

                                            <p>
                                                <?= $arItem["PROPERTIES"]["WORKING"]["~VALUE"]["TEXT"]; ?>
                                            </p>

                                        </li>
                                    </ul>
                                    <? if ($arItem["PROPERTIES"]["WORKING_ADD"]["VALUE"]) {
                                        ?>
                                        <span><?= $arItem["PROPERTIES"]["WORKING_ADD"]["VALUE"] ?></span>
                                    <? } ?>
                                </div>
                                <? if ($arItem["PREVIEW_TEXT"]) {
                                    ?>
                                    <div class="office__text">
                                        <p>
                                            <?= $arItem["PREVIEW_TEXT"] ?>
                                        </p>
                                    </div>
                                <? } else { ?>
                                    <div class="office__text-empty"></div>
                                <? } ?>
                                <div class="office__footer">
                                    <?$namePhoneSet = trim($arItem["PROPERTIES"]["NAME_PHONE"]["VALUE"])?>
                                    <?if (strlen($namePhoneSet)):?>
                                        <p><?=$namePhoneSet?>:</p>
                                    <?else:?>
                                    <p>Специалист по страхованию финским визам и загранпаспортам:</p>
                                    <?endif;?>
                                    <?php
                                    $phone_a = preg_replace('~\D+~', '', $phone);
                                    ?>
                                    <a class="office__tel" href="tel:<?= $phone_a ?>"><?= $phone ?><strong>
                                            (доб. <?= $phone_add; ?>)</strong></a>

                                    <div class="office__actions">
                                        <button class="link" data-modal-open="map" data-coords="<?=$arItem["PROPERTIES"]["MAP_YANDEX"]["VALUE"];?>" data-id="<?=$arItem["ID"];?>"> <i class="ico">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-map"></use>
                                                </svg></i><span>Посмотреть на карте</span>
                                        </button>
                                        <button class="link" data-print="data-print"> <i class="ico">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-doc"></use>
                                                </svg></i><span>Распечатать</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <? endforeach; ?>
            </div>
        </div>
        <div data-tab="1">
            <div class="map map_contacts" id="map-contacts">
            </div>
        </div>
    </div>
    <?}else{
        if($noFindAdres){?>
            По запросу не найден ближайший офис
        <?}?>
    <?}?>
</div>
<div>
    <div id="js_map-contener">
        <script>
            window.ListLocation = <?=CUtil::PhpToJSObject($arResult["COORDS"]);?>;
            window.placemarks = <?=CUtil::PhpToJSObject($arResult["COORDS"]);?>;
            <?if (is_array($_REQUEST["userCoord"]) && count($_REQUEST["userCoord"])==2):?>
                window.placemarksUser = <?=CUtil::PhpToJSObject($_REQUEST["userCoord"]);?>;
            <?else:?>
                window.placemarksUser = [];
            <?endif;?>
            window.placemarksIds = <?=CUtil::PhpToJSObject($arResult['COORDS_IDS']);?>;
        </script>
    </div>
</div>
<?if($noEmptyAdres){?>
<script type="text/javascript">
    var mapContacts = document.querySelector('#map-contacts');
    if (mapContacts) {
        var _init = function() {
            $("#map-contacts").html("");
            var myMap = new ymaps.Map("map-contacts", {
                center: [53.340304, 83.752213],
                zoom: 12,
                controls: []
            });
            var suggestView = new ymaps.SuggestView('suggest',{
                provider: {
                    suggest:(function(request, options){
                        return ymaps.suggest("Санкт-Петербург " + request);
                })}
            });
            for (var i in window.placemarks) {
                if (window.placemarks.hasOwnProperty(i)) {
                    var placemark = new ymaps.Placemark(window.placemarks[i].coordPoint, {
                        balloonContentHeader: window.placemarks[i].title,
                        balloonContentBody: window.placemarks[i].content,
                        balloonContentFooter: '',
                        hintContent: window.placemarks[i].title
                    }, {
                        preset: "islands#icon",
                        iconColor: '#FF0E19'
                    });
                    myMap.geoObjects.add(placemark);
                }
            }
            if(window.placemarksUser.length) {
                var placemark = new ymaps.Placemark(window.placemarksUser, {
                    hintContent:  "Введенный адрес",
                    balloonContentBody: "Введенный адрес",
                }, {
                    preset: "islands#icon",
                    iconColor: '#31ff00'
                });
                myMap.geoObjects.add(placemark);
            }
            window.geocodeFunc = function() {
                // Забираем запрос из поля ввода.
                var request = $('#suggest').val();
                // Геокодируем введённые данные.
                ymaps.geocode(request).then(function (res) {
                    var obj = res.geoObjects.get(0),
                        error, hint;

                    if (obj) {
                    }
                    else {
                        error = 'Адрес не найден';
                        hint = 'Уточните адрес';
                    }
                    if (error) {

                    } else {
                        $coordLoc = obj.geometry._coordinates;
                        $listDistance = {};
                        $.each(window.ListLocation,function (key,elem) {
                            $coordOffice = elem.coordPoint;
                            $dist = Math.sqrt(Math.pow(parseFloat($coordOffice[0])-parseFloat($coordLoc[0]),2)+Math.pow(parseFloat($coordOffice[1])-parseFloat($coordLoc[1]),2));
                            $listDistance[elem.id] = $dist;
                        });
                        $.ajax({
                            href: "/adress/index.php",
                            data: {locDist: $listDistance,userCoord: $coordLoc, ajax: 'y'},
                            dataType: "html",
                            success: function (data) {
                                $("#contacts-list").html($("#contacts-list",data));
                                $("#js_map-contener").html($("#js_map-contener",data));
                                ///window.eval($("#js_map-contener",data).text());
                                _init();
                                window.MapContact.setCenter($coordLoc);
                                document.querySelectorAll('#contacts-list .gallery').forEach(function (gallery, i) {
                                    var count = gallery.querySelector('.gallery__count'),
                                        galleryListCount = gallery.querySelector('.gallery__list').children.length;
                                    if (count) {
                                        count.dataset.galleryCountAll = galleryListCount;
                                        gallery.querySelector('[data-gallery-controls]').addEventListener('click', function (e) {
                                            var direction = Number(e.target.closest('[data-gallery-controls]').dataset.galleryControls);
                                            var index = gallery.querySelector('.gallery__item_selected').dataset.index;

                                            switch (direction) {
                                                case 0:
                                                    if (index == 0) {
                                                        index = galleryListCount - 1;
                                                    } else {
                                                        --index;
                                                    }
                                                    break;
                                                case 1:
                                                    if (index == galleryListCount - 1) {
                                                        index = 0;
                                                    } else {
                                                        ++index;
                                                    }
                                                    break;
                                            }

                                            gallery.querySelector('.gallery__item[data-index="' + index + '"]').click();
                                        });
                                    }

                                    gallery.querySelector('.gallery__item:first-child').click();
                                });
                                document.querySelectorAll('[data-modal-open]').forEach(function (trigger, i) {
                                    trigger.addEventListener('click', function (e) {
                                        e.preventDefault();

                                        var t = e.target.closest('[data-modal-open]'),
                                            data = t.dataset.modalOpen,
                                            modalElement = document.querySelector('[data-modal="' + data + '"]'),
                                            scroll = $(window).scrollTop();

                                        if (data == 'gallery') {
                                            modalElement.querySelector('.modal__content').innerHTML = t.innerHTML;
                                        }

                                        var modalContent = modalElement.innerHTML;

                                        var modal = new tingle.modal({
                                            closeMethods: ['overlay', 'escape'],
                                            onClose: function onClose() {
                                                $(window).scrollTop(scroll);
                                                try {
                                                    this.remove();
                                                } catch (e) {
                                                }
                                            },
                                            cssClass: modalElement.classList
                                        });

                                        modal.setContent(modalContent);
                                        modal.open();

                                        if (data == 'map') {
                                            var coords = t.dataset.coords,
                                                contact_id = t.dataset.id;
                                            if(coords !== undefined && contact_id !== undefined && placemarksIds !== undefined) {
                                                var modalMap = new ymaps.Map($('.modal__content_map').get(0), {
                                                        center: coords.split(','),
                                                        zoom: 16,
                                                        controls: []
                                                    }),
                                                    myPlacemark = new ymaps.Placemark(placemarksIds[contact_id].coordPoint, {
                                                        balloonContentHeader: placemarksIds[contact_id].title,
                                                        balloonContentBody: placemarksIds[contact_id].content,
                                                        balloonContentFooter: '',
                                                        hintContent: placemarksIds[contact_id].title
                                                    }, {
                                                        preset: "islands#icon",
                                                        iconColor: '#FF0E19'
                                                    });
                                                modalMap.geoObjects.add(myPlacemark);
                                                $('.js_caption_map').text(placemarksIds[contact_id].title);
                                            }
                                        }

                                        if (data == 'gallery') {
                                            var g = e.target.closest('.gallery');

                                            modal.modalBoxContent.querySelectorAll('[data-gallery-controls]').forEach(function (arrow, k) {
                                                arrow.addEventListener('click', function (e) {
                                                    var direction = Number(e.target.closest('[data-gallery-controls]').dataset.galleryControls),
                                                        selected = g.querySelector('.gallery__item_selected');
                                                    var newSelected = void 0;

                                                    switch (direction) {
                                                        case 0:
                                                            newSelected = selected.previousElementSibling;

                                                            if (!newSelected) {
                                                                newSelected = selected.parentNode.children[selected.parentNode.children.length - 1];
                                                            }
                                                            break;

                                                        case 1:
                                                            newSelected = selected.nextElementSibling;

                                                            if (!newSelected) {
                                                                newSelected = selected.parentNode.children[0];
                                                            }
                                                            break;
                                                    }

                                                    var img = newSelected.dataset.img;
                                                    modal.modalBoxContent.querySelector('img').src = img;
                                                    selected.classList.remove('gallery__item_selected');
                                                    newSelected.classList.add('gallery__item_selected');
                                                });
                                            });
                                        }

                                        var forms = modal.modalBoxContent.querySelectorAll('form');

                                        forms.forEach(function (form, i) {
                                            form.querySelectorAll('select').forEach(function (select, i) {
                                                console.log(i);

                                                new CustomSelect({
                                                    elem: select
                                                });

                                                form.querySelector('.select button').addEventListener('click', function (e) {
                                                    e.preventDefault();
                                                });
                                            });
                                        });

                                        phoneMask();

                                        try {
                                            document.querySelector('.modal__close').addEventListener('click', function (e) {
                                                e.preventDefault();
                                                modal.close();
                                            });
                                        } catch (e) {
                                        }
                                    });
                                });
                            }
                        })
                    }
                })

            }
            myMap.setBounds(myMap.geoObjects.getBounds());
            myMap.setZoom(11);
            window.MapContact = myMap;
        };
        ymaps.ready(_init);
        $('#address-search').bind('submit', function (e) {
            window.geocodeFunc();
            return false;
        });
    }
</script>
<?}?>