<?php
$jsParams = [];
$jsParamMap = [];
if (is_array($arParams["SET_SORT"]) && count($arParams["SET_SORT"])){
    $newSort = array();
    foreach ($arParams["SET_SORT"] as $idElem) {
        foreach ($arResult['ITEMS'] as $i => $arItem) {
            if ($arItem["ID"] == $idElem){
                $newSort[] = $arItem;
            }
        }
    }
    $arResult['ITEMS'] = $newSort;
}
foreach ($arResult['ITEMS'] as $i => $arItem){
    if(!empty($arItem['PROPERTIES']['MAP_YANDEX']['VALUE'])) {
        $arCoords = explode(',', $arItem['PROPERTIES']['MAP_YANDEX']['VALUE']);

        $content = ($arItem["PROPERTIES"]["ADRES"]["VALUE"]) ? '<p><strong>Адрес: </strong> Санкт-Петербург, ' . $arItem["PROPERTIES"]["ADRES"]["VALUE"] . '</p>' : '';
        $content = ($arItem["PROPERTIES"]["WORKING"]["~VALUE"]["TEXT"]) ? $content . '<p><strong>Режим работы: </strong><br>' . strip_tags($arItem["PROPERTIES"]["WORKING"]["~VALUE"]["TEXT"]) . '</p>' : $content;
        $content = ($arItem["PROPERTIES"]["WORKING_ADD"]["VALUE"]) ? $content . '<p>' . $arItem["PROPERTIES"]["WORKING_ADD"]["VALUE"] . '</p>' : $content;
        $content = ($arItem["PROPERTIES"]["LOCATION"]["~VALUE"]["TEXT"]) ? $content . '<p><strong>Расположение: </strong>' . $arItem["PROPERTIES"]["LOCATION"]["~VALUE"]["TEXT"] . '</p>' : $content;
        $content .= '<p><strong>Телефон: </strong>' . $arItem["PROPERTIES"]["PHONE"]["VALUE"] . '<br><strong>E-mail:</strong> info@expert-spb.com</p>';

        $jsParams[] = [
            'coordPoint' => $arCoords,
            'title' => 'ЗаПутевкой.рф, Офис “'.$arItem["NAME"].'”',
            'content' => $content,
            'id' => $arItem["ID"]
        ];

        $jsParamMap[$arItem["ID"]] = [
            'coordPoint' => $arCoords,
            'title' => 'ЗаПутевкой.рф, Офис “'.$arItem["NAME"].'”',
            'content' => $content
        ];
    }

    $gallery = [];
    if(!empty($arItem['DISPLAY_PROPERTIES']['GALLERY'])){
        $c_gallery = count($arItem['DISPLAY_PROPERTIES']['GALLERY']['VALUE']);
        if($c_gallery > 0) {
            if ($c_gallery == 1) $gallery[] = $arItem['DISPLAY_PROPERTIES']['GALLERY']['FILE_VALUE'];
            else
                $gallery = $arItem['DISPLAY_PROPERTIES']['GALLERY']['FILE_VALUE'];
        }
    }
    $arResult['ITEMS'][$i]['GALLERY'] = $gallery;
}
$arResult['COORDS'] = $jsParams;
$arResult['COORDS_IDS'] = $jsParamMap;