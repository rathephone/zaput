<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h2 class="title title_centered">Дополнительная выгода для наших клиентов!</h2>
<div class="profit">
    <ul class="profit__list">
        <? $c = 1;
        foreach($arResult["ITEMS"] as $arItem):
            $is_link = ($arItem["PROPERTIES"]["LINK"]["VALUE"]) ? 1 : 0;
            ?>
            <li class="profit__item">
                <?if($is_link){?><a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"];?>"><?}?>
                    <div class="profit__block profit__block_type-<?=$c?>">
                        <div class="profit__block-inner">
                            <div class="profit__block-front"><span class="profit__block-title caption caption_size-l"><?=$arItem["NAME"]?></span><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"/>
                            </div>
                            <div class="profit__block-back">
                                <p><?=$arItem["DETAIL_TEXT"]?><br><strong><?=$arItem["PREVIEW_TEXT"]?></strong>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?if($is_link){?></a><?}?>
            </li>
            <?if($c == 1) $c = 2; else $c = 1;
        endforeach;?>
    </ul>
</div>