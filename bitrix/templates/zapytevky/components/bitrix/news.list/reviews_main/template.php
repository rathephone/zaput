<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h2 class="title">Отзывы о нашем агентстве</h2>
<div class="block">
    <div class="slider slider_reviews" data-slider="reviews">
        <div class="slider__controls" data-slider-controls="data-slider-controls">
            <button class="arrow arrow_prev" data-slider-controls-prev="data-slider-controls-prev">
                <svg>
                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow"></use>
                </svg>
            </button>
            <button class="arrow arrow_next" data-slider-controls-next="data-slider-controls-next">
                <svg>
                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow"></use>
                </svg>
            </button>
        </div>
        <ul class="slider__slides" data-slider-slides="data-slider-slides">
            <?for ($i = 0; $i <= count($arResult["ITEMS"]); $i++) {
                if($arResult["ITEMS"][$i]){
                    $item = $arResult["ITEMS"][$i];
                    $item2 = $arResult["ITEMS"][$i + 1];
                    $characters = 250;
                    ?>
                    <li class="slider__slide">
                        <div class="reviews">
                            <ul class="reviews__list">
                                <?if($item){?>
                                    <li class="reviews__item">
                                        <div class="r-item">
                                            <div class="r-item__content">
                                                <div class="r-item__text">
                                                    <p><?
                                                        if(strlen($item["PREVIEW_TEXT"]) > $characters) {
                                                            echo mb_substr($item["PREVIEW_TEXT"], 0 , $characters) . '...';
                                                        } else
                                                            echo $item["PREVIEW_TEXT"];
                                                        ?></p>
                                                </div><a class="r-item__link" href="/reviews">Читать отзыв</a>
                                            </div>
                                            <div class="r-item__footer">
                                                <div class="r-item__author star-<?=($item["DISPLAY_PROPERTIES"]["EVAL"]["VALUE_XML_ID"] == "good") ? 'good' : 'bad';?>"><span><?=$item["NAME"]?></span>
                                                </div><span class="r-item__date"><?=$item["DISPLAY_ACTIVE_FROM"]?></span>
                                            </div>
                                        </div>
                                    </li>
                                <?}?>
                                <?if($item2){?>
                                <li class="reviews__item">
                                    <div class="r-item">
                                        <div class="r-item__content">
                                            <div class="r-item__text">
                                                <p><?
                                                    if(strlen($item2["PREVIEW_TEXT"]) > $characters) {
                                                        echo mb_substr($item2["PREVIEW_TEXT"], 0 , $characters);
                                                    } else
                                                        echo $item2["PREVIEW_TEXT"];
                                                    ?></p>
                                            </div><a class="r-item__link" href="/reviews">Читать отзыв</a>
                                        </div>
                                        <div class="r-item__footer">
                                            <div class="r-item__author star-<?=($item2["DISPLAY_PROPERTIES"]["EVAL"]["VALUE_XML_ID"] == "good") ? 'good' : 'bad';?>"><span><?=$item2["NAME"]?></span>
                                            </div><span class="r-item__date"><?=$item2["DISPLAY_ACTIVE_FROM"]?></span>
                                        </div>
                                    </div>
                                </li>
                                <?}?>
                            </ul>
                        </div>
                    </li>
                    <?
                }
                $i++;
            }?>
        </ul>
    </div>
</div>