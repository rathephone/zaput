<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
    <div class="pages pages_reviews">
        <?=$arResult["NAV_STRING"]?>
    </div>
<?endif;?>

<div class="reviews">
    <ul class="reviews__list">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        $all_text = "";
        $city = ($arItem["DISPLAY_PROPERTIES"]["CITY"]["DISPLAY_VALUE"]) ? strip_tags($arItem["DISPLAY_PROPERTIES"]["CITY"]["DISPLAY_VALUE"]) : 0;
        $country = ($arItem["DISPLAY_PROPERTIES"]["COUNTRY"]["DISPLAY_VALUE"]) ? $arItem["DISPLAY_PROPERTIES"]["COUNTRY"]["DISPLAY_VALUE"] : 0;
        $resort = ($arItem["DISPLAY_PROPERTIES"]["RESORT"]["DISPLAY_VALUE"]) ? $arItem["DISPLAY_PROPERTIES"]["RESORT"]["DISPLAY_VALUE"] : 0;
        $hotel = ($arItem["DISPLAY_PROPERTIES"]["HOTEL"]["DISPLAY_VALUE"]) ? $arItem["DISPLAY_PROPERTIES"]["HOTEL"]["DISPLAY_VALUE"] : 0;
        if($city) $all_text .= $city . " / ";
        if($country) $all_text .= $country;
        if($resort) $all_text .= ', ' . $resort;
        if($hotel) $all_text .= ', ' . $hotel;
        $class_star = ($arItem["DISPLAY_PROPERTIES"]["EVAL"]["VALUE_XML_ID"] == "good") ? 'good' : 'bad';
        $showMore = false;
        $characters = 458;
        if(strlen($arItem["PREVIEW_TEXT"]) > $characters) {
            $showMore = true;
            $review = mb_substr($arItem["PREVIEW_TEXT"], 0 , $characters);
            $review .= '<span class="text-hide">' . mb_substr($arItem["PREVIEW_TEXT"], $characters) . '</span>';
        } else
            $review = $arItem["PREVIEW_TEXT"];
        ?>
            <li class="reviews__item reviews__item_page" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="review-page" data-more-action="data-more-action">
                    <div class="review-page__header">
                        <div class="review-page__title"><span class="review-page__caption"><?=$arItem["NAME"]?></span>
                            <span class="review-page__location"><?=$all_text?></span>
                        </div>
                        <div class="review-page__date"><span><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
                            <div class="grade grade_<?=$class_star?>">
                                <svg>
                                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-star"></use>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="review-page__content <?if(!$showMore){?>hide-more<?}else{?>drop-more<?}?>">
                        <div class="review-page__text">
                            <p>
                                <?=$review;?>
                            </p>
                        </div>
                        <div class="review-page__footer">
                            <button class="review-page__more" data-toggle="Свернуть" data-more="data-more"> <span>Развернуть</span></button>
                        </div>
                    </div>

                    <?if(!empty($arItem["DETAIL_TEXT"])){?>
                        <div class="review-page__answer">
                            <div class="review-page__header"><span class="review-page__caption">Ответ агенства Запутевкой.рф</span>
                            </div>
                            <div class="review-page__content">
                                <div class="review-page__text review-page__text_answer">
                                    <p><?=$arItem["DETAIL_TEXT"]?></p>
                                </div>
                            </div><span class="review-page__caption review-page__caption_answer">Руководство сети туристических агенств ЗаПутевкой.рф</span>
                        </div>
                    <?}?>
                </div>
            </li>
    <?endforeach;?>
    </ul>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <div class="pages pages_reviews">
        <?=$arResult["NAV_STRING"]?>
    </div>
<?endif;?>