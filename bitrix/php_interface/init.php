<?
define("IBLOCK_ID_CITIES", 9);
define("IBLOCK_ID_FEEDBACK", 8);
define("IBLOCK_ID_REVIEW", 10);
define("IBLOCK_ID_CONTACT", 11);
define("IBLOCK_ID_METRO", 12);
define("IBLOCK_ID_FEEDBACK_SELECT", 13);
define("IBLOCK_ID_COUNTRIES", 14);
define("IBLOCK_ID_COUNTRIES_SEO", 15);
define("IBLOCK_ID_CLAIM", 16);
define("IBLOCK_ORDER_ZAKAZ", 20);
if (strlen($_REQUEST["claimid"]) && $_REQUEST["sl_error"]!=true && $APPLICATION->GetCurDir() == "/"){
    $getClaimInfoParam = array(
        'request' => array(
            'ClaimIdentity' => $_REQUEST["claimid"]
        )
    );
    $soapClient = new SoapClient('https://claims.sletat.ru/xmlgate.svc?wsdl');

    $soapClient->__setSoapHeaders(
        new SoapHeader("urn:SletatRu:DataTypes:AuthData:v1", "AuthInfo",
            array(
                "Login" => 'sletat@6468822.ru',
                "Password" => 'xml@sletattest2019zp03'
            )));
    $data = $soapClient->getClaimInfo($getClaimInfoParam);
    if ($data->GetClaimInfoResult->RedirectToPaymentURL) header("Location: ".$data->GetClaimInfoResult->RedirectToPaymentURL);

}
require_once $_SERVER["DOCUMENT_ROOT"] . '/sletat/lib/Autoloader.php';
AddEventHandler('main', 'OnEpilog', '_Check404Error', 1);
function _Check404Error(){
    if (defined('ERROR_404') && ERROR_404 == 'Y') {
        global $APPLICATION;
        $APPLICATION->RestartBuffer();
        include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/header.php';
        include $_SERVER['DOCUMENT_ROOT'] . '/404.php';
        include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/footer.php';
    }
}
\Bitrix\Main\Loader::includeModule("iblock");
$res = CIBlockElement::GetList(array("SORT"=>"ASC"), array("IBLOCK_ID"=>IntVal(9), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y"));
while($ob = $res->GetNextElement()){
    $listCity[] = $ob->GetProperties()["CITY_ID_XML"]["VALUE"];
}
if ($_COOKIE["USER_SITY"]>0 && in_array($_COOKIE["USER_SITY"],$listCity)){
    define("USER_SITY",$_COOKIE["USER_SITY"]);
}
else{
    define("USER_SITY",1264);
}
if ($_COOKIE["TYPE_SORT"]>0){
    define("TYPE_SORT",$_COOKIE["TYPE_SORT"]);
}
else{
    define("TYPE_SORT",0);
}
define("DEFAULT_CURORT_COUNTRY",119);
define("DEFAULT_DAYS",10);
define("DEFAULT_COUNT_PEOPLE",2);
define("DEFAULT_NIGHT",7);
function CheckDiscount($params){
    $cache_dir = "discount";
    $cache = new CPHPCache();
    $applyDiscount = array();
    if ($cache->InitCache(3600,md5("discount"),$cache_dir)){
        $discount = $cache->GetVars();
    }
    elseif($cache->StartDataCache(3600,md5("discount"),$cache_dir)) {
        $filterDiscount = array(
            "ACTIVE" => "Y",
            "IBLOCK_ID" => 18,
            "PROPERTY_TYPE" => array(18, 19)
        );
        $res = CIBlockElement::GetList(array("SORT" => "ASC"), $filterDiscount);
        $return = true;
        $discount = array();
        while ($dataDiscount = $res->GetNextElement()) {
            $fields = $dataDiscount->GetFields();
            if (is_array($dataDiscount->GetProperty(57)["VALUE"])) $setFilterCountry = array_diff($dataDiscount->GetProperty(57)["VALUE"],array(""));
            else $setFilterCountry = array();

            if (is_array($dataDiscount->GetProperty(58)["VALUE"])) $setFilterCity = array_diff($dataDiscount->GetProperty(58)["VALUE"],array(""));
            else $setFilterCity = array();

            if (is_array($dataDiscount->GetProperty(59)["VALUE"])) $setFilterHotel = array_diff($dataDiscount->GetProperty(59)["VALUE"],array(""));
            else $setFilterHotel = array();

            if (is_array($dataDiscount->GetProperty(60)["VALUE"])) $setFilterTourOperator = array_diff($dataDiscount->GetProperty(60)["VALUE"],array(""));
            else $setFilterTourOperator = array();
            $discount[$fields["ID"]]["FILTER_PARAMS"] = array(
                "COUNTRY" => $setFilterCountry,
                "CITY" => $setFilterCity,
                "HOTEL" => $setFilterHotel,
                "TOUROPERATOR" => $setFilterTourOperator
            );
            $discount[$fields["ID"]]["DATA_DISCOUNT"] = array(
                "NAME" => $fields["NAME"],
                "TYPE" => $dataDiscount->GetProperty(55)["VALUE_ENUM_ID"],
                "VALUE" => trim($dataDiscount->GetProperty(56)["VALUE"]),
                "BREACK" => $dataDiscount->GetProperty(61)["VALUE_ENUM_ID"]
            );
        }
        global $CACHE_MANAGER;
        $CACHE_MANAGER->StartTagCache($cache_dir);
        $CACHE_MANAGER->RegisterTag("iblock_id_18");
        $CACHE_MANAGER->RegisterTag("iblock_id_new");
        $CACHE_MANAGER->EndTagCache();
        $cache->EndDataCache($discount);
    }
    foreach ($discount as $id => $dataDiscount){
        if (!is_array($dataDiscount["DATA_DISCOUNT"]["BREACK"])) $dataDiscount["DATA_DISCOUNT"]["BREACK"] = array();
        if (count($dataDiscount["FILTER_PARAMS"]["COUNTRY"])>0 && !in_array($params["COUNTRY"],$dataDiscount["FILTER_PARAMS"]["COUNTRY"])) continue;
        if (count($dataDiscount["FILTER_PARAMS"]["CITY"])>0 && !in_array($params["CITY"],$dataDiscount["FILTER_PARAMS"]["CITY"])) continue;
        if (count($dataDiscount["FILTER_PARAMS"]["HOTEL"])>0 && !in_array($params["HOTEL"],$dataDiscount["FILTER_PARAMS"]["HOTEL"])) continue;
        if (count($dataDiscount["FILTER_PARAMS"]["TOUROPERATOR"])>0 && !in_array($params["TOUROPERATOR"],$dataDiscount["FILTER_PARAMS"]["TOUROPERATOR"])) continue;
        $applyDiscount[] = $dataDiscount["DATA_DISCOUNT"];
        if (in_array(20,$dataDiscount["DATA_DISCOUNT"]["BREACK"])) break;
    }
    return $applyDiscount;
}

function CheckNacenka($params){
    $cache_dir = "nacenka";
    $cache = new CPHPCache();
    $applyNacenka = array();
    if ($cache->InitCache(3600,md5("nacenka"),$cache_dir)){
        $nacenka = $cache->GetVars();
    }
    elseif($cache->StartDataCache(3600,md5("nacenka"),$cache_dir)) {
        $filterNacenka = array(
            "ACTIVE" => "Y",
            "IBLOCK_ID" => 18,
            "PROPERTY_TYPE" => array(16,17)
        );
        $res = CIBlockElement::GetList(array("SORT" => "ASC"), $filterNacenka);
        $return = true;
        $nacenka = array();
        while ($dataNacenka = $res->GetNextElement()) {
            $fields = $dataNacenka->GetFields();
            if (is_array($dataNacenka->GetProperty(57)["VALUE"])) $setFilterCountry = array_diff($dataNacenka->GetProperty(57)["VALUE"],array(""));
            else $setFilterCountry = array();

            if (is_array($dataNacenka->GetProperty(58)["VALUE"])) $setFilterCity = array_diff($dataNacenka->GetProperty(58)["VALUE"],array(""));
            else $setFilterCity = array();

            if (is_array($dataNacenka->GetProperty(59)["VALUE"])) $setFilterHotel = array_diff($dataNacenka->GetProperty(59)["VALUE"],array(""));
            else $setFilterHotel = array();

            if (is_array($dataNacenka->GetProperty(60)["VALUE"])) $setFilterTourOperator = array_diff($dataNacenka->GetProperty(60)["VALUE"],array(""));
            else $setFilterTourOperator = array();
            $nacenka[$fields["ID"]]["FILTER_PARAMS"] = array(
                "COUNTRY" => $setFilterCountry,
                "CITY" => $setFilterCity,
                "HOTEL" => $setFilterHotel,
                "TOUROPERATOR" => $setFilterTourOperator
            );
            $nacenka[$fields["ID"]]["DATA_NACENKA"] = array(
                "NAME" => $fields["NAME"],
                "TYPE" => $dataNacenka->GetProperty(55)["VALUE_ENUM_ID"],
                "VALUE" => trim($dataNacenka->GetProperty(56)["VALUE"]),
                "BREACK" => $dataNacenka->GetProperty(61)["VALUE_ENUM_ID"]
            );
        }
        global $CACHE_MANAGER;
        $CACHE_MANAGER->StartTagCache($cache_dir);
        $CACHE_MANAGER->RegisterTag("iblock_id_18");
        $CACHE_MANAGER->RegisterTag("iblock_id_new");
        $CACHE_MANAGER->EndTagCache();
        $cache->EndDataCache($nacenka);
    }
    foreach ($nacenka as $id => $dataNacenkaParams){
        if (!is_array($dataNacenkaParams["DATA_NACENKA"]["BREACK"])) $dataNacenkaParams["DATA_NACENKA"]["BREACK"] = array();
        if (count($dataNacenkaParams["FILTER_PARAMS"]["COUNTRY"])>0 && !in_array($params["COUNTRY"],$dataNacenkaParams["FILTER_PARAMS"]["COUNTRY"])) continue;
        if (count($dataNacenkaParams["FILTER_PARAMS"]["CITY"])>0 && !in_array($params["CITY"],$dataNacenkaParams["FILTER_PARAMS"]["CITY"])) continue;
        if (count($dataNacenkaParams["FILTER_PARAMS"]["HOTEL"])>0 && !in_array($params["HOTEL"],$dataNacenkaParams["FILTER_PARAMS"]["HOTEL"])) continue;
        if (count($dataNacenkaParams["FILTER_PARAMS"]["TOUROPERATOR"])>0 && !in_array($params["TOUROPERATOR"],$dataNacenkaParams["FILTER_PARAMS"]["TOUROPERATOR"])) continue;
        $applyNacenka[$id] = $dataNacenkaParams["DATA_NACENKA"];
        if (in_array(21,$dataNacenkaParams["DATA_NACENKA"]["BREACK"])) break;
    }
    return $applyNacenka;
}

function CheckFixia($params){
    $cache_dir = "fixia";
    $cache = new CPHPCache();
    $applyFixia = array();
    if ($cache->InitCache(3600,md5("fixia"),$cache_dir)){
        $fixia = $cache->GetVars();
    }
    elseif($cache->StartDataCache(3600,md5("fixia"),$cache_dir)) {
        $filterFixia = array(
            "ACTIVE" => "Y",
            "IBLOCK_ID" => 18,
            "PROPERTY_TYPE" => array(22, 23)
        );
        $res = CIBlockElement::GetList(array("SORT" => "ASC"), $filterFixia);
        $return = true;
        $fixia = array();
        while ($dataFixia = $res->GetNextElement()) {
            $fields = $dataFixia->GetFields();
            if (is_array($dataFixia->GetProperty(57)["VALUE"])) $setFilterCountry = array_diff($dataFixia->GetProperty(57)["VALUE"],array(""));
            else $setFilterCountry = array();

            if (is_array($dataFixia->GetProperty(58)["VALUE"])) $setFilterCity = array_diff($dataFixia->GetProperty(58)["VALUE"],array(""));
            else $setFilterCity = array();

            if (is_array($dataFixia->GetProperty(59)["VALUE"])) $setFilterHotel = array_diff($dataFixia->GetProperty(59)["VALUE"],array(""));
            else $setFilterHotel = array();

            if (is_array($dataFixia->GetProperty(60)["VALUE"])) $setFilterTourOperator = array_diff($dataFixia->GetProperty(60)["VALUE"],array(""));
            else $setFilterTourOperator = array();
            $fixia[$fields["ID"]]["FILTER_PARAMS"] = array(
                "COUNTRY" => $setFilterCountry,
                "CITY" => $setFilterCity,
                "HOTEL" => $setFilterHotel,
                "TOUROPERATOR" => $setFilterTourOperator
            );
            $fixia[$fields["ID"]]["DATA_FIXIA"] = array(
                "NAME" => $fields["NAME"],
                "TYPE" => $dataFixia->GetProperty(55)["VALUE_ENUM_ID"],
                "VALUE" => trim($dataFixia->GetProperty(56)["VALUE"]),
                "BREACK" => $dataFixia->GetProperty(61)["VALUE_ENUM_ID"]
            );
        }
        global $CACHE_MANAGER;
        $CACHE_MANAGER->StartTagCache($cache_dir);
        $CACHE_MANAGER->RegisterTag("iblock_id_18");
        $CACHE_MANAGER->RegisterTag("iblock_id_new");
        $CACHE_MANAGER->EndTagCache();
        $cache->EndDataCache($fixia);
    }
    foreach ($fixia as $id => $dataFixia){
        if (!is_array($dataFixia["DATA_FIXIA"]["BREACK"])) $dataFixia["DATA_FIXIA"]["BREACK"] = array();
        if (count($dataFixia["FILTER_PARAMS"]["COUNTRY"])>0 && !in_array($params["COUNTRY"],$dataFixia["FILTER_PARAMS"]["COUNTRY"])) continue;
        if (count($dataFixia["FILTER_PARAMS"]["CITY"])>0 && !in_array($params["CITY"],$dataFixia["FILTER_PARAMS"]["CITY"])) continue;
        if (count($dataFixia["FILTER_PARAMS"]["HOTEL"])>0 && !in_array($params["HOTEL"],$dataFixia["FILTER_PARAMS"]["HOTEL"])) continue;
        if (count($dataFixia["FILTER_PARAMS"]["TOUROPERATOR"])>0 && !in_array($params["TOUROPERATOR"],$dataFixia["FILTER_PARAMS"]["TOUROPERATOR"])) continue;
        $applyFixia[] = $dataFixia["DATA_FIXIA"];
        if (in_array(24,$dataFixia["DATA_FIXIA"]["BREACK"])) break;
    }
    return $applyFixia;
}

function GetDiscount($dataTour){
    //$dataTour["CityFromId"] //CounryId //HotelId //SourceId
    $dataTour["OldPrice"] = 0;
    $dataTour["NewPrice"] = $dataTour["Price"];
    $city = false;
    if ($dataTour["CityFromId"]>0) $city = $dataTour["CityFromId"];
    elseif ($dataTour["SysCityFromId"]>0) $city = $dataTour["SysCityFromId"];

    $country = false;
    if ($dataTour["CounryId"]>0) $country = $dataTour["CounryId"];
    elseif ($dataTour["SysCountryId"]>0) $country = $dataTour["SysCountryId"];

    $hotel = false;
    if ($dataTour["HotelId"]>0) $hotel = $dataTour["HotelId"];
    elseif ($dataTour["SysHotelId"]>0) $hotel = $dataTour["SysHotelId"];

    $touroperator = false;
    if ($dataTour["SourceId"]>0) $touroperator = $dataTour["SourceId"];

    $filterTour = array(
        "CITY" => $city,
        "COUNTRY" => $country,
        "HOTEL" => $hotel,
        "TOUROPERATOR" => $touroperator
    );
    $listDiscountName = array();
    $breackDiscount = false;
    foreach (CheckFixia($filterTour) as $dataFixia){
        $listDiscountName[] = $dataFixia["NAME"];
        switch ($dataFixia["TYPE"]){
            case 22:
                if ($dataTour["OldPrice"] == 0) $dataTour["OldPrice"] = ceil($dataTour["NewPrice"]/(intval($dataFixia["VALUE"]) / 100));
                else $dataTour["OldPrice"] = ceil($dataTour["OldPrice"]/(intval($dataFixia["VALUE"]) / 100));
                break;
            case 23:
                if ($dataTour["OldPrice"] == 0) $dataTour["OldPrice"] = ceil($dataTour["NewPrice"]+intval($dataFixia["VALUE"]));
                else $dataTour["OldPrice"] = ceil($dataTour["OldPrice"]+intval($dataFixia["VALUE"]));
                break;
        }
        if (in_array(20,$dataFixia["BREACK"])) $breackDiscount = true;
        if (in_array(21,$dataFixia["BREACK"])) $breackNacenka = true;
    }
    if (!$breackNacenka) {
        foreach (CheckNacenka($filterTour) as $dataNacenka) {
            $listDiscountName[] = $dataNacenka["NAME"];
            switch ($dataNacenka["TYPE"]) {
                case 16:
                    $dataTour["NewPrice"] = ceil($dataTour["NewPrice"] * ((100 + intval($dataNacenka["VALUE"])) / 100));
                    break;
                case 17:
                    $dataTour["NewPrice"] = ceil($dataTour["NewPrice"] + intval($dataNacenka["VALUE"]));
                    break;
            }
            if (in_array(20, $dataNacenka["BREACK"])) $breackDiscount = true;
        }
    }
    if (!$breackDiscount) {
        foreach (CheckDiscount($filterTour) as $dataDiscount) {
            $listDiscountName[] = $dataDiscount["NAME"];
            switch ($dataDiscount["TYPE"]) {
                case 18:
                    if ($dataTour["OldPrice"] == 0) $dataTour["OldPrice"] = $dataTour["NewPrice"];
                    $dataTour["NewPrice"] = ceil($dataTour["NewPrice"] * ((100 - intval($dataDiscount["VALUE"])) / 100));
                    break;
                case 19:
                    if ($dataTour["OldPrice"] == 0) $dataTour["OldPrice"] = $dataTour["NewPrice"];
                    $dataTour["NewPrice"] = ceil($dataTour["NewPrice"] - intval($dataDiscount["VALUE"]));
                    break;
            }

        }
    }
    if (count($listDiscountName)) $dataTour["discpuntName"] = "Использованные правила скидок: ".implode(" / ",$listDiscountName);
    return $dataTour;
}
AddEventHandler("main", "OnPageStart", 'OnPageStartHandler');
function OnPageStartHandler(){
    global $APPLICATION;
    if (CSite::InDir("/bitrix/")) {
        CJSCore::Init(array("jquery2"));
    }
    if (CSite::InDir("/bitrix/admin/iblock_element_edit.php")) {
        Bitrix\Main\Page\Asset::getInstance()->addJs("/bitrix/templates/zapytevky/js/admin.js");
        $APPLICATION->SetAdditionalCSS("/bitrix/templates/zapytevky/css/admin.css");
    }
}
require_once 'typeprop/country.php';
require_once 'typeprop/hotel.php';
require_once 'typeprop/town.php';
require_once 'typeprop/turoperator.php';
function GetSletatCountry(){
    $cache = new CPHPCache();
    if ($cache->InitCache(3600,md5("GetSletatCountry123"),"/sletat/country")){
        return $cache->GetVars();
    }
    elseif($cache->StartDataCache(3600,md5("GetSletatCountry123"),"/sletat/country")) {
        $xml = new \sletatru\XmlGate([
            'login' => 'sletat@6468822.ru',
            'password' => 'xml@sletattest2019zp03',
        ]);
        $city = array();
        $res = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>IntVal(9), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y"), false, Array("nPageSize"=>50), Array("ID", "IBLOCK_ID", "NAME","PROPERTY_*"));
        while($ob = $res->GetNextElement()){
            $arProps = $ob->GetProperties();
            $city[] = $arProps["CITY_ID_XML"]["VALUE"];
        }
        $departCountry = array();
        foreach ($city as $idCity) {
            $dataGet = $xml->GetCountries($idCity);
            foreach ($dataGet as $dataCountry) {
                $dataAdd = array(
                    "NAME" => $dataCountry["Name"],
                    "CITY" => array($idCity)
                );
                if (!key_exists($dataCountry["Id"],$departCountry)) {
                    $departCountry[$dataCountry["Id"]] = $dataAdd;
                }
                else{
                    $departCountry[$dataCountry["Id"]]["CITY"][] = $idCity;
                }
            }
        }
        $cache->EndDataCache($departCountry);
        return $departCountry;
    }
}
function GetSletatCity(){
    $cache = new CPHPCache();
    if ($cache->InitCache(3600,md5("GetSletatCity"),"/sletat/city")){
        return $cache->GetVars();
    }
    elseif($cache->StartDataCache(3600,md5("GetSletatCity"),"/sletat/city")) {
        $res = CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>IntVal(9), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y"), false, Array("nPageSize"=>50), Array("ID", "IBLOCK_ID", "NAME","PROPERTY_*"));
        while($ob = $res->GetNextElement()){
            $arProps = $ob->GetProperties();
            $arFields = $ob->GetFields();
            $departCity[$arProps["CITY_ID_XML"]["VALUE"]] = $arFields["NAME"];
        }
        $cache->EndDataCache($departCity);
        return $departCity;
    }
}
function GetSletatTouroperator(){
    $cache = new CPHPCache();
    if ($cache->InitCache(3600,md5("GetSletatHotel"),"/sletat/touroperator")){
        return $cache->GetVars();
    }
    elseif($cache->StartDataCache(3600,md5("GetSletatHotel"),"/sletat/touroperator")) {
        $xml = new \sletatru\XmlGate([
            'login' => 'sletat@6468822.ru',
            'password' => 'xml@sletattest2019zp03',
        ]);
        $country = GetSletatCountry();
        $departTouroperator = array();
        foreach ($country as $idCountry=>$dataCountry) {
            foreach ($dataCountry["CITY"] as $idCity) {
                $dataGet = $xml->GetTourOperators($idCity,$idCountry);
                foreach ($dataGet as $dataTourOperator) {
                    if ($dataTourOperator["Enabled"]) {
                        $dataAdd = array(
                            "NAME" => $dataTourOperator["Name"],
                            "COUNTRY" => array($idCountry),
                            "CITY" => array($idCity),
                        );
                        if (!key_exists($dataTourOperator["Id"], $departTouroperator)) {
                            $departTouroperator[$dataTourOperator["Id"]] = $dataAdd;
                        }
                        else {
                            if (!in_array($idCity,$departTouroperator[$dataTourOperator["Id"]]["CITY"])){
                                $departTouroperator[$dataTourOperator["Id"]]["CITY"][] = $idCity;
                            }
                            if (!in_array($idCountry,$departTouroperator[$dataTourOperator["Id"]]["COUNTRY"])){
                                $departTouroperator[$dataTourOperator["Id"]]["COUNTRY"][] = $idCountry;
                            }
                        }
                    }
                }
            }
        }
        $cache->EndDataCache($departTouroperator);
        return $departTouroperator;
    }
}
function GetSletatHotel(){
    $cache = new CPHPCache();
    if ($cache->InitCache(3600,md5("GetSletatHotel"),"/sletat/hotel")){
        return $cache->GetVars();
    }
    elseif($cache->StartDataCache(3600,md5("GetSletatHotel"),"/sletat/hotel")) {
        $xml = new \sletatru\XmlGate([
            'login' => 'sletat@6468822.ru',
            'password' => 'xml@sletattest2019zp03',
        ]);
        $country = GetSletatCountry();
        foreach ($country as $idCountry=>$dataCountry) {
            $departHotel["COUNTRY"][$idCountry] = $dataCountry["NAME"];
            $dataGet = $xml->GetHotels($idCountry);
            foreach ($dataGet as $dataHotel) {
                $departHotel["HOTEL"][$idCountry][$dataHotel["Id"]] = $dataHotel["Name"];
            }
        }
        $cache->EndDataCache($departHotel);
        return $departHotel;
    }
}
class PBit{
    function getDeclination($count,$returnList){
        if ($returnList>=10 && $returnList<=19){
            return $returnList[2];
        }
        else{
            switch (substr((string)$count, -1)){
                case "0":
                case "1":
                    return $returnList[0];
                case "2":
                case "3":
                case "4":
                    return $returnList[1];
                case "5":
                case "6":
                case "7":
                case "8":
                case "9":
                    return $returnList[2];
            }
        }
    }

    static function mPage($arr_count, $arr_show_page, $curPage, $classPage = "")
    {
        $total = ceil($arr_count / $arr_show_page); // всего количество страниц

        $arrowLeft = '<svg><use xlink:href="' . SITE_TEMPLATE_PATH . '/img/symbols.svg#svg-ico-arrow-small"></use></svg>';
        $arrowRight = '<svg><use xlink:href="' . SITE_TEMPLATE_PATH . '/img/symbols.svg#svg-ico-arrow-small"></use></svg>';

        $html = '<ul class="pages__list '. $classPage .'">';
        $ext = ($curPage - 2) < 1 ? 1 : $curPage - 2;

        $html .= '
            <li data-page="'.($curPage - 1).'" class="pages__item pages__item_previous ' . ($curPage == 1 ? "pages__item_disabled" : "") . '">
                <a href="javascript:void(0);">'.$arrowLeft.'</a>
            </li>';
        if ($curPage > $total - 2 && $total > 5) $ext = $total - 4;

        if ($curPage > 3 && $total > 5)
            $html .= '
                    <li data-page="1" class="pages__item"><a href="javascript:void(0);">1</a></li>
                    <li class="pages__item"><span>...</span></li>';

        for ($i = $ext; $i <= $total && $i < ($curPage > 2 ? $curPage + 3 : 6); $i++) {
            $html .= '<li data-page="'.$i.'" ' . ($i == $curPage ? ' class="pages__item pages__item_current"' : ' class="pages__item"') . '>' . ($i == $curPage ? '<span>' : '<a href="javascript:void(0);">') . $i . ($i == $curPage ? '</span>' : '</a>') . '</li>';
        }
        if ($curPage < $total - 2 && $total > 5)
            $html .= '
                    <li class="pages__item"><span>...</span></li>
                    <li data-page="'.$total.'" class="pages__item"><a href="javascript:void(0);">' . $total . '</a></li>';

        $html .= '
            <li data-page="'.($curPage + 1).'" class="pages__item pages__item_next ' . ($curPage == $total ? "pages__item_disabled" : "") . '">
                <a href="javascript:void(0);">'.$arrowRight.'</a>
            </li>';
        $html .= '</ul>';
        return $html;
    }

    static function property_is_required($block_id, $code){
        if(\Bitrix\Main\Loader::includeModule("iblock")) {
            if (CIBlockProperty::GetByID($code, $block_id)->Fetch()['IS_REQUIRED'] == "Y")
                return "required";
            else
                return "";
        } else
            return "";
    }

    static function field_is_required($block_id, $code){
        if(\Bitrix\Main\Loader::includeModule("iblock")) {
            if (CIBlock::GetFields($block_id)[$code]['IS_REQUIRED'] == "Y")
                return "required";
            else
                return "";
        } else
            return "";
    }
}