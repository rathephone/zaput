<?
use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Type\Date,
    Bitrix\Iblock;
\Bitrix\Main\EventManager::getInstance()->addEventHandler('iblock', 'OnIBlockPropertyBuildList',
    array('PBHotelSletat', 'GetUserTypeDescription')
);
class PBHotelSletat
{
    const USER_TYPE = 'SletatHotel';

    public static function GetUserTypeDescription()
    {
        return array(
            "PROPERTY_TYPE" => Iblock\PropertyTable::TYPE_STRING,
            "USER_TYPE" => self::USER_TYPE,
            "DESCRIPTION" => "Отель",
            "GetPublicViewHTML" => array(__CLASS__, "GetPublicViewHTML"),
            "GetPublicEditHTML" => array(__CLASS__, "GetPublicEditHTML"),
            "GetAdminListViewHTML" => array(__CLASS__, "GetAdminListViewHTML"),
            "GetPropertyFieldHtml" => array(__CLASS__, "GetPropertyFieldHtml"),
            "CheckFields" => array(__CLASS__, "CheckFields"),
            "ConvertToDB" => array(__CLASS__, "ConvertToDB"),
            "ConvertFromDB" => array(__CLASS__, "ConvertFromDB"),
            "GetSettingsHTML" => array(__CLASS__, "GetSettingsHTML"),
            "GetAdminFilterHTML" => array(__CLASS__, "GetAdminFilterHTML"),
            "GetPublicFilterHTML" => array(__CLASS__, "GetPublicFilterHTML"),
            "AddFilterFields" => array(__CLASS__, "AddFilterFields"),
            "GetUIFilterProperty" => array(__CLASS__, "GetUIFilterProperty")
        );
    }

    public static function AddFilterFields($arProperty, $strHTMLControlName, &$arFilter, &$filtered)
    {

    }

    public static function GetAdminFilterHTML($arProperty, $strHTMLControlName)
    {

    }

    public static function GetPublicFilterHTML($arProperty, $strHTMLControlName)
    {

    }

    public static function GetPublicViewHTML($arProperty, $value, $strHTMLControlName)
    {
        if (strlen($value["VALUE"]) > 0)
        {
            if (!CheckDateTime($value["VALUE"]))
                $value = static::ConvertFromDB($arProperty, $value, $strHTMLControlName["DATETIME_FORMAT"]);

            if (isset($strHTMLControlName["MODE"]))
            {
                if ($strHTMLControlName["MODE"] == "CSV_EXPORT")
                    return $value["VALUE"];
                elseif ($strHTMLControlName["MODE"] == "SIMPLE_TEXT")
                    return $value["VALUE"];
                elseif ($strHTMLControlName["MODE"] == "ELEMENT_TEMPLATE")
                    return $value["VALUE"];
            }
            return str_replace(" ", "&nbsp;", htmlspecialcharsEx($value["VALUE"]));
        }

        return '';
    }

    public static function GetPublicEditHTML($arProperty, $value, $strHTMLControlName)
    {
        /** @var CMain */
    }

    public static function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName)
    {
        $listHotel = GetSletatHotel();
        foreach ($listHotel["HOTEL"] as $arHotel){
            foreach ($arHotel as $idHotel => $dataHotel) {
                if ($idHotel == $value["VALUE"]) return $dataHotel;
            }
        }
    }
    public static function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {
        if ($_REQUEST["ID"]>0) {
            $countrySelect = CIBlockElement::GetByID($_REQUEST["ID"])->GetNextElement()->GetProperty(57)["VALUE"];
            if (!is_array($countrySelect)) $countrySelect = array();
            if (count($countrySelect) > 0) {
                $dataHotelAll = GetSletatHotel();
                $s = "<select onchange='updSelectSity()' name='".$strHTMLControlName["VALUE"]."'>";
                $s .= "<option></option>";
                foreach ($dataHotelAll["HOTEL"] as $idCountry => $arHotel) {
                    if (in_array($idCountry, $countrySelect)) {
                        $s .= "<optgroup label='" . $dataHotelAll["COUNTRY"][$idCountry] . "'>";
                        foreach ($arHotel as $idHotel => $dataHotel) {
                            if ($value["VALUE"] == $idHotel) {
                                $s .= "<option selected value='$idHotel'>" . $dataHotel . "</option>";
                            } else {
                                $s .= "<option value='$idHotel'>" . $dataHotel . "</option>";
                            }
                        }
                        $s .= "</optgroup>";
                    }
                }
                $s .= "</select>";
            }
            else $s = "Для отображения списка выбирите страны и сохраните";

        }
        else {
            $s = "Для отображения списка выбирите страны и сохраните";
        }
        return $s;
    }

    public static function CheckFields($arProperty, $value)
    {
        $arResult = array();
        return $arResult;
    }

    public static function ConvertToDB($arProperty, $value)
    {
        return $value;
    }

    public static function ConvertFromDB($arProperty, $value, $format = '')
    {
        return $value;
    }

    public static function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
    {
        $arPropertyFields = array(
            "HIDE" => array("ROW_COUNT", "COL_COUNT"),
        );

        return '';
    }
}
?>