<?
use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Type\Date,
    Bitrix\Iblock;
\Bitrix\Main\EventManager::getInstance()->addEventHandler('iblock', 'OnIBlockPropertyBuildList',
    array('PBTownSletat', 'GetUserTypeDescription')
);
class PBTownSletat
{
    const USER_TYPE = 'SletatTown';

    public static function GetUserTypeDescription()
    {
        return array(
            "PROPERTY_TYPE" => Iblock\PropertyTable::TYPE_STRING,
            "USER_TYPE" => self::USER_TYPE,
            "DESCRIPTION" => "Город вылета",
            "GetPublicViewHTML" => array(__CLASS__, "GetPublicViewHTML"),
            "GetPublicEditHTML" => array(__CLASS__, "GetPublicEditHTML"),
            "GetAdminListViewHTML" => array(__CLASS__, "GetAdminListViewHTML"),
            "GetPropertyFieldHtml" => array(__CLASS__, "GetPropertyFieldHtml"),
            "CheckFields" => array(__CLASS__, "CheckFields"),
            "ConvertToDB" => array(__CLASS__, "ConvertToDB"),
            "ConvertFromDB" => array(__CLASS__, "ConvertFromDB"),
            "GetSettingsHTML" => array(__CLASS__, "GetSettingsHTML"),
            "GetAdminFilterHTML" => array(__CLASS__, "GetAdminFilterHTML"),
            "GetPublicFilterHTML" => array(__CLASS__, "GetPublicFilterHTML"),
            "AddFilterFields" => array(__CLASS__, "AddFilterFields"),
            "GetUIFilterProperty" => array(__CLASS__, "GetUIFilterProperty")
        );
    }

    public static function AddFilterFields($arProperty, $strHTMLControlName, &$arFilter, &$filtered)
    {

    }

    public static function GetAdminFilterHTML($arProperty, $strHTMLControlName)
    {

    }

    public static function GetPublicFilterHTML($arProperty, $strHTMLControlName)
    {

    }

    public static function GetPublicViewHTML($arProperty, $value, $strHTMLControlName)
    {
        if (strlen($value["VALUE"]) > 0)
        {
            if (!CheckDateTime($value["VALUE"]))
                $value = static::ConvertFromDB($arProperty, $value, $strHTMLControlName["DATETIME_FORMAT"]);

            if (isset($strHTMLControlName["MODE"]))
            {
                if ($strHTMLControlName["MODE"] == "CSV_EXPORT")
                    return $value["VALUE"];
                elseif ($strHTMLControlName["MODE"] == "SIMPLE_TEXT")
                    return $value["VALUE"];
                elseif ($strHTMLControlName["MODE"] == "ELEMENT_TEMPLATE")
                    return $value["VALUE"];
            }
            return str_replace(" ", "&nbsp;", htmlspecialcharsEx($value["VALUE"]));
        }

        return '';
    }

    public static function GetPublicEditHTML($arProperty, $value, $strHTMLControlName)
    {
        /** @var CMain */
    }

    public static function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName)
    {
        $listCity = GetSletatCity();
        return $listCity[$value["VALUE"]];
    }
    public static function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {
        $s = "<select onchange='updSelectSity()' name='".$strHTMLControlName["VALUE"]."'>";
        $s .= "<option></option>";
        foreach (GetSletatCity() as $cityID => $arCity){
            if ($value["VALUE"]==$cityID){
                $s .= "<option selected data-select-city='$cityID' value='$cityID'>" . $arCity . "</option>";
            }
            else {
                $s .= "<option data-select-city='$cityID' value='$cityID'>" . $arCity . "</option>";
            }
        }
        $s .= "</select>";
        return $s;
    }

    public static function CheckFields($arProperty, $value)
    {
        $arResult = array();
        return $arResult;
    }

    public static function ConvertToDB($arProperty, $value)
    {
        return $value;
    }

    public static function ConvertFromDB($arProperty, $value, $format = '')
    {
        return $value;
    }

    public static function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
    {
        $arPropertyFields = array(
            "HIDE" => array("ROW_COUNT", "COL_COUNT"),
        );

        return '';
    }
}
?>