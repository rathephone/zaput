<?
use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Type\Date,
    Bitrix\Iblock;
use Bitrix\Main\ObjectException;
use Bitrix\Main\Type\DateTime;

\Bitrix\Main\EventManager::getInstance()->addEventHandler('iblock', 'OnIBlockPropertyBuildList',
    array('PBCountrySletat', 'GetUserTypeDescription')
);
class PBCountrySletat
{
    const USER_TYPE = 'SletatCountry';

    public static function GetUserTypeDescription()
    {
        return array(
            "PROPERTY_TYPE" => Iblock\PropertyTable::TYPE_STRING,
            "USER_TYPE" => self::USER_TYPE,
            "DESCRIPTION" => "Страна",
            "GetPublicViewHTML" => array(__CLASS__, "GetPublicViewHTML"),
            "GetPublicEditHTML" => array(__CLASS__, "GetPublicEditHTML"),
            "GetAdminListViewHTML" => array(__CLASS__, "GetAdminListViewHTML"),
            "GetPropertyFieldHtml" => array(__CLASS__, "GetPropertyFieldHtml"),
            "CheckFields" => array(__CLASS__, "CheckFields"),
            "ConvertToDB" => array(__CLASS__, "ConvertToDB"),
            "ConvertFromDB" => array(__CLASS__, "ConvertFromDB"),
            "GetSettingsHTML" => array(__CLASS__, "GetSettingsHTML"),
            "GetAdminFilterHTML" => array(__CLASS__, "GetAdminFilterHTML"),
            "GetPublicFilterHTML" => array(__CLASS__, "GetPublicFilterHTML"),
            "AddFilterFields" => array(__CLASS__, "AddFilterFields"),
            "GetUIFilterProperty" => array(__CLASS__, "GetUIFilterProperty")
        );
    }
    public static function AddFilterFields($arProperty, $strHTMLControlName, &$arFilter, &$filtered)
    {

    }

    public static function GetAdminFilterHTML($arProperty, $strHTMLControlName)
    {

    }

    public static function GetPublicFilterHTML($arProperty, $strHTMLControlName)
    {

    }

    public static function GetPublicViewHTML($arProperty, $value, $strHTMLControlName)
    {
        return 'test';
    }

    public static function GetPublicEditHTML($arProperty, $value, $strHTMLControlName)
    {
        /** @var CMain */
    }

    public static function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName)
    {
        $listCountry = GetSletatCountry();
        return $listCountry[$value["VALUE"]]["NAME"];
    }
    public static function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
    {
        $s = "<select onchange='updSelectCountry()' name='".$strHTMLControlName["VALUE"]."'>";
        $s .= "<option></option>";
        foreach (GetSletatCountry() as $countryID => $arCountry){
            if ($value["VALUE"]==$countryID){
                $s .= "<option selected data-select-country='$countryID' data-if-city='" . implode("|", $arCountry["CITY"]) . "' value='$countryID'>" . $arCountry["NAME"] . "</option>";
            }
            else {
                $s .= "<option data-select-country='$countryID' data-if-city='" . implode("|", $arCountry["CITY"]) . "' value='$countryID'>" . $arCountry["NAME"] . "</option>";
            }
        }
        $s .= "</select>";
        return $s;
    }

    public static function CheckFields($arProperty, $value)
    {
        $arResult = array();
        return $arResult;
    }

    public static function ConvertToDB($arProperty, $value)
    {
        return $value;
    }

    public static function ConvertFromDB($arProperty, $value, $format = '')
    {
        return $value;
    }

    public static function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields)
    {
        $arPropertyFields = array(
            "HIDE" => array("ROW_COUNT", "COL_COUNT"),
        );

        return '';
    }
}
?>