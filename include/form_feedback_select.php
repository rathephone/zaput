<div class="sidebar__header sidebar__header_highlight">
    <span class="caption caption_size-s">Сомневаетесь в выборе?</span>
    <span>Мы поможем вам подобрать тур!</span>
</div>
<div class="sidebar__content">
    <form class="form form__feedback-select">
        <?=bitrix_sessid_post();?>
        <div class="form__feedback-error field__caption"></div>
        <label class="field field_<?=PBit::property_is_required(IBLOCK_ID_FEEDBACK_SELECT, "NAME")?>"><span class="field__caption">Как к Вам обращаться?</span>
            <div class="field__input"><input type="text" placeholder="Введите имя" name="NAME" <?=PBit::property_is_required(IBLOCK_ID_FEEDBACK_SELECT, "NAME")?>/>
            </div>
        </label>
        <label class="field field_<?=PBit::property_is_required(IBLOCK_ID_FEEDBACK_SELECT, "PHONE")?>"><span class="field__caption">Ваш контактный телефон:</span>
            <div class="field__input"><input type="tel" placeholder="+7 (___) ___ - __ __" name="PHONE" <?=PBit::property_is_required(IBLOCK_ID_FEEDBACK_SELECT, "PHONE")?>/>
            </div>
        </label>
        <label class="field field_<?=PBit::field_is_required(IBLOCK_ID_FEEDBACK_SELECT, "PREVIEW_TEXT")?>"><span class="field__caption">Ваши пожелания</span>
            <div class="field__input field__input_text"><textarea placeholder="Комментарий" name="PREVIEW_TEXT" <?=PBit::field_is_required(IBLOCK_ID_FEEDBACK_SELECT, "PREVIEW_TEXT")?>></textarea>
            </div>
        </label>
        <button type="submit" class="button button_full"><span>Отправить заявку</span></button>
    </form>
</div>