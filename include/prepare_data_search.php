<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require_once $_SERVER["DOCUMENT_ROOT"] . '/sletat/lib/Autoloader.php';
$request = \Bitrix\Main\Context::getCurrent()->getRequest();
$CACHE = new CPHPCache();
$SELECT_TOUR = NULL;
if ($request->get("CODE")){
    if ($selectTour = CIBlockElement::GetList(array(),array("CODE" => $request->get("CODE"), "IBLOCK_ID" => 14))->GetNext()){
        $res = CIBlockElement::GetProperty($selectTour["IBLOCK_ID"],$selectTour["ID"]);
        while ($dataProp = $res->GetNext()) {
            if ($dataProp["CODE"] != "XML_ID") continue;
            $SELECT_TOUR = $dataProp["VALUE"];
        }
    }
}
$xml = new \sletatru\XmlGate([
    'login' => 'sletat@6468822.ru',
    'password' => 'xml@sletattest2019zp03',
]);
if ($CACHE->InitCache(36000,md5("GetCountries".USER_SITY),'searchtur/contries')){
    $departCountry = $CACHE->GetVars();
}
elseif($CACHE->StartDataCache(36000,md5("GetCountries".USER_SITY),'searchtur/contries')) {
    $dataGet = $xml->GetCountries(USER_SITY);
    foreach ($dataGet as $dataCountry) {
        $departCountry[$dataCountry["Id"]] = $dataCountry;
    }
    $CACHE->EndDataCache($departCountry);
}
$SELECT = array();

if ($request->get("country")>0){
    $SELECT["COUNTRY"]["TEXT"] = $departCountry[$request->get("country")]["Name"];
    $SELECT["COUNTRY"]["VALUE"] = $request->get("country");
}
elseif ($SELECT_TOUR>0){
    $SELECT["COUNTRY"]["TEXT"] = $departCountry[$SELECT_TOUR]["Name"];
    $SELECT["COUNTRY"]["VALUE"] = $SELECT_TOUR;
}
else{
    $SELECT["COUNTRY"]["TEXT"] = $departCountry[DEFAULT_CURORT_COUNTRY]["Name"];
    $SELECT["COUNTRY"]["VALUE"] = DEFAULT_CURORT_COUNTRY;
}
if ($CACHE->InitCache(36000,md5("GetTourOperators".print_r(array(USER_SITY, $SELECT["COUNTRY"]["VALUE"]),true)),'searchtur/operator')){
    $departOperator = $CACHE->GetVars();
}
elseif($CACHE->StartDataCache(36000,md5("GetTourOperators".print_r(array(USER_SITY, $SELECT["COUNTRY"]["VALUE"]),true)),'searchtur/operator')) {
    $dataGet = $xml->GetTourOperators(USER_SITY, $SELECT["COUNTRY"]["VALUE"]);
    foreach ($dataGet as $dataOperator) {
        if ($dataOperator["Enabled"]) $departOperator[$dataOperator["Id"]] = array_intersect_key($dataOperator,array_flip(array("Id","Name")));
    }
    $CACHE->EndDataCache($departOperator);
}
if (is_array($request->get("operator")) && count($request->get("operator"))){
    $allDataArray = array();
    foreach ($request->get("operator") as $idOperator) if (key_exists($idOperator,$departOperator)) $allDataArray[] = $departOperator[$idOperator]["Name"];
    $SELECT["OPERATOR"]["TEXT"] = implode(" / ",$allDataArray);
}
else{
    $SELECT["OPERATOR"]["TEXT"] = "Все";
}
usort($departOperator,function ($a,$b){
    if ($a["Name"]<$b["Name"]) return -1;
    if ($a["Name"]>$b["Name"]) return 1;
    return 0;
});
array_unshift($departOperator,array('Id' => '', 'Name' => "Все"));
$SELECT["OPERATOR"]["VARIANT"] = $departOperator;
if (is_array($request->get("operator")) && count($request->get("operator"))){
    $SELECT["OPERATOR"]["VALUE"] = $request->get("operator");
    if (count($SELECT["OPERATOR"]["VALUE"]) == 1 && $SELECT["OPERATOR"]["VALUE"][0] == ""){
        $SELECT["OPERATOR"]["VALUE"] = array();
    }
}
else{
    $SELECT["OPERATOR"]["VALUE"] = array();
}
if (is_array($request->get("date")) && count(is_array($request->get("date")))){
    $SELECT["DATE"]["VALUE"]["MIN"] = $request->get("date")["min"];
    $SELECT["DATE"]["VALUE"]["MAX"] = $request->get("date")["max"];
}
else{
    $SELECT["DATE"]["VALUE"]["MIN"] = strtotime(date("d.m.Y"))*1000;
    $SELECT["DATE"]["VALUE"]["MAX"] = strtotime(date("d.m.Y",time()+86400*DEFAULT_DAYS))*1000;
}
if ($SELECT["DATE"]["VALUE"]["MIN"] == $SELECT["DATE"]["VALUE"]["MAX"]){
    $SELECT["DATE"]["TEXT"] = date("d.m",$SELECT["DATE"]["VALUE"]["MIN"]/1000);
}
else{
    $SELECT["DATE"]["TEXT"] = date("d.m",$SELECT["DATE"]["VALUE"]["MIN"]/1000)." - ".date("d.m",$SELECT["DATE"]["VALUE"]["MAX"]/1000);
}
if (!$request->get("country") || $request->get("SEARCH") == "Y"){
    $SELECT["PERELET"]["VALUE"] = "Y";
}
if ($request->get("inc_perelet")){
    $SELECT["PERELET"]["VALUE"] = "Y";
}
if ($request->get("people")>0){
    $SELECT["PEOPLE"]["VALUE"] = $request->get("people");
}
else{
    $SELECT["PEOPLE"]["VALUE"] = DEFAULT_COUNT_PEOPLE;
}
if ($SELECT["PEOPLE"]["VALUE"] == 1){
    $SELECT["PEOPLE"]["TEXT"] = $SELECT["PEOPLE"]["VALUE"]." Взрослый";
}
else{
    $SELECT["PEOPLE"]["TEXT"] = $SELECT["PEOPLE"]["VALUE"]." Взрослых";
}
if (is_array($request->get("children")) && count(is_array($request->get("children")))){
    $SELECT["CHILDREN"]["VALUE"] = $request->get("children");
}
else{
    $SELECT["CHILDREN"]["VALUE"] = array();
}
$SELECT["CHILDREN"]["TEXT"] = "";
if (count($SELECT["CHILDREN"]["VALUE"])==1){
    $SELECT["CHILDREN"]["TEXT"] = "1 Ребенок";
}
elseif(count($SELECT["CHILDREN"]["VALUE"])>1){
    $SELECT["CHILDREN"]["TEXT"] = count($SELECT["CHILDREN"]["VALUE"])." Детей";
}
if ($request->get("night")){
    $SELECT["NIGHT"]["VALUE"] = $request->get("night");
}
else{
    $SELECT["NIGHT"]["VALUE"] = DEFAULT_NIGHT;
}
if ($request->get("night_pom")){
    if (($SELECT["NIGHT"]["VALUE"]-2)<1){
        $setStart = 1;
    }
    else{
        $setStart = $SELECT["NIGHT"]["VALUE"]-2;
    }
    $SELECT["NIGHT"]["TEXT"] = $setStart."-".($SELECT["NIGHT"]["VALUE"]+2)." ".PBit::getDeclination($SELECT["NIGHT"]["VALUE"],array("Ночь","Ночи","Ночей"));
    $SELECT["NIGHT_SEARCH"]["VALUE"]["MIN"] = $setStart;
    $SELECT["NIGHT_SEARCH"]["VALUE"]["MAX"] = $SELECT["NIGHT"]["VALUE"]+2;
}
else{
    $SELECT["NIGHT"]["TEXT"] = ($SELECT["NIGHT"]["VALUE"])." ".PBit::getDeclination($SELECT["NIGHT"]["VALUE"],array("Ночь","Ночи","Ночей"));
    $SELECT["NIGHT_SEARCH"]["VALUE"]["MIN"] = $SELECT["NIGHT_SEARCH"]["VALUE"]["MAX"] = $SELECT["NIGHT"]["VALUE"];
}
if (is_array($request->get("classmeals")) && count($request->get("classmeals"))){
    foreach ($request->get("classmeals") as $val)
        $SELECT["MIALS"]["VALUE"][] = (int)$val;
}
else{
    $SELECT["MIALS"]["VALUE"] = array();
}
if (is_array($request->get("classhotel")) && count($request->get("classhotel"))){
    foreach ($request->get("classhotel") as $val)
    $SELECT["CLASS_HOTEL"]["VALUE"][] = (int)$val;
}
else{
    $SELECT["CLASS_HOTEL"]["VALUE"] = array();
}
if($SELECT["COUNTRY"]["VALUE"]>0){
    if ($CACHE->InitCache(36000,md5("GetCities".$SELECT["COUNTRY"]["VALUE"]),'searchtur/contries')){
        $departTown = $CACHE->GetVars();
    }
    elseif($CACHE->StartDataCache(36000,md5("GetCities".$SELECT["COUNTRY"]["VALUE"]),'searchtur/contries')) {
        $dataGet = $xml->GetCities($SELECT["COUNTRY"]["VALUE"]);
        foreach ($dataGet as $dataTown){
            $departTown[$dataTown["Id"]] = array_intersect_key($dataTown,array_flip(array("Id","Name")));
        }
        $CACHE->EndDataCache($departTown);
    }
    if (is_array($request->get("town")) && count($request->get("town"))){
        $allDataArray = array();
        foreach ($request->get("town") as $idTownd) if (key_exists($idTownd,$departTown)) $allDataArray[] = $departTown[$idTownd]["Name"];
        $SELECT["TOWN"]["TEXT"] = implode(" / ",$allDataArray);
    }
    else{
        $SELECT["TOWN"]["TEXT"] = "Все";
    }
    usort($departTown,function ($a,$b){
        if ($a["IsPopular"] == true && $b["IsPopular"] == false) return -1;
        if ($a["IsPopular"] == false && $b["IsPopular"] == true) return 1;
        if ($a["Name"]<$b["Name"]) return -1;
        if ($a["Name"]>$b["Name"]) return 1;
        return 0;
    });
    array_unshift($departTown,array('Id' => '', 'Name' => "Все"));
    $SELECT["TOWN"]["VARIANT"] = $departTown;
    if (is_array($request->get("town")) && count($request->get("town"))){
        $SELECT["TOWN"]["VALUE"] = $request->get("town");
        if (count($SELECT["TOWN"]["VALUE"]) == 1 && $SELECT["TOWN"]["VALUE"][0] == ""){
            $SELECT["TOWN"]["VALUE"] = array();
        }
    }
    else{
        $SELECT["TOWN"]["VALUE"] = array();
    }
    if ($CACHE->InitCache(36000,md5("GetHotels".print_r(array($SELECT["COUNTRY"]["VALUE"],$SELECT["TOWN"]["VALUE"],$SELECT["CLASS_HOTEL"]["VALUE"]),true)),'searchtur/hotel')){
        $departHotel = $CACHE->GetVars();
    }
    elseif($CACHE->StartDataCache(36000,md5("GetHotels".print_r(array($SELECT["COUNTRY"]["VALUE"],$SELECT["TOWN"]["VALUE"],$SELECT["CLASS_HOTEL"]["VALUE"]),true)),'searchtur/hotel')) {
        $dataGet = $xml->GetHotels($SELECT["COUNTRY"]["VALUE"],$SELECT["TOWN"]["VALUE"],$SELECT["CLASS_HOTEL"]["VALUE"]);
        foreach ($dataGet as $dataHotel){
            $departHotel[$dataHotel["Id"]] = array_intersect_key($dataHotel,array_flip(array("Id","Name")));
        }
        $CACHE->EndDataCache($departHotel);
    }
    if (is_array($request->get("hotel")) && count($request->get("hotel"))){
        $allDataArray = array();
        foreach ($request->get("hotel") as $idTownd) if (key_exists($idTownd,$departHotel)) $allDataArray[] = $departHotel[$idTownd]["Name"];
        $SELECT["HOTEL"]["TEXT"] = implode(" / ",$allDataArray);
    }
    else{
        $SELECT["HOTEL"]["TEXT"] = "Все";
    }
    usort($departHotel,function ($a,$b){
        if ($a["PopularityLevel"]>$b["PopularityLevel"]) return -1;
        if ($a["PopularityLevel"]<$b["PopularityLevel"]) return 1;
        if ($a["Name"]<$b["Name"]) return -1;
        if ($a["Name"]>$b["Name"]) return 1;
        return 0;
    });
    array_unshift($departHotel,array('Id' => '', 'Name' => "Все"));
    $SELECT["HOTEL"]["VARIANT"] = $departHotel;
    if (is_array($request->get("hotel")) && count($request->get("hotel"))){
        $SELECT["HOTEL"]["VALUE"] = $request->get("hotel");
        if (count($SELECT["HOTEL"]["VALUE"]) == 1 && $SELECT["HOTEL"]["VALUE"][0] == ""){
            $SELECT["HOTEL"]["VALUE"] = array();
        }
    }
    else{
        $SELECT["HOTEL"]["VALUE"] = array();
    }
}
if ($request->get("AJAX_PAGE")=="Y"){
    echo json_encode($SELECT);
}
else{
    ?>
    <script>
        window.SearchTour = <?=json_encode($SELECT)?>;
        setDataSearch();
    </script>
    <?
}