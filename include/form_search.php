<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require $_SERVER["DOCUMENT_ROOT"]."/include/prepare_data_search.php";
?>
<?
/***************************************/
$arSelect = Array("ID", "IBLOCK_ID", "NAME","PROPERTY_*");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
$arFilter = Array("IBLOCK_ID"=>IntVal(9), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, Array("nPageSize"=>50), $arSelect);
while($ob = $res->GetNextElement()){
    $arFields = $ob->GetFields();
    $arProps = $ob->GetProperties();
    $fromSity[]=array(
        "name"    =>$arFields["NAME"],
        "val_id" =>$arProps["CITY_ID_XML"]["VALUE"]
    );
}
/***************************************/
?>
<?$pageOpen = $APPLICATION->GetCurPage()?>
<form <?if (preg_match("/(\/search-tour|\/tury-)/ui",$pageOpen)):?>onsubmit="return false;"<?endif;?> class="search js__search__form" action="/search-tour/"  method="post">
    <div class="search__header">
        <div class="search__block"><span class="search__location">Откуда:
                <div class="select select_link">
                  <select style="display: none">
                      <?if(is_array($fromSity) && count($fromSity)>0):?>
                          <?foreach ($fromSity as $itemSity):?>
                              <option value="<?=$itemSity["val_id"]?>" <?if ($itemSity["val_id"] == USER_SITY):?>selected<?endif;?>><?=$itemSity["name"]?></option>
                          <?endforeach;?>
                      <?endif;?>
                  </select>
                </div></span>
        </div>
        <div class="search__block"><a class="link" href="/search-tour"> <span>Расширенный поиск</span><i class="ico">
                <svg>
                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-filter"></use>
                </svg></i>
            </a>
        </div>
    </div>
    <div class="search__form">
        <div class="search__options">
            <div class="search__select select" data-drop="data-drop" data-name="COUNTRY">
                <input type="text" class="js-Dropdown-title" data-prev="">
                <div class="select__dropdown max_height ">
                    <span class="select__caption">Популярные:</span>
                    <?$curortPopular = array("Турция","Тунис","Кипр","Греция","Таиланд"); ?>
                    <ul class="options">
                        <? foreach ($departCountry as $itemDepartCity): ?>
                            <?if (!in_array($itemDepartCity["Name"],$curortPopular)) continue?>
                            <li class="options__item">
                                <label class="checkbox">
                                    <input name="country" <?if ($SELECT_TOUR == $itemDepartCity["Id"]):?>checked<?endif;?> value="<?= $itemDepartCity["Id"] ?>" type="radio"/>
                                    <div class="checkbox__box"></div>
                                    <div class="checkbox__content"><p><?= $itemDepartCity["Name"] ?></p></div>
                                </label>
                            </li>
                        <?endforeach;?>
                    </ul>
                    <span class="select__caption">Остальные курорты:</span>
                    <ul class="options" id="search_tur_list">
                        <? foreach ($departCountry as $itemDepartCity): ?>
                            <?if (in_array($itemDepartCity["Name"],$curortPopular)) continue?>
                            <li class="options__item">
                                <label class="checkbox">
                                    <input name="country" <?if ($SELECT_TOUR == $itemDepartCity["Id"]):?>checked<?endif;?> value="<?= $itemDepartCity["Id"] ?>" type="radio"/>
                                    <div class="checkbox__box"></div>
                                    <div class="checkbox__content">
                                        <p><?= $itemDepartCity["Name"] ?></p>
                                    </div>
                                </label>
                            </li>
                        <?endforeach;?>
                    </ul>
                </div>
            </div>
            <div class="search__select select select_size-s" data-drop="data-drop" data-name="DATE">
                <input type="hidden" id="data_search_start" name="departFrom" value="">
                <input type="hidden" id="data_search_stop" name="departTo" value="">
                <button class="js-Dropdown-title">

                </button>
                <input type="hidden" name="date[min]" value=""/>
                <input type="hidden" name="date[max]" />
                <div class="select__dropdown select__dropdown_fix">
                    <div class="calendar">
                        <div class="calendar__header">
                            <div class="calendar__months">
                                <div class="calendar__progress">
                                </div>
                                <ul class="calendar__months-list">
                                    <li class="calendar__months-item">
                                    </li>
                                    <li class="calendar__months-item">
                                    </li>
                                    <li class="calendar__months-item">
                                    </li>
                                    <li class="calendar__months-item">
                                    </li>
                                    <li class="calendar__months-item">
                                    </li>
                                    <li class="calendar__months-item">
                                    </li>
                                    <li class="calendar__months-item">
                                    </li>
                                    <li class="calendar__months-item">
                                    </li>
                                    <li class="calendar__months-item">
                                    </li>
                                    <li class="calendar__months-item">
                                    </li>
                                    <li class="calendar__months-item">
                                    </li>
                                    <li class="calendar__months-item">
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="calendar__content">
                            <div class="calendar__controls">
                                <button class="calendar__arrow calendar__arrow_prev" data-calendar-controls="0">
                                    <svg>
                                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-arrow-small"></use>
                                    </svg>
                                </button>
                                <button class="calendar__arrow calendar__arrow_next" data-calendar-controls="2">
                                    <svg>
                                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-arrow-small"></use>
                                    </svg>
                                </button>
                            </div>
                            <ul class="calendar__list">
                                <li class="calendar__item">
                                    <div class="month">
                                    </div>
                                </li>
                                <li class="calendar__item">
                                    <div class="month">
                                    </div>
                                </li>
                                <li class="calendar__item">
                                    <div class="month">
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="calendar__footer">

                        </div>
                    </div>
                </div>
            </div>
            <div class="search__select select" data-drop="data-drop" id="search-people" data-name="PEOPLE">
                <button class="js-Dropdown-title">
                </button>
                <div class="select__dropdown">
                    <div class="counter">
                        <button class="counter__btn counter__btn_minus" data-counter-control="0">
                            <svg>
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-minus"></use>
                            </svg>
                        </button>
                        <input type="hidden" data-counter-value-input name="people" value="">
                        <div class="counter__value" data-counter-value="0"><span>Взрослых:</span>
                        </div>
                        <button class="counter__btn counter__btn_plus" data-counter-control="1">
                            <svg>
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-plus"></use>
                            </svg>
                        </button>
                    </div>
                    <div class="selector" data-selector="data-selector"><input class="selector__input" type="text"/>
                        <ul class="selector__list">
                        </ul>
                        <div class="toggle">
                            <button class="toggle__header"><span>Добавить ребенка</span>
                            </button>
                            <div class="toggle__content">
                                <div class="toggle__block">
                                    <ul class="toggle__list">
                                        <li class="toggle__list-item" data-children="1" data-value="Ребенок 1 год"><span>1 год</span>
                                        </li>
                                        <li class="toggle__list-item" data-children="2" data-value="Ребенок 2 года"><span>2 года</span>
                                        </li>
                                        <li class="toggle__list-item" data-children="3" data-value="Ребенок 3 года"><span>3 года</span>
                                        </li>
                                        <li class="toggle__list-item" data-children="4" data-value="Ребенок 4 года"><span>4 года</span>
                                        </li>
                                        <li class="toggle__list-item" data-children="5" data-value="Ребенок 5 лет"><span>5 лет</span>
                                        </li>
                                        <li class="toggle__list-item" data-children="6" data-value="Ребенок 6 лет"><span>6 лет</span>
                                        </li>
                                    </ul>
                                </div>
                                <div class="toggle__block">
                                    <ul class="toggle__list">
                                        <li class="toggle__list-item" data-children="7" data-value="Ребенок 7 лет"><span>7 лет</span>
                                        </li>
                                        <li class="toggle__list-item" data-children="8" data-value="Ребенок 8 лет"><span>8 лет</span>
                                        </li>
                                        <li class="toggle__list-item" data-children="9" data-value="Ребенок 9 лет"><span>9 лет</span>
                                        </li>
                                        <li class="toggle__list-item" data-children="10" data-value="Ребенок 10 лет"><span>10 лет</span>
                                        </li>
                                        <li class="toggle__list-item" data-children="11" data-value="Ребенок 11 лет"><span>11 лет</span>
                                        </li>
                                        <li class="toggle__list-item" data-children="12" data-value="Ребенок 12 лет"><span>12 лет</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="search__select select select_size-s" data-drop="data-drop" data-name="NIGHT">
                <button class="js-Dropdown-title">
                    <span class='placeholder requare'>Страна назначения</span>
                    <div class="select_val">
                        <input type="hidden" name="count[people]">
                        <div id="child"></div>
                        <div class="text"></div>
                    </div>
                </button>
                <div class="select__dropdown">
                    <div class="counter counter_btns">
                        <button class="counter__btn counter__btn_minus" data-counter-control="0">
                            <svg>
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-minus"></use>
                            </svg>
                        </button>
                        <input type="hidden" data-counter-value-input name="night" value="">
                        <div class="counter__value counter__value_hidden"></div>
                        <button class="counter__btn counter__btn_plus" data-counter-control="1">
                            <svg>
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-plus"></use>
                            </svg>
                        </button>
                    </div>
                    <div class="select__line">
                        <label class="checkbox checkbox_dropdown">
                            <input name="night_pom" type="checkbox"/>
                            <div class="checkbox__box"></div>
                            <span class="checkbox__span">±2</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="search__submit">
            <button class="button button_highlight button_size-l button_full" name="SEARCH" value="Y" onclick="searchTur(true);"><span>Поиск</span>
            </button>
        </div>
    </div>
</form>