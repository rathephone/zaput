<div class="modal modal_size-s modal__feedback" data-modal="order">
    <button class="modal__close">
    </button>
    <header class="modal__head"><span class="caption caption_size-l">Оставить заявку</span>
    </header>
    <div class="modal__content">
        <form class="form form_modal form__feedback">
            <?=bitrix_sessid_post();?>
            <div class="form__feedback-error field__caption"></div>
            <div class="form__line">
                <label class="field field_size-full"><span class="field__caption">Ваше имя</span>
                    <div class="field__input"><input type="text" placeholder="Иванов Иван Иванович" name="NAME" <?=PBit::property_is_required(IBLOCK_ID_FEEDBACK, "NAME")?>/>
                    </div>
                </label>
            </div>
            <div class="form__line">
                <label class="field field_size-full"><span class="field__caption">Ваш e-mail</span>
                    <div class="field__input"><input type="email" placeholder="example@mail.com" name="EMAIL" <?=PBit::property_is_required(IBLOCK_ID_FEEDBACK, "EMAIL")?>/>
                    </div>
                </label>
            </div>
            <div class="form__line">
                <label class="field field_size-full"><span class="field__caption">Контактный телефон</span>
                    <div class="field__input"><input type="tel" placeholder="+7 ( ___ ) - ___ - __ - __" data-mask="+# (###) ###-####" name="PHONE" <?=PBit::property_is_required(IBLOCK_ID_FEEDBACK, "PHONE")?>/>
                    </div>
                </label>
            </div>
            <div class="form__text">
                <p>
                    Отправка заявки ни к чему не обязывает и не является бронированием.
                    Получив заявку, менеджер свяжется с Вами для уточнения деталей
                </p>
            </div>
            <div class="form__submit">
                <button type="submit" class="button button_highlight button_full form__feedback-submit"><span>Отправить заявку</span>
                </button>
            </div>
        </form>
    </div>
</div>
