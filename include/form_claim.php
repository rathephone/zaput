<?php
use Bitrix\Main\Loader;
Loader::includeModule("iblock");
$arClaim = [];
$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>IBLOCK_ID_CLAIM, "CODE"=>"TITLE_CLAIM"));
while($enum_fields = $property_enums->GetNext()) $arClaim[$enum_fields["ID"]] = $enum_fields["VALUE"];
?>

<div class="modal modal_size-s" data-modal="claim">
    <button class="modal__close">
    </button>
    <header class="modal__head"><span class="caption caption_size-l">Написать директору</span></header>
    <div class="modal__content">
        <form class="form form_modal form__claim">
            <?=bitrix_sessid_post();?>
            <div class="form__feedback-error field__caption"></div>
            <div class="form__line">
                <label class="field field_size-full"><span class="field__caption">Ваше имя</span>
                    <div class="field__input"><input type="text" placeholder="Иванов Иван Иванович" name="NAME" <?=PBit::property_is_required(IBLOCK_ID_CLAIM, "NAME")?>/>
                    </div>
                </label>
            </div>
            <div class="form__line">
                <label class="field field_size-full"><span class="field__caption">Ваш телефон</span>
                    <div class="field__input"><input type="tel" placeholder="+7 ( ___ ) - ___ - __ - __" data-mask="+# (###) ###-####" name="PHONE" <?=PBit::property_is_required(IBLOCK_ID_CLAIM, "PHONE")?>/>
                    </div>
                </label>
            </div>
            <?if(count($arClaim) > 0){?>
            <div class="form__line">
                <div class="field field_size-full"><span class="field__caption">Тема обращения</span>
                    <div class="select select_field select_full select_fsize-s">
                        <select name="TITLE_CLAIM" <?=PBit::property_is_required(IBLOCK_ID_CLAIM, "TITLE_CLAIM")?>>
                            <?foreach ($arClaim as $id => $claim){ ?>
                                <option value="<?=$id?>"><?=$claim?></option>
                            <?}?>
                        </select>
                    </div>
                </div>
            </div>
            <?}?>
            <div class="form__line">
                <label class="field field_size-full"><span class="field__caption">Тема обращения</span>
                    <div class="field__input field__input_text">
                        <textarea placeholder="Комментарий" name="PREVIEW_TEXT" <?=PBit::field_is_required(IBLOCK_ID_CLAIM, "PREVIEW_TEXT")?>></textarea>
                    </div>
                </label>
            </div>
            <div class="form__submit">
                <button type="submit" class="button button_highlight button_full"><span>Отправить заявку</span>
                </button>
            </div>
        </form>
    </div>
</div>
