<div class="sidebar__block"><span class="sidebar__caption">Курорты:</span>
    <div class="select select_field select select_fsize-s" data-drop="data-drop">
        <button class="js-Dropdown-title">Все
        </button>
        <div class="select__dropdown"><span class="select__caption">Популярные:</span>
            <ul class="options">
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>Аланья
                            </p>
                        </div>
                    </label>
                </li>
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>Анталья
                            </p>
                        </div>
                    </label>
                </li>
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>Белек
                            </p>
                        </div>
                    </label>
                </li>
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>Бодрум
                            </p>
                        </div>
                    </label>
                </li>
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>Даламан
                            </p>
                        </div>
                    </label>
                </li>
            </ul><span class="select__caption">Все курорты:</span>
            <label class="field">
                <div class="field__input"><input type="text" placeholder="Поиск по курорту"/>
                </div>
            </label>
            <ul class="options">
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>Аланья
                            </p>
                        </div>
                    </label>
                </li>
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>Анталья
                            </p>
                        </div>
                    </label>
                </li>
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>Белек
                            </p>
                        </div>
                    </label>
                </li>
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>Бодрум
                            </p>
                        </div>
                    </label>
                </li>
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>Даламан
                            </p>
                        </div>
                    </label>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="sidebar__block"><span class="sidebar__caption">Питание:</span>
    <ul class="options">
        <li class="options__item">
            <label class="checkbox">
                <input type="checkbox"/>
                <div class="checkbox__box">
                </div>
                <div class="checkbox__content">
                    <p>Любое
                    </p>
                </div>
            </label>
        </li>
        <li class="options__item">
            <label class="checkbox">
                <input type="checkbox"/>
                <div class="checkbox__box">
                </div>
                <div class="checkbox__content">
                    <p>UAI - Ультра все включено
                    </p>
                </div>
            </label>
        </li>
        <li class="options__item">
            <label class="checkbox">
                <input type="checkbox"/>
                <div class="checkbox__box">
                </div>
                <div class="checkbox__content">
                    <p>AI - Всё включено
                    </p>
                </div>
            </label>
        </li>
        <li class="options__item">
            <label class="checkbox">
                <input type="checkbox"/>
                <div class="checkbox__box">
                </div>
                <div class="checkbox__content">
                    <p>BB – Завтрак
                    </p>
                </div>
            </label>
        </li>
        <li class="options__item">
            <label class="checkbox">
                <input type="checkbox"/>
                <div class="checkbox__box">
                </div>
                <div class="checkbox__content">
                    <p>FB - Завтрак, обед, ужин
                    </p>
                </div>
            </label>
        </li>
        <li class="options__item">
            <label class="checkbox">
                <input type="checkbox"/>
                <div class="checkbox__box">
                </div>
                <div class="checkbox__content">
                    <p>HB - Завтрак + ужин
                    </p>
                </div>
            </label>
        </li>
        <li class="options__item">
            <label class="checkbox">
                <input type="checkbox"/>
                <div class="checkbox__box">
                </div>
                <div class="checkbox__content">
                    <p>RO - Без питания
                    </p>
                </div>
            </label>
        </li>
    </ul>
</div>
<div class="sidebar__block"><span class="sidebar__caption">Класс отеля:</span>
    <ul class="options">
        <li class="options__item">
            <label class="checkbox">
                <input type="checkbox"/>
                <div class="checkbox__box">
                </div>
                <div class="checkbox__content">
                    <p>Любая
                    </p>
                </div>
            </label>
        </li>
        <li class="options__item">
            <label class="checkbox checkbox_rating">
                <input type="checkbox"/>
                <div class="checkbox__box">
                </div>
                <div class="checkbox__content"><i class="ico ico_star">
                        <svg>
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-star2"></use>
                        </svg></i>
                </div>
            </label>
        </li>
        <li class="options__item">
            <label class="checkbox checkbox_rating">
                <input type="checkbox"/>
                <div class="checkbox__box">
                </div>
                <div class="checkbox__content"><i class="ico ico_star">
                        <svg>
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-star2"></use>
                        </svg></i><i class="ico ico_star">
                        <svg>
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-star2"></use>
                        </svg></i>
                </div>
            </label>
        </li>
        <li class="options__item">
            <label class="checkbox checkbox_rating">
                <input type="checkbox"/>
                <div class="checkbox__box">
                </div>
                <div class="checkbox__content"><i class="ico ico_star">
                        <svg>
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-star2"></use>
                        </svg></i><i class="ico ico_star">
                        <svg>
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-star2"></use>
                        </svg></i><i class="ico ico_star">
                        <svg>
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-star2"></use>
                        </svg></i>
                </div>
            </label>
        </li>
        <li class="options__item">
            <label class="checkbox checkbox_rating">
                <input type="checkbox"/>
                <div class="checkbox__box">
                </div>
                <div class="checkbox__content"><i class="ico ico_star">
                        <svg>
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-star2"></use>
                        </svg></i><i class="ico ico_star">
                        <svg>
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-star2"></use>
                        </svg></i><i class="ico ico_star">
                        <svg>
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-star2"></use>
                        </svg></i><i class="ico ico_star">
                        <svg>
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-star2"></use>
                        </svg></i>
                </div>
            </label>
        </li>
        <li class="options__item">
            <label class="checkbox checkbox_rating">
                <input type="checkbox"/>
                <div class="checkbox__box">
                </div>
                <div class="checkbox__content"><i class="ico ico_star">
                        <svg>
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-star2"></use>
                        </svg></i><i class="ico ico_star">
                        <svg>
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-star2"></use>
                        </svg></i><i class="ico ico_star">
                        <svg>
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-star2"></use>
                        </svg></i><i class="ico ico_star">
                        <svg>
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-star2"></use>
                        </svg></i><i class="ico ico_star">
                        <svg>
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-star2"></use>
                        </svg></i>
                </div>
            </label>
        </li>
        <li class="options__item">
            <label class="checkbox">
                <input type="checkbox"/>
                <div class="checkbox__box">
                </div>
                <div class="checkbox__content">
                    <p>Аппартаменты
                    </p>
                </div>
            </label>
        </li>
        <li class="options__item">
            <label class="checkbox">
                <input type="checkbox"/>
                <div class="checkbox__box">
                </div>
                <div class="checkbox__content">
                    <p>Виллы
                    </p>
                </div>
            </label>
        </li>
    </ul>
</div>
<div class="sidebar__block"><span class="sidebar__caption">Отель:</span>
    <div class="select select_field select select_fsize-s" data-drop="data-drop">
        <button class="js-Dropdown-title">Все
        </button>
        <div class="select__dropdown"><span class="select__caption">Популярные:</span>
            <ul class="options">
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>10 Karakoy, A Morgans Original
                            </p>
                        </div>
                    </label>
                </li>
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>116 Residence Hotel
                            </p>
                        </div>
                    </label>
                </li>
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>1207 Boutique Hotel
                            </p>
                        </div>
                    </label>
                </li>
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>2000 Anittepe
                            </p>
                        </div>
                    </label>
                </li>
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>3T Residence
                            </p>
                        </div>
                    </label>
                </li>
            </ul><span class="select__caption">Все курорты:</span>
            <label class="field">
                <div class="field__input"><input type="text" placeholder="Поиск по отелю"/>
                </div>
            </label>
            <ul class="options">
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>10 Karakoy, A Morgans Original
                            </p>
                        </div>
                    </label>
                </li>
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>116 Residence Hotel
                            </p>
                        </div>
                    </label>
                </li>
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>1207 Boutique Hotel
                            </p>
                        </div>
                    </label>
                </li>
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>2000 Anittepe
                            </p>
                        </div>
                    </label>
                </li>
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>3T Residence
                            </p>
                        </div>
                    </label>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="sidebar__block"><span class="sidebar__caption">Туроператор:</span>
    <div class="select select_field select select_fsize-s" data-drop="data-drop">
        <button class="js-Dropdown-title">Все
        </button>
        <div class="select__dropdown">
            <ul class="options">
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>Туроператор
                            </p>
                        </div>
                    </label>
                </li>
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>Туроператор
                            </p>
                        </div>
                    </label>
                </li>
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>Туроператор
                            </p>
                        </div>
                    </label>
                </li>
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>Туроператор
                            </p>
                        </div>
                    </label>
                </li>
                <li class="options__item">
                    <label class="checkbox">
                        <input type="checkbox"/>
                        <div class="checkbox__box">
                        </div>
                        <div class="checkbox__content">
                            <p>Туроператор
                            </p>
                        </div>
                    </label>
                </li>
            </ul>
        </div>
    </div>
    <ul class="options">
        <li class="options__item options__item_strong">
            <label class="checkbox">
                <input type="checkbox"/>
                <div class="checkbox__box">
                </div>
                <div class="checkbox__content">
                    <p>Перелет включен
                    </p>
                </div>
            </label>
        </li>
    </ul>
</div>