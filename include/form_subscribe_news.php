<div class="subscribe">
    <div class="subscribe__container">
        <h2 class="title">Узнайте первым о наших турах!</h2>
        <form class="form form__subscribe">
            <div class="form__subscribe-error"></div>
            <div class="form__line form__line_double">
                <label class="field field_btn">
                    <div class="field__input field__input_with-button">
                        <input type="email" placeholder="Email" name="EMAIL" value="" required/>
                        <button class="field__btn">
                            <svg>
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-mail"></use>
                            </svg>
                        </button>
                    </div>
                </label>
                <button class="button button_highlight" type="submit">
                    <span>Подписаться на новости</span>
                </button>
            </div>
            <input type="hidden" name="RUB_ID" value="2">
        </form>
    </div>
</div>