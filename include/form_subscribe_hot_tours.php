<div class="sidebar__header"><span class="caption caption_size-s">Рассылка Горящих туров!</span></div>
<div class="sidebar__content">
    <form class="form form__subscribe">
        <div class="form__subscribe-error"></div>
        <label class="field">
            <div class="field__input field__input_with-button">
                <input type="email" placeholder="Email" name="EMAIL" value="" required/>
                <button class="field__btn" type="submit">
                    <svg>
                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-mail"></use>
                    </svg>
                </button>
            </div>
        </label>
        <input type="hidden" name="RUB_ID" value="2">
    </form>
</div>