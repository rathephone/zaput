<?php
use Bitrix\Main\Loader;
Loader::includeModule("iblock");
$arCity = [];
$arSelect = Array("ID", "NAME");
$arFilter = Array("IBLOCK_ID" => IBLOCK_ID_CITIES, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, Array("nPageSize"=>50), $arSelect);
while($ob = $res->GetNextElement()) $arCity[] = $ob->GetFields();
?>
<form class="form form_review form__review">
    <?=bitrix_sessid_post();?>
    <div class="form__feedback-error field__caption"></div>
    <div class="form__line form__line_inline">
        <label class="field field_size-wide field_<?=PBit::property_is_required(IBLOCK_ID_REVIEW, "NAME")?>"><span class="field__caption">ФИО</span>
            <div class="field__input"><input type="text" placeholder="Иванов Иван Иванович" name="NAME" <?=PBit::property_is_required(IBLOCK_ID_REVIEW, "NAME")?>/>
            </div>
        </label>
        <label class="field field_size-wide-s field_<?=PBit::property_is_required(IBLOCK_ID_REVIEW, "NUMBER_OF_CONTRACT")?>"><span class="field__caption">Номер договора</span>
            <div class="field__input"><input type="text" placeholder="1234567890" name="NUMBER_OF_CONTRACT" maxlength="15" <?=PBit::property_is_required(IBLOCK_ID_REVIEW, "NUMBER_OF_CONTRACT")?>/>
            </div>
        </label>
        <label class="field field_<?=PBit::property_is_required(IBLOCK_ID_REVIEW, "PHONE")?>"><span class="field__caption">Контактный телефон</span>
            <div class="field__input"><input type="tel" placeholder="+7 (123) - ___ - __ - __" data-mask="+7 (###) ###-####" name="PHONE" <?=PBit::property_is_required(IBLOCK_ID_REVIEW, "PHONE")?>/>
            </div>
        </label>
        <label class="field field_<?=PBit::property_is_required(IBLOCK_ID_REVIEW, "CITY")?>"><span class="field__caption">Город оформления</span><span class="select select_field select_city">
              <select name="CITY" <?=PBit::property_is_required(IBLOCK_ID_REVIEW, "CITY")?>>
                  <?if(count($arCity)>0){
                      foreach ($arCity as $city){?>
                          <option value="<?=$city["ID"];?>"><?=$city["NAME"];?></option>
                      <?}
                  }?>
              </select></span>
        </label>
    </div>
    <div class="form__line form__line_triple">
        <label class="field field_<?=PBit::property_is_required(IBLOCK_ID_REVIEW, "COUNTRY")?>"><span class="field__caption">Страна</span>
            <div class="field__input"><input type="text" placeholder="" name="COUNTRY" <?=PBit::property_is_required(IBLOCK_ID_REVIEW, "COUNTRY")?>/>
            </div>
        </label>
        <label class="field field_<?=PBit::property_is_required(IBLOCK_ID_REVIEW, "RESORT")?>"><span class="field__caption">Курорт</span>
            <div class="field__input"><input type="text" placeholder="" name="RESORT" <?=PBit::property_is_required(IBLOCK_ID_REVIEW, "RESORT")?>/>
            </div>
        </label>
        <label class="field field_<?=PBit::property_is_required(IBLOCK_ID_REVIEW, "HOTEL")?>"><span class="field__caption">Отель</span>
            <div class="field__input"><input type="text" placeholder="" name="HOTEL" <?=PBit::property_is_required(IBLOCK_ID_REVIEW, "HOTEL")?>/>
            </div>
        </label>
    </div>
    <div class="form__line">
        <label class="field field_size-l field_size-full field_<?=PBit::field_is_required(IBLOCK_ID_REVIEW, "PREVIEW_TEXT")?>"><span class="field__caption">Отзыв:</span>
            <div class="field__input field__input_text"><textarea placeholder="Не более 4000 символов" name="PREVIEW_TEXT" <?=PBit::field_is_required(IBLOCK_ID_REVIEW, "PREVIEW_TEXT")?>></textarea>
            </div>
        </label>
    </div>
    <div class="form__submit">
        <button class="button button_highlight button_size-wide" type="submit"><span>Отправить отзыв</span>
        </button>
    </div>
</form>