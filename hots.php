<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Горящие туры");

require_once $_SERVER["DOCUMENT_ROOT"] . '/sletat/lib/Autoloader.php';//Подключение библиотеки sletat
$json = new \sletatru\JsonGate([
    'login' => 'sletat@6468822.ru',
    'password' => 'xml@sletattest2019zp03',
]);

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$country = $request->get('COUNTRY');

$cityFromId = (int)USER_SITY;
$countryId = (isset($country)) ? $country : '';

$arTours = [];
$is_case = false;

$perelet = 0;
if ($request->get("PERELET") == "Y") $perelet = 1;

$hot = false;
$page = (!empty($request->get("page")) && intval($request->get("page")) > 0) ? (int)$request->get("page") : 1 ;// текущая активная страница
$count_view = 15;

if (!empty($countryId) && intval($countryId) > 0) {
    $cities = $request->get('TOWN');
    $meals = $request->get('MIALS');
    $stars = $request->get('CLASS_HOTEL');
    $operators = $request->get('OPERATOR');
    $s_adults = $request->get('PEOPLE');
    $s_kids = (is_array($request->get("CHILDREN"))) ? count((array) $request->get('CHILDREN')) : 0;
    $s_kids_ages = $request->get('CHILDREN');
    $groupBy = $request->get('groupBy');

    $hiddenOperators = null;
    $meals = (!empty($meals)) ? $meals : null;
    $cities = (!empty($cities)) ? $cities : null;
    $pageNumber = $page;
    $pageSize = $count_view; // количество туров в выдаче
    $s_nightsMax = (is_array($request->get("NIGHT_SEARCH"))) ? $request->get("NIGHT_SEARCH")["MIN"] : 7;//Минимальная продолжительность тура (ночей) int
    $s_nightsMin = (is_array($request->get("NIGHT_SEARCH"))) ? $request->get("NIGHT_SEARCH")["MAX"] : 7;//Максимальная продолжительность тура (ночей) int
    $stars = (!empty($stars)) ? $stars : null;
    $visibleOperators = (!empty($operators)) ? $operators : null;
    $groupBy = (!empty($groupBy)) ? $groupBy : 'so_price';
    $htmlPage = '';
    $economOnly = $perelet;

    $s_adults = (!empty($s_adults)) ? (int)$s_adults : 2;
    $s_kids = (!empty($s_kids)) ? (int)$s_kids : 0;
    $s_kids_ages = (!empty($s_kids_ages)) ? $s_kids_ages : null;
    $s_departFrom = (is_array($request->get("DATE"))) ? date('d.m.Y', ($request->get("DATE")["MIN"] / 1000)) : null;//Начальная дата диапазона дат вылета string
    $s_departTo = (is_array($request->get("DATE"))) ? date('d.m.Y', ($request->get("DATE")["MAX"] / 1000)) : null;//Конечная дата диапазона дат вылета string

    $arTours = $json->GetTours(
            $cityFromId,
            $countryId,
            'true', $cities,
            'RUB', false,
            $groupBy,
            $hiddenOperators,
            0, 0,
            $meals,
            $pageNumber, $pageSize,
            $s_nightsMax, $s_nightsMin,
            $stars, $visibleOperators,
            $economOnly
    );
    $htmlPage = PBit::mPage($arTours['Page']['iTotalRecords'], $pageSize, $pageNumber, 'hot_filter');
} else {
    try {
        $arTours = $json->getShowcaseReview($cityFromId);
        $lenArTours = count($arTours["Rows"]);
        if ($lenArTours > 0){
            $htmlPage = PBit::mPage($lenArTours, $count_view, $page, 'hot_main');
            if($page > 0)
                $arTours["Rows"] = array_slice($arTours["Rows"], ($page - 1) * $count_view, $count_view);
        }
    } catch (Exception $e) {
    }
    $is_case = true;
}
?>
    <div class="section__container container container_flex">
        <div class="section__block section__block_main" id="result_search_hot_tours_block">

            <div class="loading loading_process loading_finish">
                <span class="loading__value">0</span>
                <div class="loading__plain">
                    <svg>
                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-loading"></use>
                    </svg>
                </div>
            </div>

            <?if ($request->get("AJAX_PAGE_HOT_TOURS")=="Y") $APPLICATION->RestartBuffer();?>
            <? if (count($arTours['Rows']) > 0) {
                ?>
                <div class="section__line section__line_top">
                    <div class="section__block">
                        <div class="pages">
                            <div class="pages__title"><?= count($arTours['Rows']); ?> результатов:</div>
                            <?php echo $htmlPage;?>
                        </div>
                    </div>
                    <div class="section__block section__block_sort <?if($is_case){?>section__block_sort_hidden<?}?>" ><span class="section__caption">Сортировать:</span>
                        <div class="select select_field select_inline" data-name="SORT">
                            <select name="SORT">
                                <option value="so_price">От дешевых к дорогим</option>
                                <option value="hotel">Группировка по цене, начиная с минимальной</option>
                                <option value="sortedHotels">По названию отеля</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="items items_triple loading-process loading-process_finish">
                    <? foreach ($arTours['Rows'] as $tour) {
                        $hotel = [];
                        $hotel['SourceId'] = $tour['SourceId'];
                        $hotel['HotelId'] = $tour['HotelId'];
                        $hotel['CountryId'] = $tour['CountryId'];
                        $hotel['Price'] = intval($tour['Price']);
                        $hotel['CityFromId'] = $tour['CityFromId'];
                        $hotel = GetDiscount($hotel);
                        ?>
                        <div class="card card_hot">
                            <div class="card__image"><img src="<?= $tour['ImageHotel']; ?>"/></div>
                            <div class="card__info"><span class="caption caption_size-s"><?= $tour['CountryName']; ?>, <?= $tour['ResortName']; ?></span>
                                <div class="card__text"><p><?= $tour['HotelName'] . ' ' . $tour['OriginalStarName']; ?></p>
                                    <p><?= $tour['CheckInDate']; ?> - <?= $tour['CheckOutDate']; ?>
                                        (<?= $tour['Nights'] . ' ' . PBit::getDeclination($tour['Nights'], array("ночь", "ночи", "ночей")); ?>)</p>
                                </div>
                                <div class="card__footer">
                                    <div class="price">
                                        <?if ($hotel["OldPrice"]>0):?>
                                            <div class="price__value price__value_old"><span><?=number_format($hotel["OldPrice"], 0, ',', ' ');?>р</span></div>
                                        <?endif;?>
                                        <div class="price__value"><span><?=number_format($hotel["NewPrice"], 0, ',', ' ');?>р.</span></div>
                                    </div>
                                    <?php
                                    $url = 'OfferId=' . $tour['OfferId'] . '&SourceId=' . $tour['SourceId'] . '&RequestId=' . $arTours['RequestId'];
                                    if($arTours['RequestId'] == 0) $url .= '&hot=1';
                                    ?>
                                    <a class="button" href="/tour/?<?=$url;?>"><span>Подробнее</span></a>
                                </div>
                            </div>
                        </div>
                    <? } ?>
                </div>
            <? } else {?>
                <div class="items items_triple">
                    <h2 class="block_max_hot_tours">Увы, туры не найдены. Попробуйте изменить параметры поиска.</h2>
                </div>
            <?}?>
            <?if ($request->get("AJAX_PAGE_HOT_TOURS")=="Y") die();?>
        </div>
        <div class="section__block section__block_sidebar">
            <form class="filter__form js__filter__form">
                <div class="sidebar">
                    <div class="sidebar__content">
                        <div class="sidebar__block"><span class="sidebar__caption">Курорты:</span>
                            <div class="select select_field select select_fsize-s" data-drop="data-drop"
                                 data-name="TOWN">
                                <button class="js-Dropdown-title"></button>
                                <div class="select__dropdown max_height">
                                    <label class="field">
                                        <div class="field__input"><input type="text" class="search_in_list"
                                                                         placeholder="Поиск по курорту"/>
                                        </div>
                                    </label>
                                    <span class="select__caption">Популярные:</span>
                                    <ul class="options popular">

                                    </ul>
                                    <span class="select__caption">Все курорты:</span>
                                    <ul class="options other">

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="sidebar__block"><span class="sidebar__caption">Питание:</span>
                            <ul class="options">
                                <li class="options__item">
                                    <label class="checkbox">
                                        <input type="checkbox" name="classmeals[]" value="116"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content">
                                            <p>UAI - Ультра все включено
                                            </p>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox">
                                        <input type="checkbox" name="classmeals[]" value="115"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content">
                                            <p>AI - Всё включено
                                            </p>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox">
                                        <input type="checkbox" name="classmeals[]" value="114"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content">
                                            <p>BB – Завтрак
                                            </p>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox">
                                        <input type="checkbox" name="classmeals[]" value="112"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content">
                                            <p>FB - Завтрак, обед, ужин
                                            </p>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox">
                                        <input type="checkbox" name="classmeals[]" value="113"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content">
                                            <p>HB - Завтрак + ужин
                                            </p>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox">
                                        <input type="checkbox" name="classmeals[]" value="117"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content">
                                            <p>RO - Без питания
                                            </p>
                                        </div>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="sidebar__block"><span class="sidebar__caption">Класс отеля:</span>
                            <ul class="options">
                                <li class="options__item">
                                    <label class="checkbox checkbox_rating">
                                        <input type="checkbox" name="classhotel[]" value="400"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content"><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox checkbox_rating">
                                        <input type="checkbox" name="classhotel[]" value="401"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content"><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox checkbox_rating">
                                        <input type="checkbox" name="classhotel[]" value="402"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content"><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox checkbox_rating">
                                        <input type="checkbox" name="classhotel[]" value="403"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content"><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox checkbox_rating">
                                        <input type="checkbox" name="classhotel[]" value="404"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content"><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox">
                                        <input type="checkbox" name="classhotel[]" value="410"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content">
                                            <p>HV-1
                                            </p>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox">
                                        <input type="checkbox" name="classhotel[]" value="411"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content">
                                            <p>HV-2
                                            </p>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox">
                                        <input type="checkbox" name="classhotel[]" value="405"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content">
                                            <p>Аппартаменты
                                            </p>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox">
                                        <input type="checkbox" name="classhotel[]" value="406"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content">
                                            <p>Виллы
                                            </p>
                                        </div>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <!--<div class="sidebar__block">
                            <span class="sidebar__caption">Отель:</span>
                            <div class="select select_field select select_fsize-s" data-drop="data-drop"
                                 data-name="HOTEL">
                                <button class="js-Dropdown-title"></button>
                                <div class="select__dropdown max_height">
                                    <label class="field">
                                        <div class="field__input"><input type="text" placeholder="Поиск по отелю"/>
                                        </div>
                                    </label>
                                    <span class="select__caption">Популярные:</span>
                                    <ul class="options popular"></ul>
                                    <span class="select__caption">Все курорты:</span>
                                    <ul class="options other"></ul>
                                </div>
                            </div>
                        </div>-->
                        <div class="sidebar__block"><span class="sidebar__caption">Туроператор:</span>
                            <div class="select select_field select select_fsize-s" data-drop="data-drop"
                                 data-name="OPERATOR">
                                <button class="js-Dropdown-title"></button>
                                <div class="select__dropdown max_height">
                                    <ul class="options">
                                    </ul>
                                </div>
                            </div>
                            <ul class="options">
                                <li class="options__item options__item_strong">
                                    <label class="checkbox">
                                        <input type="checkbox" name="inc_perelet"/>
                                        <div class="checkbox__box"></div>
                                        <div class="checkbox__content">
                                            <p>Перелет включен</p>
                                        </div>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="sidebar__block">
                            <button class="button" data-submit-filter="ok" style="margin: 0 auto;display:block;">
                                <span>Применить</span>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <div class="sidebar">
                <? $APPLICATION->IncludeFile('/include/form_feedback_select.php', array(), array()) ?>
            </div>
            <div class="sidebar">
                <? $APPLICATION->IncludeFile('/include/form_subscribe_hot_tours.php', array(), array()) ?>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function clearLoading(){
            if($('.loading').length > 0) {
                $('.loading').removeClass('loading_process loading_finish');
                $('.items.items_triple.loading-process').removeClass('loading-process_finish');
                $('.loading .loading__value').text(0);
            }
        }

        function getHotToursResult(params = []) {
            var dataAjax = PrepareDataAjax();
            dataAjax['AJAX_PAGE_HOT_TOURS'] = "Y";
            if(params._page !== undefined) dataAjax['page'] = params._page;
            if(params._sort !== undefined) {
                dataAjax['groupBy'] = params._sort;
            } else {
                dataAjax['groupBy'] = $('[data-name="SORT"] .js-Dropdown-list li.is-selected').data('value');
            }
            //clearLoading();
            //loadProcess(30);

            $.ajax({
                xhr: function()
                {
                    var xhr = new window.XMLHttpRequest();
                    xhr.addEventListener("progress", function(evt){
                        var percentComplete = 0;
                        if (evt.lengthComputable) {
                            percentComplete = 100 * (evt.loaded / evt.total);
                            //loadProcess(percentComplete);
                        } else{
                            percentComplete = 100 * (evt.loaded / 20000);
                            //loadProcess(percentComplete);
                        }
                    }, false);
                    return xhr;
                },
                //url: "/hot-tours/",
                url: "/hots.php",
                dataType: "html",
                data: dataAjax,
                success: function (data) {
                    if (data) {

                        console.log($(data));

                        $('#result_search_hot_tours_block .items.items_triple').html($(data).filter('.items.items_triple').children());
                        $('#result_search_hot_tours_block .pages').html($(data).find('.pages').children());

                        if($('.section__block.section__block_sort').hasClass('section__block_sort_hidden'))
                            $('.section__block.section__block_sort').removeClass('section__block_sort_hidden');
                    } else {
                    }
                    //loadProcess(100);
                }
            });
        }
        function getHotToursMainResult(params = []) {
            var dataAjax = 'AJAX_PAGE_HOT_TOURS=Y';
            if(params._page !== undefined) dataAjax += '&page=' + params._page;
            if(params._sort !== undefined) {
                dataAjax += '&groupBy=' + params._sort;
            } else {
                dataAjax += '&groupBy=' + $('[data-name="SORT"] .js-Dropdown-list li.is-selected').data('value');
            }
            $.ajax({
                //url: "/hot-tours/",
                url: "/hots.php",
                dataType: "html",
                data: dataAjax,
                success: function (data) {
                    if (data) {
                        $('#result_search_hot_tours_block .items.items_triple').html($(data).filter('.items.items_triple').children());
                        $('#result_search_hot_tours_block .pages').html($(data).find('.pages').children());
                    } else {
                    }
                    //loadProcess(100);
                }
            });
        }
        $(document).on('click', '.hot_filter .pages__item', function (e) {
           var obj = [], page = $(this).data('page');
           if(page !== undefined && parseInt(page) !== 0){ obj._page = page; getHotToursResult(obj);}
           e.preventDefault();
           return false;
        });
        $(document).on('click', '.hot_main .pages__item', function (e) {
           var obj = [], page = $(this).data('page');

           console.log('thus');

           if(page !== undefined && parseInt(page) !== 0){
               obj._page = page;
               console.log(obj);
               getHotToursMainResult(obj);
           }
           e.preventDefault();
           return false;
        });
        $(document).on('click', '[data-name="SORT"] .js-Dropdown-list li', function (e) {
            var obj = [], sort = $(this).data('value');
            if(sort !== undefined){ obj._sort = sort; getHotToursResult(obj);}
            e.preventDefault();
            return false;
        });
        $(document).on('click', '[data-submit-filter="ok"], [name="SEARCH"]', function (e) {
            getHotToursResult();
            e.preventDefault();
            return false;
        });
    </script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>