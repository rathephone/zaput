<?php
define("STOP_STATISTICS", true);
define("PUBLIC_AJAX_MODE", true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require_once $_SERVER["DOCUMENT_ROOT"] . '/sletat/lib/Autoloader.php';//Подключение библиотеки sletat
$json = new \sletatru\JsonGate([
    'login' => 'sletat@6468822.ru',
    'password' => 'xml@sletattest2019zp03',
]);
$cityFromId = USER_SITY;
header('Content-Type: application/json; charset=' . LANG_CHARSET);
$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$arResult = [];
if ($f = $request->get('f')) {
    try {
        switch ($f) {
            case 'GetShowcaseReview':
                $arTours = $json->getShowcaseReview($cityFromId);
                foreach ($arTours['Rows'] as &$tour){
                    $tour = GetDiscount($tour);
                }
                $arResult['status'] = 'ok';
                $arResult['res'] = $arTours;
                //$arResult['res'] = $json->getShowcaseReview($cityFromId);
                break;
        }

    } catch (Exception $e) {
        $arResult['error'] = $e->getMessage();
    }
} else {
    $arResult['error'] = 'Нет функции';
}
echo json_encode($arResult);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
die();