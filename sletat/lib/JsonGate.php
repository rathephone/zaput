<?php

namespace sletatru;

/**
 * Class for a sletat.ru rest service
 */
class JsonGate extends BaseServiceRest
{
	/**
	 * @var string
	 */
	public $url = 'https://module.sletat.ru/Main.svc';
	/**
	 * @var string
	 */
	public $login = null;
	/**
	 * @var string
	 */
	public $password = null;


	/**
	 * @param string $cityFromId
	 * @param string $countryId
	 * @param bool $s_showcase
	 * @param array|string $cities
	 * @param string $currencyAlias
	 * @param bool $fake
	 * @param string $groupBy
	 * @param array|string $hiddenOperators
	 * @param int $includeDescriptions
	 * @param int $includeOilTaxesAndVisa
	 * @param array|string $meals
	 * @param int $pageNumber
	 * @param int $pageSize
	 * @param int $s_nightsMax
	 * @param int $s_nightsMin
	 * @param array|string $stars
	 * @param array|string $visibleOperators
	 */
	public function GetTours(
		$cityFromId,
		$countryId,
		$s_showcase = 'true',
		$cities = null,
		$currencyAlias = 'RUB',
		$fake = false,
		$groupBy = 'so_price',
		$hiddenOperators = null,
		$includeDescriptions = 0,
		$includeOilTaxesAndVisa = 0,
		$meals = null,
		$pageNumber = 1,
		$pageSize = 30,
		$s_nightsMax = null,
		$s_nightsMin = null,
		$stars = null,
		$visibleOperators = null,
        $economOnly = 0
	){
		$params = array(
			'cityFromId' => $cityFromId,
			'countryId' => $countryId,
			's_showcase' => $s_showcase,
			'currencyAlias' => $currencyAlias,
			'fake' => (bool) $fake,
			'groupBy' => $groupBy,
			'includeDescriptions' => (int) $includeDescriptions,
			'includeOilTaxesAndVisa' => (int) $includeOilTaxesAndVisa,
			'pageNumber' => (int) $pageNumber,
			'pageSize' => (int) $pageSize,
            'economOnly' => (int) $economOnly,
		);
		if (is_array($cities)) {
			$params['cities'] = implode(',', $cities);
		} elseif (!empty($cities)) {
			$params['cities'] = trim($cities);
		}
		if (is_array($hiddenOperators)) {
			$params['hiddenOperators'] = implode(',', $hiddenOperators);
		} elseif (!empty($hiddenOperators)) {
			$params['hiddenOperators'] = trim($hiddenOperators);
		}
		if (is_array($meals)) {
			$params['meals'] = implode(',', $meals);
		} elseif (!empty($meals)) {
			$params['meals'] = trim($meals);
		}
		if (is_array($stars)) {
			$params['stars'] = implode(',', $stars);
		} elseif (!empty($stars)) {
			$params['stars'] = trim($stars);
		}
		if ($s_nightsMax !== null) {
			$params['s_nightsMax'] = (int) $s_nightsMax;
		}
		if ($s_nightsMin !== null) {
			$params['s_nightsMin'] = (int) $s_nightsMin;
		}
		if (is_array($visibleOperators)) {
			$params['visibleOperators'] = implode(',', $visibleOperators);
		} elseif (!empty($visibleOperators)) {
			$params['visibleOperators'] = trim($visibleOperators);
		}
		$res = $this->callMethod('GetTours', $params);

		$return = array();
		if (!empty($res['aaData'])) {
			foreach ($res['aaData'] as $hotel) {
				$return['Rows'][] = array(
					'OfferId' => $hotel[0],
					'SourceId' => $hotel[1],
					'HotelDescriptionUrl' => $hotel[2],
					'HotelId' => $hotel[3],
					'ResortId' => $hotel[5],
					'TourName' => $hotel[6],
					'HotelName' => $hotel[7],
					'OriginalStarName' => $hotel[8],
					'RoomName' => $hotel[9],
					'MealName' => $hotel[10],
					'HtPlaceName' => $hotel[11],
					'CheckInDate' => $hotel[12],
					'CheckOutDate' => $hotel[13],
					'Nights' => $hotel[14],
					'Adults' => $hotel[16],
					'Kids' => $hotel[17],
					'SourceName' => $hotel[18],
					'ResortName' => $hotel[19],
					'SourceSearchFormUrl' => $hotel[20],
					'TicketsIncluded' => $hotel[22],
					//'ImageHotel' => $hotel[29],
					'CountryId' => $hotel[30],
					'CountryName' => $hotel[31],
					'CityFromId' => $hotel[32],
					'CityFromName' => $hotel[33],
					'SourceImageUrl' => $hotel[34],
					'HotelRating' => $hotel[35],
					'MealDescription' => $hotel[36],
					'HtPlaceDescription' => $hotel[37],
					'HotelDescription' => $hotel[38],
					'HtPlaceId' => $hotel[39],
					'IsDemo' => $hotel[40],
					'MealId' => $hotel[41],
					'Price' => $hotel[42],
					'Currency' => $hotel[43],
					'RoomId' => $hotel[44],
					'StarId' => $hotel[45],
					'HotelPhotosCount' => $hotel[46],
					'TourUrl' => $hotel[47],
					'OriginalHotelName' => $hotel[48],
					'OriginalStarNameOperator' => $hotel[49],
					'OriginalTownName' => $hotel[50],
					'OriginalMealName' => $hotel[51],
					'OriginalHtPlaceName' => $hotel[52],
					'OriginalRoomName' => $hotel[53],
					'PriceType' => 1,
                    'ImageHotel' => (strlen($hotel[29]) > 0) ? $this->getHotelImageUrl($hotel[3], 0, 230, 200, 1, true, $hotel[29]) : SITE_TEMPLATE_PATH . '/pictures/no_foto.jpg'
				);
			}
		}
		$return['Page'] = [
		    'hotelsCount' => $res['hotelsCount'],
		    'iTotalDisplayRecords' => $res['iTotalDisplayRecords'],
		    'iTotalRecords' => $res['iTotalRecords'],
        ];
		if (!empty($res['requestId'])) {
			$return['RequestId'] = $res['requestId'];
		}
		return $return;
	}

    /**
     * @param int $city_id
     * @return string
     */
    public function getCurTemplateName($city_id = 1264)
    {
        $params['templatesList'] = 'all';
        $res = $this->callMethod('GetTemplates', $params);

        if (isset($res['templates']) && count($res['templates']) > 0) {
            $arTemplates = $res['templates'];
            $key = array_search($city_id, array_column($arTemplates, 'departureCityId'));
            return ($arTemplates[$key]['name']) ? $arTemplates[$key]['name'] : 'Горящие туры (СПб)';
        } else {
            return 'Горящие туры (СПб)';
        }
    }

    /**
     * @param int $city_id
     * @return array
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectException
     */
    public function getShowcaseReview($city_id = 1264)
    {
        $params['templateName'] = $this->getCurTemplateName($city_id);
        $res = $this->callMethod('GetShowcaseReview', $params);
        $res = $this->replaceImageAndSortCountry($res);
        $return = [];
        if (!empty($res)) {
            foreach ($res as $hotel) {
                $objDateTime = new \Bitrix\Main\Type\DateTime($hotel['MinPriceDate'], "d/m/Y");
                $checkInDate = $objDateTime->format('d.m.Y');
                $objDateTime->add($hotel['Nights'] ." day");
                $checkOutDate = $objDateTime->format('d.m.Y');
                $return['Rows'][] = array(
                    'OfferId' => $hotel['OfferId'],
                    'SourceId' => $hotel['SourceId'],
                    'HotelId' => $hotel['HotelId'],
                    'HotelName' => $hotel['HotelName'],
                    'MealName' => $hotel['MealName'],
                    'HtPlaceName' => $hotel['HtPlaceName'],
                    'CountryId' => $hotel['CountryId'],
                    'CountryName' => $hotel['CountryName'],
                    'ImageHotel' => $hotel['CountryImageUrl'],
                    'Price' => $hotel['MinPrice'],
                    'MinPriceDate' => $hotel['MinPriceDate'],
                    'Nights' => $hotel['Nights'],
                    'ResortName' => $hotel['ResortName'],
                    'OriginalStarName' => $hotel['StarName'],
                    'adults' => $hotel['Template']['adults'],
                    'checkInFrom' => $hotel['Template']['checkInFrom'],
                    'checkInTo' => $hotel['Template']['checkInTo'],
                    'hasTickets' => $hotel['Template']['hasTickets'],
                    'hotelIsNotInStop' => $hotel['Template']['hotelIsNotInStop'],
                    'kids' => $hotel['Template']['kids'],
                    'lastPricePercent' => $hotel['Template']['lastPricePercent'],
                    'meals' => $hotel['Template']['meals'],
                    'nightsMax' => $hotel['Template']['nightsMax'],
                    'nightsMin' => $hotel['Template']['nightsMin'],
                    'priceMax' => $hotel['Template']['priceMax'],
                    'priceMin' => $hotel['Template']['priceMin'],
                    'stars' => $hotel['Template']['stars'],
                    'ticketsIncluded' => $hotel['Template']['ticketsIncluded'],
                    'CityFromId' => $hotel['Template']['townFromId'],
                    'useRandomPricePercent' => $hotel['Template']['useRandomPricePercent'],
                    'CheckInDate' => $checkInDate,
                    'CheckOutDate' => $checkOutDate,
                    'Sort' => $hotel['Sort'],
                    'DetailPageUrl' => $hotel['DetailPageUrl']
                );
            }
        }
        if (empty($res['requestId'])) {
            $return['RequestId'] = 0;
        }
        return $return;
    }

    /**
     * @param $id
     * @param int $count
     * @param null $width
     * @param null $height
     * @param int $method
     * @param bool $isHttps
     * @param string $smallPicture
     * @return string
     */
    public function getHotelImageUrl($id, $count = 0, $width = null, $height = null, $method = 1, $isHttps = false, $smallPicture = "")
    {
        $type = $width !== null || $height !== null ? 'p' : 'f';
        $return = ($isHttps ? 'https://' : 'http://') . 'hotels.sletat.ru/i/' . $type . '/' . intval($id) . '_' . intval($count);
        if (($width = (int) $width) > 0 && ($height = (int) $height) > 0) {
            $return .= '_' . $height . '_' . $width;
        }
        $return .= '_' . intval($method);
        $return .= '.jpg';
        return $return;
    }

    /**
     * @param array $array
     * @return array
     * @throws \Bitrix\Main\LoaderException
     */
    function replaceImageAndSortCountry(array $array = array()){
        $arShowcaseReview = $array;
        $arNewShowcaseReview = [];
        if(\Bitrix\Main\Loader::includeModule("iblock")) {
            if (count($arShowcaseReview) > 0) {
                $arIdsCountry = array_column($arShowcaseReview, 'CountryId');
                $res = \CIBlockElement::GetList(
                    Array("SORT" => "ASC", "SHOW_COUNTER" => "DESC"),
                    Array("IBLOCK_ID" => IBLOCK_ID_COUNTRIES, "ACTIVE" => "Y", "PROPERTY_XML_ID" => $arIdsCountry),
                    false,
                    false,
                    Array("PREVIEW_PICTURE", "PROPERTY_XML_ID", "DETAIL_PAGE_URL")
                );
                $k = 0;
                while ($ob = $res->GetNext()) {
                    $key = array_search($ob["PROPERTY_XML_ID_VALUE"], $arIdsCountry);
                    if($key == 0 || $key > 0){
                        $arNewShowcaseReview[$k] = $arShowcaseReview[$key];
                        $arNewShowcaseReview[$k]["CountryImageUrl"] = ($ob["PREVIEW_PICTURE"]) ? \CFile::GetPath($ob["PREVIEW_PICTURE"]) : $arShowcaseReview[$key]["CountryImageUrl"];
                        $arNewShowcaseReview[$k]["DetailPageUrl"] = $ob["DETAIL_PAGE_URL"];
                    }
                    $k++;
                }
            }
        }
        $array = $arNewShowcaseReview;
        unset($arShowcaseReview);
        return $array;
    }

    /**
     * @param $offerId
     * @param $sourceId
     * @param int $showcase
     * @return mixed
     */
    public function getActualizePrice($offerId, $sourceId, $showcase = 1)
    {
        $params = array(
            'requestId' => 0,
            'offerId' => $offerId,
            'sourceId' => $sourceId,
            'showcase' => $showcase
        );
        $res = $this->callMethod('ActualizePrice', $params);
        return $res;
    }


	/**
	 * @param string $url
	 * @param array $params
	 * @param string $type
	 * @return mixed
	 */
	protected function callMethod($method, array $params = array())
	{
		$url = rtrim($this->url, '/\\') . '/' . str_replace(array('/', '\\', '#', ':', '@'), '', $method);
		$params['login'] = $this->login;
		$params['password'] = $this->password;
		return $this->doRequest($url, $params);
	}

	/**
	 * @param string $result
	 * @return string
	 */
	protected function parseResult($result)
	{
		$response = !empty($result['content']) ? reset(json_decode($result['content'], true)) : array();
		if (!empty($response['IsError'])) {
			$this->addError($response['ErrorMessage']);
		}
		return !empty($response['Data']) ? $response['Data'] : array();
	}

}
