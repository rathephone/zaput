<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Поиск туров");
$APPLICATION->SetTitle("Поиск туров");
$request = \Bitrix\Main\Context::getCurrent()->getRequest();
?>
<?if ($request->count()>0):?>
<script>
    searchTur();
</script>
<?endif;?>
    <div class="section__container container container_flex">
        <div class="section__block section__block_main" id="result_search_block">

        </div>
        <div class="section__block section__block_sidebar">
            <form onsubmit="return false;" class="filter__form js__filter__form">
                <div class="sidebar">
                    <div class="sidebar__content">
                        <div class="sidebar__block block_list">
                            <span class="sidebar__caption">Курорты:<div class="cleare_all">Очистить</div></span>
                            <label class="field">
                                <div class="field__input"><input type="text" class="search_in_list_2" placeholder="Поиск по курорту"/></div>
                            </label>
                            <div class="max_height_2">
                                <ul class="options max_height_3" data-name="TOWN">
                                </ul>
                                <a class="show_more" href="javascript:void(0);" data-show="Скрыть все" data-hide="Показать все"></a>
                            </div>
                        </div>
                        <div class="sidebar__block"><span class="sidebar__caption">Питание:</span>
                            <ul class="options">
                                <li class="options__item">
                                    <label class="checkbox">
                                        <input type="checkbox" name="classmeals[]" value="116"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content">
                                            <p>UAI - Ультра все включено
                                            </p>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox">
                                        <input type="checkbox" name="classmeals[]" value="115"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content">
                                            <p>AI - Всё включено
                                            </p>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox">
                                        <input type="checkbox" name="classmeals[]" value="114"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content">
                                            <p>BB – Завтрак
                                            </p>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox">
                                        <input type="checkbox" name="classmeals[]" value="112"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content">
                                            <p>FB - Завтрак, обед, ужин
                                            </p>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox">
                                        <input type="checkbox" name="classmeals[]" value="113"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content">
                                            <p>HB - Завтрак + ужин
                                            </p>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox">
                                        <input type="checkbox" name="classmeals[]" value="117"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content">
                                            <p>RO - Без питания
                                            </p>
                                        </div>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="sidebar__block"><span class="sidebar__caption">Класс отеля:</span>
                            <ul class="options">
                                <li class="options__item">
                                    <label class="checkbox checkbox_rating">
                                        <input type="checkbox" name="classhotel[]" value="400"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content"><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox checkbox_rating">
                                        <input type="checkbox" name="classhotel[]" value="401"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content"><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox checkbox_rating">
                                        <input type="checkbox" name="classhotel[]" value="402"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content"><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox checkbox_rating">
                                        <input type="checkbox" name="classhotel[]" value="403"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content"><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox checkbox_rating">
                                        <input type="checkbox" name="classhotel[]" value="404"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content"><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i><i class="ico ico_star">
                                                <svg>
                                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star2"></use>
                                                </svg>
                                            </i>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox">
                                        <input type="checkbox" name="classhotel[]" value="410"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content">
                                            <p>HV-1
                                            </p>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox">
                                        <input type="checkbox" name="classhotel[]" value="411"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content">
                                            <p>HV-2
                                            </p>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox">
                                        <input type="checkbox" name="classhotel[]" value="405"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content">
                                            <p>Аппартаменты
                                            </p>
                                        </div>
                                    </label>
                                </li>
                                <li class="options__item">
                                    <label class="checkbox">
                                        <input type="checkbox" name="classhotel[]" value="406"/>
                                        <div class="checkbox__box">
                                        </div>
                                        <div class="checkbox__content">
                                            <p>Виллы
                                            </p>
                                        </div>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="sidebar__block block_list">
                            <span class="sidebar__caption">Отель:<div class="cleare_all">Очистить</div></span>
                            <label class="field">
                                <div class="field__input"><input type="text" class="search_in_list_2" placeholder="Поиск по отелю"/></div>
                            </label>
                            <div class="max_height_2">
                                <ul class="options max_height_3" data-name="HOTEL"></ul>
                                <a class="show_more" href="javascript:void(0);" data-show="Скрыть все" data-hide="Показать все"></a>
                            </div>
                        </div>
                        <div class="sidebar__block block_list">
                            <span class="sidebar__caption">Туроператор:<div class="cleare_all">Очистить</div></span>
                            <label class="field">
                                <div class="field__input"><input type="text" class="search_in_list_2" placeholder="Поиск по туроператору"/></div>
                            </label>
                            <div class="max_height_2">
                                <ul class="options max_height_3" data-name="OPERATOR"></ul>
                                <a class="show_more" href="javascript:void(0);" data-show="Скрыть все" data-hide="Показать все"></a>
                            </div>
                        </div>
                        <div class="sidebar__block">
                            <ul class="options">
                                <li class="options__item options__item_strong">
                                    <label class="checkbox">
                                        <input type="checkbox" checked name="inc_perelet"/>
                                        <div class="checkbox__box"></div>
                                        <div class="checkbox__content">
                                            <p>Перелет включен</p>
                                        </div>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="sidebar__block">
                            <button class="button" data-submit-filter="ok" style="margin: 0 auto;display:block;" onclick="searchTur(true);">
                                <span>Применить</span>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <div class="sidebar">
                <? $APPLICATION->IncludeFile('/include/form_feedback_select.php', array(), array()) ?>
            </div>
            <div class="sidebar">
                <? $APPLICATION->IncludeFile('/include/form_subscribe_hot_tours.php', array(), array()) ?>
            </div>
        </div>
    </div>
<? $detail = ["IS_EXIST" => false, "TEXT" => "", "NAME" => "", "MENU" => []];
if ($country_code = $_REQUEST['CODE']) {
    \Bitrix\Main\Loader::includeModule("iblock");
    $resCountry = \CIBlockElement::GetList(
        Array(),
        Array("IBLOCK_ID" => IBLOCK_ID_COUNTRIES, "ACTIVE" => "Y", "CODE" => $country_code),
        false,
        Array("nPageSize" => 1),
        Array("ID", "NAME")
    )->GetNext();
    if ($resCountry) {
  $ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues(IBLOCK_ID_COUNTRIES,$resCountry["ID"]);
         $ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues(IBLOCK_ID_COUNTRIES,$resCountry["ID"]);
        $IPROPERTY = $ipropValues->getValues();
        if (count($IPROPERTY)){
            $APPLICATION->SetPageProperty("TITLE", $IPROPERTY["ELEMENT_META_TITLE"]);
            $APPLICATION->SetPageProperty("DESCRIPTION", $IPROPERTY["ELEMENT_META_DESCRIPTION"]);
            $APPLICATION->SetPageProperty("KEYWORDS", $IPROPERTY["ELEMENT_META_KEYWORDS"]);
            $APPLICATION->SetTitle($IPROPERTY["ELEMENT_PAGE_TITLE"]);
        }
        $APPLICATION->AddChainItem($resCountry["NAME"], "");

        CIBlockElement::CounterInc($resCountry["ID"]);

        $resCountryText = \CIBlockElement::GetList(
            Array("SORT" => "ASC"),
            Array("IBLOCK_ID" => IBLOCK_ID_COUNTRIES_SEO, "ACTIVE" => "Y", "PROPERTY_COUNTRY" => $resCountry["ID"]),
            false,
            Array("nPageSize" => 5),
            Array("ID", "NAME", "PREVIEW_TEXT", "PROPERTY_COUNTRY", "PROPERTY_IS_SHOW", "DETAIL_PAGE_URL")
        );
        while ($resText = $resCountryText->GetNext()) {
            if ($resText["PROPERTY_IS_SHOW_VALUE"] == "Да" && !$detail["IS_EXIST"]) {
                $detail["TEXT"] = $resText["PREVIEW_TEXT"];
                $detail["NAME"] = $resText["NAME"];
                $detail["IS_EXIST"] = true;
            } else {
                $detail["MENU"][] = [
                    "URL" => $resText["DETAIL_PAGE_URL"],
                    "NAME" => $resText["NAME"]
                ];
            }
        }
    } else {
        @define("ERROR_404", "Y");
    }
}
if ($detail["IS_EXIST"]) { ?>
    <section class="section section_light section_information">
        <div class="section__container container container_flex">
            <div class="section__block section__block_sidebar">
                <? if (count($detail["MENU"])) { ?>
                    <ul class="sidebar-links">
                        <? foreach ($detail["MENU"] as $menu) { ?>
                            <li class="sidebar-links__item"><a href="<?= $menu["URL"]; ?>"><?= $menu["NAME"]; ?></a><i
                                        class="ico">
                                    <svg>
                                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-arrow-small"></use>
                                    </svg>
                                </i>
                            </li>
                        <? } ?>
                    </ul>
                <? } ?>
            </div>
            <div class="section__block section__block_main">
                <h3 class="caption"><?= $detail["NAME"] ?></h3>
                <?= $detail["TEXT"]; ?>
                <? if (count($detail["MENU"])) { ?>
                    <ul class="content-links">
                        <? foreach ($detail["MENU"] as $menu) { ?>
                            <li class="content-links__item"><a href="<?= $menu["URL"]; ?>"><?= $menu["NAME"]; ?></a>
                            </li>
                        <? } ?>
                    </ul>
                <? } ?>

            </div>
        </div>
    </section>
<? }
else{?>
    <section class="section section_information">
    </section>
    <?}?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>