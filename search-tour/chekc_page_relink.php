<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
$request = \Bitrix\Main\Context::getCurrent()->getRequest();
$selectCountry = $request->get("extLocation");
\Bitrix\Main\Loader::includeModule("iblock");
if ($resCountry = CIBlockElement::GetList(array(),array("IBLOCK_ID" => 14, "ACTIVE" => "Y", "PROPERTY_XML_ID" => $selectCountry))->GetNext()){
    $ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues(IBLOCK_ID_COUNTRIES,$resCountry["ID"]);
    $IPROPERTY = $ipropValues->getValues();
    $setName = $resCountry["NAME"];
    if (count($IPROPERTY)){
        $setName = $IPROPERTY["ELEMENT_PAGE_TITLE"];
    }
    CIBlockElement::CounterInc($resCountry["ID"]);

    $detail = ["IS_EXIST" => false, "TEXT" => "", "NAME" =>  $setName, "LINK" => "/tury-".$resCountry["CODE"], "MENU" => [], "DETAL_HTML" => ""];
    $resCountryText = \CIBlockElement::GetList(
        Array("SORT" => "ASC"),
        Array("IBLOCK_ID" => 15, "ACTIVE" => "Y", "PROPERTY_COUNTRY" => $resCountry["ID"]),
        false,
        false,
        Array("ID", "NAME", "PREVIEW_TEXT", "PROPERTY_COUNTRY", "PROPERTY_IS_SHOW", "DETAIL_PAGE_URL")
    );
    while ($resText = $resCountryText->GetNext()) {
        if ($resText["PROPERTY_IS_SHOW_VALUE"] == "Да" && !$detail["IS_EXIST"]) {
            $detail["TEXT"] = $resText["PREVIEW_TEXT"];
            $detail["IS_EXIST"] = true;
        } else {
            $detail["MENU"][] = [
                "URL" => $resText["DETAIL_PAGE_URL"],
                "NAME" => $resText["NAME"]
            ];
        }
    }
    if ($detail["IS_EXIST"]) { ?>
        <?$detail["DETAL_HTML"] = '<section class="section section_light section_information">
            <div class="section__container container container_flex">
                <div class="section__block section__block_sidebar">';
                    if (count($detail["MENU"])) {
                        $detail["DETAL_HTML"] .= '<ul class="sidebar-links">';
                            foreach ($detail["MENU"] as $menu) {
                                $detail["DETAL_HTML"] .= '<li class="sidebar-links__item"><a href="'.$menu["URL"].'">'.$menu["NAME"].'</a><i class="ico"><svg><use xlink:href="'.SITE_TEMPLATE_PATH .'/img/symbols.svg#svg-ico-arrow-small"></use></svg></i></li>';
                            }
                        $detail["DETAL_HTML"] .= '</ul>';
                    }
                $detail["DETAL_HTML"] .= '</div>';
                $detail["DETAL_HTML"] .= '<div class="section__block section__block_main">';
                    $detail["DETAL_HTML"] .= '<h3 class="caption">'.$detail["NAME"].'</h3>';
                    $detail["DETAL_HTML"] .= $detail["TEXT"];
                    if (count($detail["MENU"])) {
                        $detail["DETAL_HTML"] .= '<ul class="content-links">';
                            foreach ($detail["MENU"] as $menu) {
                                $detail["DETAL_HTML"] .= '<li class="content-links__item"><a href="'.$menu["URL"].'">'.$menu["NAME"].'</a></li>';
                            }
                        $detail["DETAL_HTML"] .= '</ul>';
                    }

                $detail["DETAL_HTML"] .= '</div>';
                $detail["DETAL_HTML"] .= '</div>';
                $detail["DETAL_HTML"] .= '</section>';
        $detail["DETAL_HTML"] = str_replace(array("\n","\r","\n\r","\r\n"),"",$detail["DETAL_HTML"])
        ?>
    <? }
    unset($detail["TEXT"]);
}
echo json_encode($detail);
?>