<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
require_once $_SERVER["DOCUMENT_ROOT"] . '/sletat/lib/Autoloader.php';//Подключение библиотеки sletat
//инициируем новый объект xml сервиса
$xml = new \sletatru\XmlGate([
    'login' => 'sletat@6468822.ru',
    'password' => 'xml@sletattest2019zp03',
]);
$request = \Bitrix\Main\Context::getCurrent()->getRequest();
$RESULT = array();
$perelet = false;
if ($request->get("PERELET") == true){
    $perelet = true;
}
if ($request->get("PREPARE") == "Y" && $request->get("COUNTRY") && USER_SITY > 0) {
    $countryId = $request->get("COUNTRY");//Идентификатор страны.
    $cityFromId = (int)USER_SITY;// Идентификатор города вылета.
    $cities = (!empty($request->get("TOWN"))) ? $request->get("TOWN") : null;//Список идентификаторов курортов arr
    $meals = (!empty($request->get("MIALS"))) ? $request->get("MIALS") : null;//id типа питания arr
    $stars = (is_array($request->get("CLASS_HOTEL"))) ? $request->get("CLASS_HOTEL") : null;//идентификатор категорий отелей arr
    $hotels = (!empty($request->get("HOTEL"))) ? $request->get("HOTEL") : null;//Список идентификаторов отелей arr
    $adults = (!empty($request->get("PEOPLE"))) ? (int)$request->get("PEOPLE") : 2;//Количество туристов (взрослых) int
    $kids = (!empty($request->get("CHILDREN"))) ? count($request->get("CHILDREN")) : 0;//Количество туристов (Детей) int
    $kidsAges = (!empty($request->get("CHILDREN"))) ? $request->get("CHILDREN") : null;//Возраст каждого ребёнка arr
    $nightsMin = (is_array($request->get("NIGHT_SEARCH"))) ? $request->get("NIGHT_SEARCH")["MIN"] : 7;//Минимальная продолжительность тура (ночей) int
    $nightsMax = (is_array($request->get("NIGHT_SEARCH"))) ? $request->get("NIGHT_SEARCH")["MAX"] : 7;//Максимальная продолжительность тура (ночей) int
    $priceMin = (!empty($request->get("priceMin"))) ? (int)$request->get("priceMin") : null;//Минимальная цена тура. int
    $priceMax = (!empty($request->get("priceMax"))) ? (int)$request->get("priceMax") : null;//Максимальная цена тура int
    $currencyAlias = 'RUB';
    $departFrom = (is_array($request->get("DATE"))) ? date('d.m.Y', ($request->get("DATE")["MIN"] / 1000)) : null;//Начальная дата диапазона дат вылета string
    $departTo = (is_array($request->get("DATE"))) ? date('d.m.Y', ($request->get("DATE")["MAX"] / 1000)) : null;//Конечная дата диапазона дат вылета string
    $hotelIsNotInStop = (!empty($request->get("hotelIsNotInStop")) && $request->get("hotelIsNotInStop") == "1") ? true : false;//Фильтрация результатов поиска по наличию мест в отеле bool
    $hasTickets = (!empty($request->get("hasTickets")) && $request->get("hasTickets") == "1") ? true : false;//Фильтрация результатов поиска по наличию мест в отеле bool
    $ticketsIncluded = $perelet;//Фильтрация результатов поиска по наличию мест в отеле bool
    $useFilter = (is_array($request->get("OPERATOR")) && count($request->get("OPERATOR")>0)) ? true : false;//Фильтрация результатов поиска по наличию мест в отеле bool
    $f_to_id = (is_array($request->get("OPERATOR"))) ? $request->get("OPERATOR") : null;;
    $includeDescriptions = (!empty($request->get("includeDescriptions")) && $request->get("includeDescriptions") == "1") ? true : false;//Фильтрация результатов поиска по наличию мест в отеле bool
    $cacheMode = 0;
    $createRequest = $xml->CreateRequest(
        $countryId,
        $cityFromId,
        $cities,
        $meals,
        $stars,
        $hotels,
        $adults,
        $kids,
        $kidsAges,
        $nightsMin,
        $nightsMax,
        $priceMin,
        $priceMax,
        $currencyAlias,
        $departFrom,
        $departTo,
        $hotelIsNotInStop,
        $hasTickets,
        $ticketsIncluded,
        $useFilter,
        $f_to_id,
        $includeDescriptions,
        $cacheMode
    );
    echo json_encode(array("success" => true, "result_id" => $createRequest));
    die();
} elseif (empty($request->get("RESULT")) && $request->get("ID_RESULT") > 0) {
    $array_of_tours = $xml->GetRequestState($request->get("ID_RESULT"));
    $success = true;
    $countSuccess = 0;
    foreach ($array_of_tours as $tourResult) {
        if (!$tourResult["IsProcessed"]) $success = false;
        else $countSuccess++;
    }
    echo json_encode(array("success" => $success, "percent" => intval($countSuccess / count($array_of_tours) * 100)));
    die();
} elseif ($request->get("RESULT") == "Y" && $request->get("ID_RESULT") > 0) {
    $arrTurResult = $xml->GetRequestResult($request->get("ID_RESULT"));
    foreach ($arrTurResult["Rows"] as $key => $dataTour){
        if (empty($request->get("PERELET")) && $dataTour["TicketsIncluded"] == "Included"){
            unset($arrTurResult["Rows"][$key]);continue;
        }
        $arrTurResult["Rows"][$key] = $hotel = GetDiscount($dataTour);
    }
    switch (TYPE_SORT) {
        case 0:
            usort($arrTurResult["Rows"], function ($a, $b) {
                if ($a["NewPrice"]<$b["NewPrice"]) return -1;
                elseif ($a["NewPrice"]>$b["NewPrice"]) return 1;
                return 0;
            });
            break;
        case 1:
            usort($arrTurResult["Rows"], function ($a, $b) {
                if ($a["NewPrice"]>$b["NewPrice"]) return -1;
                elseif ($a["NewPrice"]<$b["NewPrice"]) return 1;
                return 0;
            });
            break;
        case 2:
            usort($arrTurResult["Rows"], function ($a, $b) {
                if ($a["HotelRating"]>$b["HotelRating"]) return -1;
                elseif ($a["HotelRating"]<$b["HotelRating"]) return 1;
                return 0;
            });
            break;
    }
    if (count($arrTurResult["Rows"])) {
        foreach ($arrTurResult["Rows"] as $dataTour) {
            $RESULT[$dataTour["HotelId"]][] = $dataTour;
        }
    }
    unset($arrTurResult["Rows"] );
}
if (count($RESULT) > 0):
    $count_page = ceil(count($RESULT) / 20);
    ?>
<?
    foreach ($arrTurResult["OilTaxes"] as $tax){
        $oilTax[$tax["SourceId"]][] = $tax;
    }
    ksort($oilTax);
    ?>
    <div class="section__line section__line_top">
        <div class="section__block">
            <div class="pages">
                <div class="pages__title"><?= count($RESULT) ?> результатов:
                </div>
                <ul class="pages__list">
                    <? if ($count_page <= 5): ?>
                        <li class="pages__item  pages__item_previous  <?if ($request->get("PAGE")==1):?>pages__item_disabled<?endif;?>"><a href="javascript:void(0);" data-page="<?= $request->get("PAGE")-1 ?>"
                                                                                                                                           data-result="<?= $request->get("ID_RESULT") ?>">
                                <svg>
                                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow-small"></use>
                                </svg></a>
                        </li>
                        <? for ($i = 1; $i <= 5; $i++): ?>
                            <li class="pages__item <? if ($i == $request->get("PAGE")): ?>pages__item_current<? endif; ?>">
                                <a href="javascript:void(0);" data-page="<?= $i ?>"
                                   data-result="<?= $request->get("ID_RESULT") ?>"><?= $i ?></a></li>
                        <? endfor; ?>
                        <li class="pages__item pages__item_next <?if ($request->get("PAGE")==$count_page):?>pages__item_disabled<?endif;?>">
                            <a href="javascript:void(0);" data-page="<?= $request->get("PAGE")+1 ?>"                                       data-result="<?= $request->get("ID_RESULT") ?>">
                                <svg>
                                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow-small"></use>
                                </svg>
                            </a>
                        </li>
                    <? elseif (($count_page == 6) || ($request->get("PAGE") < 3) || ($request->get("PAGE") > ($count_page - 2))): ?>
                        <? if ($request->get("PAGE") <= 3): ?>
                            <li class="pages__item  pages__item_previous  <?if ($request->get("PAGE")==1):?>pages__item_disabled<?endif;?>"><a href="javascript:void(0);" data-page="<?= $request->get("PAGE")-1 ?>"
                                                                                                                                               data-result="<?= $request->get("ID_RESULT") ?>">
                                    <svg>
                                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow-small"></use>
                                    </svg></a>
                            </li>
                            <? for ($i = 1; $i <= 3; $i++): ?>
                                <li class="pages__item <? if ($i == $request->get("PAGE")): ?>pages__item_current<? endif; ?>">
                                    <a href="javascript:void(0);" data-page="<?= $i ?>"
                                       data-result="<?= $request->get("ID_RESULT") ?>"><?= $i ?></a></li>
                            <? endfor; ?>
                            <li class="pages__item"><span>...</span></li>
                            <li class="pages__item"><a href="javascript:void(0);" data-page="<?= $count_page ?>"
                                                       data-result="<?= $request->get("ID_RESULT") ?>"><?= $count_page ?></a>
                            </li>
                            <li class="pages__item pages__item_next <?if ($request->get("PAGE")==$count_page):?>pages__item_disabled<?endif;?>"><a href="javascript:void(0);" data-page="<?= $request->get("PAGE")+1 ?>"
                                                                                                                                                   data-result="<?= $request->get("ID_RESULT") ?>">
                                    <svg>
                                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow-small"></use>
                                    </svg></a>
                            </li>
                        <? else: ?>
                            <li class="pages__item  pages__item_previous <?if ($request->get("PAGE")==1):?>pages__item_disabled<?endif;?>"><a href="javascript:void(0);" data-page="<?= $request->get("PAGE")-1 ?>"
                                                                                                                                              data-result="<?= $request->get("ID_RESULT") ?>">
                                    <svg>
                                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow-small"></use>
                                    </svg></a>
                            </li>
                            <li class="pages__item"><a href="javascript:void(0);" data-page="1"
                                                       data-result="<?= $request->get("ID_RESULT") ?>">1</a></li>
                            <li class="pages__item"><span>...</span></li>
                            <? for ($i = $count_page - 2; $i <= $count_page; $i++): ?>
                                <li class="pages__item <? if ($i == $request->get("PAGE")): ?>pages__item_current<? endif; ?>">
                                    <a href="javascript:void(0);" data-page="<?= $i ?>"
                                       data-result="<?= $request->get("ID_RESULT") ?>"><?= $i ?></a></li>
                            <? endfor; ?>
                            <li class="pages__item pages__item_next <?if ($request->get("PAGE")==$count_page):?>pages__item_disabled<?endif;?>">
                                <a href="javascript:void(0);" data-page="<?= $request->get("PAGE")+1 ?>"                                                      data-result="<?= $request->get("ID_RESULT") ?>">
                                    <svg>
                                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow-small"></use>
                                    </svg>
                                </a>
                            </li>

                        <? endif; ?>
                    <? else: ?>
                        <li class="pages__item pages__item_previous  <?if ($request->get("PAGE")==1):?>pages__item_disabled<?endif;?>"><a href="javascript:void(0);" data-page="<?= $request->get("PAGE")-1 ?>"
                                                                                                                                          data-result="<?= $request->get("ID_RESULT") ?>">
                                <svg>
                                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow-small"></use>
                                </svg></a>
                        </li>
                        <li class="pages__item"><a href="javascript:void(0);" data-page="1"
                                                   data-result="<?= $request->get("ID_RESULT") ?>">1</a></li>
                        <li class="pages__item"><span>...</span></li>
                        <? for ($i = $request->get("PAGE") - 1; $i <= $request->get("PAGE") + 1; $i++): ?>
                            <li class="pages__item <? if ($i == $request->get("PAGE")): ?>pages__item_current<? endif; ?>">
                                <a href="javascript:void(0);" data-page="<?= $i ?>"
                                   data-result="<?= $request->get("ID_RESULT") ?>"><?= $i ?></a></li>
                        <? endfor; ?>
                        <li class="pages__item"><span>...</span></li>
                        <li class="pages__item"><a href="javascript:void(0);" data-page="<?= $count_page ?>"
                                                   data-result="<?= $request->get("ID_RESULT") ?>"><?= $count_page ?></a>
                        </li>
                        <li class="pages__item pages__item_next <?if ($request->get("PAGE")==$count_page):?>pages__item_disabled<?endif;?>"><a href="javascript:void(0);" data-page="<?= $request->get("PAGE")+1 ?>"
                                                                                                                                               data-result="<?= $request->get("ID_RESULT") ?>">
                                <svg>
                                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow-small"></use>
                                </svg></a>
                        </li>
                    <? endif; ?>
                </ul>
            </div>
        </div>
        <div class="section__block section__block_sort" data-result="<?= $request->get("ID_RESULT") ?>"><span class="section__caption">Сортировать:</span>
            <div class="select select_field select_inline">
                <select id="type_sort">
                    <option value="0" <?if(TYPE_SORT==0):?> selected <?endif;?>>От дешевых к дорогим</option>
                    <option value="1" <?if(TYPE_SORT==1):?> selected <?endif;?>>От дорогих к дешевым</option>
                    <option value="2" <?if(TYPE_SORT==2):?> selected <?endif;?>>По популярности</option>
                </select>
            </div>
        </div>

    </div>
    <? $showResult = array_slice($RESULT, ($request->get("PAGE") - 1) * 20, 20) ?>
    <?$dataCount=0;?>
    <? foreach ($showResult as $itemTurResult): ?>
    <div class="card card_search" data-more-action="data-more-action">
        <?$dataCount++;?>
        <div class="card__main">
            <div class="card__image">
                <? if (strlen($itemTurResult[0]["HotelTitleImageUrl"]) > 3): ?>
                    <img src="https://hotels.sletat.ru/i/p/<?= $itemTurResult[0]["HotelId"] ?>_0_300_300.jpg"
                         alt="<?= $itemTurResult[0]["HotelName"] ?>"/>
                <? else: ?>
                    <img src="<?= SITE_TEMPLATE_PATH ?>/pictures/no_foto.jpg" alt="Нет фото">
                <? endif; ?>
            </div>
            <div class="card__info">
                <div class="card__info-block card__info-block_info">
                    <div class="card__header">
                        <?$arrayType = array(
                            "HV-1" => "Hotel Vilege 1",
                            "HV-2" => "Hotel Vilege 2",
                            "Apts" => "Аппартаменты",
                            "Villas" => "Вилла"
                        ) ?>
                        <?if (!key_exists($itemTurResult[0]["StarName"],$arrayType)):?>
                        <div class="rating">
                            <ul class="rating__list" data-value="<?= (int)$itemTurResult[0]["StarName"]; ?>">
                                <li class="rating__star">
                                    <svg>
                                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star"></use>
                                    </svg>
                                </li>
                                <li class="rating__star">
                                    <svg>
                                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star"></use>
                                    </svg>
                                </li>
                                <li class="rating__star">
                                    <svg>
                                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star"></use>
                                    </svg>
                                </li>
                                <li class="rating__star">
                                    <svg>
                                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star"></use>
                                    </svg>
                                </li>
                                <li class="rating__star">
                                    <svg>
                                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/img/symbols.svg#svg-ico-star"></use>
                                    </svg>
                                </li>
                            </ul>
                        </div>
                        <?else:?>
                            <span class="card__location"><b><?=$arrayType[$itemTurResult[0]["StarName"]]?></b></span>
                        <?endif;?>
                        <span class="card__location"><?= $itemTurResult[0]["CountryName"] ?>, <?= $itemTurResult[0]["OriginalTownName"] ?></span>
                        <span class="caption caption_size-s"><?= $itemTurResult[0]["HotelName"] ?></span>
                    </div>
                    <ul class="info">
                        <li class="info__cell"><span class="info__label">Вылет из:</span><span
                                    class="info__value"><?= $itemTurResult[0]["CityFromName"] ?></span>
                        </li>
                        <li class="info__cell"><span class="info__label">Гости:</span><span
                                    class="info__value"><? if ($itemTurResult[0]["Adults"] > 0): ?><?= $itemTurResult[0]["Adults"] ?> (взрослых)<? endif; ?><? if ($itemTurResult[0]["Kids"] > 0): ?> + <?= $itemTurResult[0]["Kids"] ?><? if ($itemTurResult[0]["Kids"] == 1): ?> (ребенок)<? else: ?> (детей)<? endif; ?><? endif; ?></span>
                        </li>
                    </ul>
                    <div class="card__text card__text_size-s">
                        <p>Купите <a href="/adress/">Online</a> или в <a href="#">офисе</a></p>
                        <p>Купить тур в <a href="/tury-v-rassrochku/"><strong>рассрочку</strong></a></p>
                    </div>
                </div>
                <div class="card__info-block card__info-block_price">
                    <div class="price price_size-l">
                        <div class="price__value"><span><?=number_format($itemTurResult[0]["NewPrice"],0,"."," ")?>р</span></div>
                        <?if ($itemTurResult[0]["OldPrice"]>0):?><div class="price__value price__value_old"><span><?=number_format($itemTurResult[0]["OldPrice"],0,"."," ")?>р</span></div><?endif;?>
                    </div>
                    <button class="button button_highlight" data-more="data-more" data-toggle="Свернуть">
                        <span>Подробнее</span>
                    </button>
                </div>
            </div>
        </div>

        <ul class="card__details">
            <?$dataCountViwe=0;?>
            <? foreach ($itemTurResult as $itemHotel): ?>
                <?$dataCountViwe++;?>
                <?
                    if($dataCountViwe>3){
                        $dataClass="showMore showHotel".$dataCount;
                    }else{
                        $dataClass="";
                    }
                ?>
                <li class="card__details-item <?=$dataClass;?>">
                    <a class="details"
                       href="/tour/?SourceId=<?= $itemHotel["SourceId"] ?>&OfferId=<?= $itemHotel["OfferId"] ?>&RequestId=<?= $request->get("ID_RESULT") ?>&oldprice=<?=$itemHotel["OldPrice"]?>"><span
                                class="details__info">
                          <ul class="info info_details">
                            <li class="info__cell"><span class="info__label">Даты:</span>
                                <span class="info__value"><?= $itemHotel["CheckInDate"] ?></span>
                                <span class="info__value"> <?= $itemHotel["Nights"] ?> ночей</span>
                            </li>
                            <li class="info__cell"><span class="info__label">Размещение:</span>
                                <span class="info__value"><?= $itemHotel["OriginalMealName"] ?> </span>
                                <span class="info__value"><?= $itemHotel["OriginalHtPlaceName"] ?></span>
                            </li>
                            <li class="info__cell">
                                 <span class="info__label">Авиаперелет:</span>
                                <span class="info__value"><?
                                    switch ($itemHotel["TicketsIncluded"]) {
                                        case "NotIncluded":
                                            echo "авиаперелёт не включён в стоимость тура";
                                            break;
                                        case "Included":
                                            echo "авиаперелёт включён";
                                            break;
                                        case "Unknown":
                                            echo "нет данных";
                                            break;
                                        default:
                                            echo "нет данных";
                                            break;
                                    }
                                    ?>
                                </span>
                                <span class="info__label">Топливный сбор:</span>
                                <span class="info__value">Не включен
                                </span>
                            </li>
                            <li class="info__cell"><span class="info__label">Туроператор:</span><span
                                        class="info__value info__value_image"><img
                                            src="<?= $itemHotel["SourceImageUrl"] ?>"/></span>
                            </li>
                          </ul></span>
                        <span class="details__price">
                                    <span class="price price_details">
                                        <?global $USER?>
                                        <?if ($USER->IsAdmin()):?><div class="helper_price_admin"><div><h3>Цена от sletat.ru: <?=number_format($hotel["Price"], 0, ',', ' ');?>р.</h3><p><?=$hotel["discpuntName"]?></p></div></div><?endif;?>

                                        <?if ($itemHotel["OldPrice"]):?>
                                            <span class="price__value price__value_old"><span><?=number_format($itemHotel["OldPrice"],0,"."," ")?>р</span></span>
                                        <?endif;?>
                                        <span class="price__value">
                                            <span><?=number_format($itemHotel["NewPrice"],0,"."," ")?>р</span>
                                        </span>
                                    </span>
                                </span>
                    </a>
                </li>
            <? endforeach; ?>
            <?if(count($itemTurResult)>3):?>
            <li><a class="card__all button fadeBtn<?=$dataCount?>" onclick="showAllTour(<?=$dataCount?>)" href="#all-tours">Все туры в этот отель</a></li>
            <?endif;?>
        </ul>
    </div>
<? endforeach; ?>
<? endif; ?>
