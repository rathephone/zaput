<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Не найдена страница");

$referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "";
$href = "/";
if($referer){
    $host = $_SERVER['HTTP_HOST'];
    if (stripos($referer, $host)
        && stripos($referer, "yandex") === false
        && stripos($referer, "google") === false)
    {
        $href = $referer;
    }
}
?>
    <style>
        .breadcrumbs, .breadcrumbs + h2.caption  {
            display: none;
        }
    </style>
<div class="section__container container">
    <div class="not-found not-found_oops">
        <div class="not-found__content"><span class="not-found__caption">404</span>
            <div class="not-found__text not-found__text_strong">
                <p>Кажется что то пошло не так!</p>
            </div><a class="button button_highlight button_size-wide" href="<?=$href?>">Вернуться назад</a>
        </div>
    </div>
</div>
<?

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>