<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require_once $_SERVER["DOCUMENT_ROOT"].'/sletat/lib/Autoloader.php';
//инициируем новый объект xml сервиса
$xml = new \sletatru\XmlGate([
    'login' => 'sletat@6468822.ru',
    'password' => 'xml@sletattest2019zp03',
]);


if(!empty($_REQUEST["SourceId"]) && !empty($_REQUEST["OfferId"]) && !empty($_REQUEST["RequestId"])&& empty($_REQUEST["hot"])):
    $SourceId=(int)$_REQUEST["SourceId"];//
    $OfferId=$_REQUEST["OfferId"];//
    $requestId=(int)$_REQUEST["RequestId"];//
    $resToursHotel = $xml->ActualizePrice($SourceId, $OfferId, $requestId);
    $hotel=$resToursHotel["TourInfo"];
    $HotelInfo = $xml->GetHotelInformation($hotel["SysHotelId"]);
    $HotelComment = $xml->GetHotelComments($hotel["SysHotelId"]);

    for($i=0; $i<100; $i++){
        $array_of_tours = $xml->GetRequestState((int)$requestId);
        $flag = false;
        foreach($array_of_tours as $tour){
            if($tour['IsProcessed']){
                $flag = true;
            } else {
                $flag = false;
            }
        }
        if($flag){
            $arrTurResult = $xml->GetRequestResult($requestId);
            break;
        }
        sleep(1.5);
    }
    $arrTur=array();
    foreach ($arrTurResult["Rows"] as $keyTour => $itemTurResult){
        $arrTurResult["Rows"][$keyTour] = GetDiscount($itemTurResult);
        if($hotel["SysHotelId"]==$itemTurResult["HotelId"]){
            $hotelId=$itemTurResult["HotelId"];
            $arrTur[]=$itemTurResult;
        }
    }
    $RandomNumber=$resToursHotel["RandomNumber"];

    $BusinessTicketsDpt=$resToursHotel["TourInfo"]["BusinessTicketsDpt"];
    $BusinessTicketsRtn=$resToursHotel["TourInfo"]["BusinessTicketsRtn"];
    $EconomTicketsDpt=$resToursHotel["TourInfo"]["EconomTicketsDpt"];
    $EconomTicketsRtn=$resToursHotel["TourInfo"]["EconomTicketsRtn"];

    $HotelIsInStop=$resToursHotel["TourInfo"]["HotelIsInStop"];

    $turOperator=$hotel["SourceName"];
endif;
if(!empty($_REQUEST["SourceId"]) && !empty($_REQUEST["OfferId"])&& $_REQUEST["RequestId"]==0 && $_REQUEST["hot"]==1){
    $hotLogin="sletat@6468822.ru";
    $hotPass="xml@sletattest2019zp03";
    $hot=1;
    $SourceId=(int)$_REQUEST["SourceId"];//
    $OfferId=$_REQUEST["OfferId"];//
    $urlHotTur="https://module.sletat.ru/Main.svc/ActualizePrice?requestId=0&offerId=".$OfferId."&sourceId=".$SourceId."&showcase=1&login=".$hotLogin."&password=".$hotPass."";
    $dataHotTur = json_decode(file_get_contents($urlHotTur));
    $dataHotTur=$dataHotTur->ActualizePriceResult->Data;
    $RandomNumber=$dataHotTur->randomNumber;
    $hotelIds=$dataHotTur->data[32];
    $HotelInfo = $xml->GetHotelInformation($hotelIds);
    $HotelComment = $xml->GetHotelComments($hotelIds);
    $hotel["CityFromName"]=$dataHotTur->data[1];
    $hotel["CountryName"]=$dataHotTur->data[0];
    $hotel["ResortName"]=$dataHotTur->data[2];
    $thisTour["CheckInDate"]=$dataHotTur->data[4];
    $thisTour["Nights"]=$dataHotTur->data[5];
    $hotel["CheckIn"]=$dataHotTur->data[4];
    $hotel["CheckOut"]=$dataHotTur->data[10];
    $hotel["Nights"]=$dataHotTur->data[5];

    $hotel["HotelName"]=$dataHotTur->data[6];
    $hotel["SysStarName"]=$dataHotTur->data[8];
    $hotel["Rating"]=$dataHotTur->data[48];
    $hotel["HotelPhotosCount"]=$dataHotTur->data[45];
    $hotel["Adults"]=$dataHotTur->data[53];
    $hotel["Kids"]=$dataHotTur->data[54];
    $hotel["MealName"]=$dataHotTur->data[11];
    $hotel["SysHotelId"]=$dataHotTur->data[32];
    $hotel["HtPlaceName"]=$dataHotTur->data[22];
    $hotel["Price"]=$dataHotTur->data[19];
    $hotel["SysStarName"]=$dataHotTur->data[35];
    $hotel["CityFromId"] = $dataHotTur->data[28];
    $hotel["CounryId"] = $dataHotTur->data[26];
    $hotel["HotelId"] = $dataHotTur->data[32];
    $hotel["SourceId"] = $dataHotTur->data[24];
    $BusinessTicketsDpt=$dataHotTur->data[16];
    $BusinessTicketsRtn=$dataHotTur->data[17];
    $EconomTicketsDpt=$dataHotTur->data[14];
    $EconomTicketsRtn=$dataHotTur->data[15];
    $HotelIsInStop=$dataHotTur->data[13];
}
if ($_REQUEST["oldprice"]>0) $hotel["RawPrice"] = intval($_REQUEST["oldprice"]);
else $hotel["RawPrice"] = $hotel["Price"];
$hotel = GetDiscount($hotel);
$bizTuda="";
switch ($BusinessTicketsDpt){
    case "0":
        $bizTuda="НЕТ";
        break;
    case "1":
        $bizTuda="ЕСТЬ";
        break;
    case "2":
        $bizTuda="По Запросу";
        break;
    case "Stop":
        $bizTuda="НЕТ";
        break;
    case "Available":
        $bizTuda="ЕСТЬ";
        break;
    case "Request":
        $bizTuda="По Запросу";
        break;
    case "Unknown":
        $bizTuda="нет данных";
        break;
    default :
        $bizTuda="нет данных";
        break;
}
$bizOttuda="";
switch ($BusinessTicketsRtn){
    case "0":
        $bizOttuda="НЕТ";
        break;
    case "1":
        $bizOttuda="ЕСТЬ";
        break;
    case "2":
        $bizOttuda="По Запросу";
        break;
    case "Stop":
        $bizOttuda="НЕТ";
        break;
    case "Available":
        $bizOttuda="ЕСТЬ";
        break;
    case "Request":
        $bizOttuda="По Запросу";
        break;
    case "Unknown":
        $bizOttuda="нет данных";
        break;
    default :
        $bizOttuda="нет данных";
        break;
}
$econTuda="";
switch ($EconomTicketsDpt){
    case "0":
        $econTuda="НЕТ";
        break;
    case "1":
        $econTuda="ЕСТЬ";
        break;
    case "2":
        $econTuda="По Запросу";
        break;
    case "Stop":
        $econTuda="НЕТ";
        break;
    case "Available":
        $econTuda="ЕСТЬ";
        break;
    case "Request":
        $econTuda="По Запросу";
        break;
    case "Unknown":
        $econTuda="нет данных";
        break;
    default :
        $econTuda="нет данных";
        break;
}
$econOtTuda="";
switch ($EconomTicketsRtn){
    case "0":
        $econOtTuda="НЕТ";
        break;
    case "1":
        $econOtTuda="ЕСТЬ";
        break;
    case "2":
        $econOtTuda="По Запросу";
        break;
    case "Stop":
        $econOtTuda="НЕТ";
        break;
    case "Available":
        $econOtTuda="ЕСТЬ";
        break;
    case "Request":
        $econOtTuda="По Запросу";
        break;
    case "Unknown":
        $econOtTuda="нет данных";
        break;
    default :
        $econOtTuda="нет данных";
        break;
}
$havePlase="";
switch ($HotelIsInStop){
    case "0":
        $havePlase="НЕТ";
        break;
    case "1":
        $havePlase="ЕСТЬ";
        break;
    case "2":
        $havePlase="По Запросу";
        break;
    case "Stop":
        $havePlase="НЕТ";
        break;
    case "Available":
        $havePlase="ЕСТЬ";
        break;
    case "Request":
        $havePlase="По Запросу";
        break;
    case "Unknown":
        $havePlase="нет данных";
        break;
    default :
        $havePlase="нет данных";
        break;
}
$APPLICATION->SetTitle($hotel["CityFromName"]." - ".$hotel["CountryName"].", ".$hotel["ResortName"]);
?>
    <div class="section__container container">
        <div class="block block_links">
            <ul class="links">
                <li class="links__item links__item_selected"><a href="#description">Описание</a>
                </li>
                <li class="links__item"><a href="#mapSection">Расположение на карте</a>
                </li>
                <li class="links__item"><a href="#information">Информация</a>
                </li>
                <li class="links__item"><a href="#tours">Варианты туров</a>
                </li>
                <li class="links__item"><a href="#reviews">Отзывы</a>
                </li>
            </ul>
        </div>
        <div id="results_tour"></div>
        <div class="card card_hotel">
            <div class="card__header">
                <h2 class="caption">
                    <?=$hotel["HotelName"]?> <?=$hotel["SysStarName"]?>
                </h2>
                <div class="rating rating_trip">
                    <div class="rating__value"><span><?=$hotel["Rating"]?></span>
                    </div>
                    <div class="rating__list" data-value="<?=$hotel["Rating"]/2;?>">
                        <div class="rating__progress">
                        </div>
                        <div class="rating__circle">
                        </div>
                        <div class="rating__circle">
                        </div>
                        <div class="rating__circle">
                        </div>
                        <div class="rating__circle">
                        </div>
                        <div class="rating__circle">
                        </div>
                        <div class="rating__circle">
                        </div>
                        <div class="rating__circle">
                        </div>
                        <div class="rating__circle">
                        </div>
                        <div class="rating__circle">
                        </div>
                        <div class="rating__circle">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card__content">
                <div class="card__block card__block_gallery">
                    <div class="gallery" data-slider="gallery">
                        <div class="gallery__controls" data-slider-controls="data-slider-controls">
                            <button class="arrow arrow_prev" data-slider-controls-prev="data-slider-controls-prev">
                                <svg>
                                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow"></use>
                                </svg>
                            </button>
                            <button class="arrow arrow_next" data-slider-controls-next="data-slider-controls-next">
                                <svg>
                                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow"></use>
                                </svg>
                            </button>
                        </div>
                        <div>
                            <div class="gallery__view" data-modal-open="gallery">
                                <img src="#"/>
                            </div>
                            <div class="gallery__controls" data-slider-main-controls>
                                <button class="arrow arrow_prev" data-slider-controls-prev="data-slider-controls-prev">
                                    <svg>
                                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow"></use>
                                    </svg>
                                </button>
                                <button class="arrow arrow_next" data-slider-controls-next="data-slider-controls-next">
                                    <svg>
                                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-arrow"></use>
                                    </svg>
                                </button>
                            </div>
                        </div>
                        <ul class="gallery__list" data-slider-slides="data-slider-slides">
                            <?
                            if($hotel["HotelPhotosCount"]!=0):
                            for ($i = 0; $i <= $hotel["HotelPhotosCount"]-1; $i++):?>
                                <li class="gallery__item" data-img="https://hotels.sletat.ru/i/f/<?=$hotel["SysHotelId"]?>_<?=$i?>.jpg"><img src="https://hotels.sletat.ru/i/f/<?=$hotel["SysHotelId"]?>_<?=$i?>.jpg"/>
                                </li>
                            <? endfor;?>
                                <? else:?>
                            <li class="gallery__item" data-img="<?=SITE_TEMPLATE_PATH?>/pictures/no_foto.jpg">
                                <img src="<?=SITE_TEMPLATE_PATH?>/pictures/no_foto.jpg" alt="Нет фото">
                            </li>
                            <?endif;?>
                        </ul>
                    </div>
                </div>
                <div class="card__block">
                    <div class="card__block card__block_info">
                        <ul class="info info_horizontal">

                            <?$CheckIn = $DB->FormatDate($hotel["CheckIn"], "DD.MM.YYYY", "DD.MM");?>
                            <?$CheckOut = $DB->FormatDate($hotel["CheckOut"], "DD.MM.YYYY", "DD.MM");?>
                            <li class="info__cell"><span class="info__label">Даты:</span><span class="info__value"><?=$CheckIn?> - <?=$CheckOut?> (<?=$hotel["Nights"]?> ночей)</span>
                            </li>
                            <li class="info__cell"><span class="info__label">Вылет из:</span><span class="info__value"><?=$hotel["CityFromName"]?></span>
                            </li>
                            <li class="info__cell"><span class="info__label">Гости:</span>
                                <span class="info__value">
                                    <?if($hotel["Adults"]!==0):?><?=$hotel["Adults"]?> (взрослых)<? endif;?>
                                    <?if($hotel["Kids"]!==0):?><?=$hotel["Kids"]?> <?if($hotel["Kids"]==1):?>(ребенок)<?else:?>(детей)<? endif;?><? endif;?>
                                </span>
                            </li>
                            <li class="info__cell"><span class="info__label">Питание:</span><span class="info__value"><?=$hotel["MealName"]?></span>
                            </li>
                            <li class="info__cell"><span class="info__label">Тип номера:</span><span class="info__value"><?=$hotel["HtPlaceName"]?></span>
                            </li>
                            <?if ($turOperator):?>
                                <li class="info__cell"><span class="info__label">Туроператор:</span>
                                    <div class="info__value info__value_image"><?=$turOperator?>
                                        <!--<img src="pictures/operators/vit-tour.png"/>-->
                                    </div>
                                </li>
                            <?endif;?>
                        </ul>
                        <ul class="info info_horizontal">
                            <li class="info__cell"><span class="info__label">Авиаперелет:</span>
                                <div class="info__value info__value_image"><!--<img src="pictures/avia/wizz.png"/>-->
                                    <?
                                        switch ($hotel["TicketsIncluded"]){
                                            case "NotIncluded":
                                                echo "авиаперелёт не включён в стоимость тура";
                                                break;
                                            case "Included":
                                                echo "авиаперелёт включён";
                                                break;
                                            case "Unknown":
                                                echo "нет данных";
                                                break;
                                            default:
                                                echo "нет данных";
                                                break;
                                        }
                                    ?>
                                </div>
                            </li>
                            <li class="info__cell"><span class="info__label">Топливный сбор:</span>
                                <span class="info__value">
                                    <?if(false && $resToursHotel["OilTaxes"][0]["Tax"]!=0):?>
                                        Включен
                                    <?else:?>
                                        Не включен
                                    <?endif;?>
                                </span>
                            </li>
                            <li class="info__cell"><span class="info__label">Перелет:</span><span class="info__value info__value_event" data-droping="info">Информация о рейсе
                      <div class="drop drop_info" data-dropped="info">
                        <div class="flight">
                          <ul class="flight__list">
                            <li class="flight__item">
                              <div class="flight__date"><span class="flight__time">22:00</span>
                                <p><strong>Москва</strong> 9 Марта, сб</p>
                              </div>
                              <div class="flight__info">
                                <div class="flight__path"><span class="flight__path-name">Прямой перелет</span>
                                  <div class="flight__path-line">
                                    <div class="flight__path-plain">
                                      <svg>
                                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-plain"></use>
                                      </svg>
                                    </div>
                                    <div class="flight__path-point" data-code="SVO">
                                    </div>
                                    <div class="flight__path-point" data-code="HKT">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="flight__date"><span class="flight__time">11:00</span>
                                <p><strong>Вьетнам</strong> 9 Марта, сб</p>
                              </div>
                            </li>
                            <li class="flight__item">
                              <div class="flight__date"><span class="flight__time">12:00</span>
                                <p><strong>Москва</strong> 9 Марта, сб</p>
                              </div>
                              <div class="flight__info">
                                <div class="flight__path flight__path_back"><span class="flight__path-name">Прямой перелет</span>
                                  <div class="flight__path-line">
                                    <div class="flight__path-plain">
                                      <svg>
                                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-plain"></use>
                                      </svg>
                                    </div>
                                    <div class="flight__path-point" data-code="HKT">
                                    </div>
                                    <div class="flight__path-point" data-code="SVO">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="flight__date"><span class="flight__time">11:00</span>
                                <p><strong>Вьетнам</strong> 9 Марта, сб</p>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div></span>
                            </li>
                        </ul>
                    </div>
                    <div class="card__block">
                        <div class="ticket">
                            <div class="ticket__number"><span>Номер тура: <?=$RandomNumber?></span>
                            </div>
                            <div class="price price_ticket">

                                <div class="price__block">

                                    <?if($hot!=1):?>
                                    <p class="price__text">Общая стоимость тура
                                    </p>
                                    <?else:?>
                                        <p class="price__text">Цена за человека
                                        </p>
                                    <?endif;?>
                                </div>
                                <div class="price__block">
                                    <?if ($hotel["OldPrice"]>0):?>
                                        <div class="price__value price__value_old"><span><?=number_format($hotel["OldPrice"],0,"."," ")?>р.</span></div>
                                    <?endif;?>
                                    <div class="price__value"><span><?=number_format($hotel["NewPrice"],0,"."," ")?>р.</span>
                                        <?global $USER?>
                                        <?if ($USER->IsAdmin()):?><div class="helper_price_admin"><div><h3>Цена от sletat.ru: <?=number_format($hotel["Price"], 0, ',', ' ');?>р.</h3><p><?=$hotel["discpuntName"]?></p></div></div><?endif;?>
                                    </div>
                                </div>
                            </div>
                            <button class="button button_highlight button_full" data-modal-open="order_tour"><span>Оставить заявку</span></button>
                            <?if ($_REQUEST["hot"]!=1):?><form method="get" action="/oplatit-on-layn/">
                                <input name="hot" type="hidden" value="<?=($_REQUEST['hot']==1)?"Y":"N"?>" />
                                <input name="SourceId" type="hidden" value="<?=$_REQUEST["SourceId"]?>" />
                                <input name="OfferId" type="hidden" value="<?=$_REQUEST["OfferId"]?>" />
                                <input name="RequestId" type="hidden" value="<?=$_REQUEST["RequestId"]?>" />
                                <input name="oldprice" type="hidden" value="<?=$_REQUEST["oldprice"]?>" />
                                <button class="button button_full"><span>Купить онлайн</span></button>
                            </form><?endif;?>
                                <div class="ticket__line">
                                <div class="ticket__cell">
                                    <p>Купить тур <a href="/tury-v-rassrochku/"><strong>в рассрочку</strong></a></p>
                                </div>
                                <div class="ticket__cell">
                                    <div class="ticket__logo"><img src="<?php echo SITE_TEMPLATE_PATH ?>/pictures/banks/hcb.png" alt="Home Credit Bank"/>
                                    </div>
                                </div>
                                <div class="ticket__cell">
                                    <div class="ticket__logo"><img src="<?php echo SITE_TEMPLATE_PATH ?>/pictures/banks/russtandart.png" alt="Банк Русский Стандарт"/>
                                    </div>
                                </div>
                            </div>
                            <div class="ticket__line">
                                <p>Купить тур в <a href="/adress/">офисах продаж</a></p>
                            </div>
                        </div>
                    </div>
                    <?if(count($arrTur)>0):?>
                        <a class="button button_highlight button_full button_size-l" href="#tours">Еще <?=count($arrTur);?> вариантов</a>
                    <?endif;?>
                </div>
            </div>
        </div>
        <h3 class="caption caption_padding" id="mapSection">Расположение на карте
        </h3>
        <script src="https://api-maps.yandex.ru/2.1/?apikey=669dcec3-3a8d-4676-ad47-f1c78ae06db0&amp;lang=ru_RU" type="text/javascript"></script>
        <script type="text/javascript">
            ymaps.ready(init);
            function init(){
                var myMap = new ymaps.Map("map", {
                    center: [<?=$HotelInfo["Latitude"]?>, <?=$HotelInfo["Longitude"]?>],
                    zoom: 15,
                    control: []
                });
                var myPlacemark = new ymaps.Placemark(
                    // Координаты метки
                    [<?=$HotelInfo["Latitude"]?>, <?=$HotelInfo["Longitude"]?>]
                );
                myMap.geoObjects.add(myPlacemark);
                myMap.behaviors.disable('scrollZoom');
            }
        </script>
        <div class="map" id="map">
        </div>
        <div class="block block_section block_hotel" id="information">
            <div class="block__header">
                <h3 class="caption caption_block">Информация об отеле
                </h3>
                <ul class="info info_hotel">
                    <li class="info__cell"><span class="info__label">Название:</span>
                        <span class="caption"><?=$hotel["HotelName"]?> <?=$hotel["SysStarName"]?></span>
                        <div class="rating rating_no-more">
                            <ul class="rating__list" data-value="<?=$HotelInfo["Longitude"]?>3">
                                <li class="rating__star">
                                    <svg>
                                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-rating-star"></use>
                                    </svg>
                                </li>
                                <li class="rating__star">
                                    <svg>
                                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-rating-star"></use>
                                    </svg>
                                </li>
                                <li class="rating__star">
                                    <svg>
                                        <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-rating-star"></use>
                                    </svg>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="info__cell"><span class="info__label">Ссылка:</span>
                        <a class="link" href="<?=$HotelInfo["Site"]?>#"><?=$HotelInfo["Site"]?></a>
                    </li>
                    <li class="info__cell">
                        <ul class="info info_horizontal">
                            <li class="info__cell"><span class="info__label">Адрес:</span>
                                <span class="info__value"><?=$HotelInfo["Resort"]?> <?if($HotelInfo["Resort"]!=""):?>,<?endif;?>
                                    <?=$HotelInfo["Street"]?><?if($HotelInfo["Street"]!=""):?>,<?endif;?>
                                    <?=$HotelInfo["HouseNumber"]?></span>
                            </li>
                            <li class="info__cell"><span class="info__label">Телефон:</span><span class="info__value"><?=$HotelInfo["Phone"]?></span>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="block__content">
                <?if(is_array($HotelInfo["HotelFacilities"]) && count($HotelInfo["HotelFacilities"])>0):?>
                    <div class="feautures feautures_quadruple" data-limit="4">
                        <ul class="feautures__list" data-limit-list="data-limit-list">
                            <?foreach ($HotelInfo["HotelFacilities"] as $HotelFacilities):?>
                                <li class="feautures__item">
                                    <div class="feauture feauture_hotel">
                                        <div class="feauture__header">
                                            <div class="feauture__ico">
                                                <svg width="30" height="30">
                                                    <use xlink:href="<?=SITE_TEMPLATE_PATH?>/img/symbols.svg#svg-ico-feauture-wifi"></use>
                                                </svg>
                                            </div><span class="feauture__caption"><?=$HotelFacilities['Name']?></span>
                                        </div>
                                        <?foreach ($HotelFacilities['Facilities'] as $Facilities):?>
                                            <div class="feauture__content">
                                                <p><?=$Facilities["Name"]?></p>
                                            </div>
                                        <?endforeach;?>
                                    </div>
                                </li>
                            <?endforeach;?>
                        </ul>
                        <?if(count($HotelInfo["HotelFacilities"])>4):?>
                            <div class="feautures__more" data-limit-disable="data-limit-disable">
                                <button class="button button_transparent-dark button_size-wide"><span>Подробнее</span>
                                </button>
                            </div>
                        <?endif;?>
                    </div>
                <?endif;?>
                <div class="content content_description" id="description">
                    <?//$HotelInfo["Description"]?>
                </div>
                <script>
                    $.ajax({
                        url: "/include/load_desc_hotel.php",
                        data: {id: <?=$HotelInfo["HotelId"]?>},
                        dataType: "html",
                        success: function (data) {
                            $("#description").html(data);
                        }
                    })
                </script>
            </div>
        </div>
        <?$APPLICATION->IncludeFile('/include/form_search.php',array(),array());?>
        <h3 class="caption caption_padding" id="tours">Все варианты туров в отель <?=$hotel["HotelName"]?>
        </h3>
        <div class="block block_content">
            <?if(count($arrTur)>0):?>
                <ul class="items">
                    <?$fCount=0?>
                    <?foreach ($arrTur as $thisTour):?>
                        <?$fCount++;?>
                        <li class="details details_offer <?if($fCount>3):?> details_offer_none <?endif;?>">
                            <div class="details__info">
                                <ul class="info info_details">
                                    <li class="info__cell"><span class="info__label">Даты:</span>
                                        <span class="info__value"><?=$thisTour["CheckInDate"]?></span>
                                        <span class="info__value"> <?=$thisTour["Nights"]?> ночей</span>
                                    </li>
                                    <li class="info__cell"><span class="info__label">Размещение:</span><span class="info__value"><?=$thisTour["OriginalMealName"]?></span>
                                        <span class="info__value"><?=$thisTour["OriginalMealName"]?> + <?=$thisTour["OriginalHtPlaceName"]?> </span>
                                    </li>
                                    <!--<li class="info__cell"><span class="info__label">Авиакомпания:</span><span class="info__value info__value_image"> <img src="/pictures/avia/aviaflot.png"/></span><span class="info__value">Топливный сбор включен</span>
                                    </li>-->
                                    <li class="info__cell">
                                        <span class="info__label">Авиаперелет:</span>
                                        <span class="info__value"><?
                                            switch ($thisTour["TicketsIncluded"]){
                                                case "NotIncluded":
                                                    echo "авиаперелёт не включён в стоимость тура";
                                                    break;
                                                case "Included":
                                                    echo "авиаперелёт включён";
                                                    break;
                                                case "Unknown":
                                                    echo "нет данных";
                                                    break;
                                                default:
                                                    echo "нет данных";
                                                    break;
                                            }
                                            ?></span>
                                        <!--<span class="info__value">Топливный сбор включен</span>-->
                                    </li>
                                    <li class="info__cell">
                                        <span class="info__label">Туроператор:</span>
                                        <span class="info__value info__value_image"><img src="<?=$thisTour["SourceImageUrl"]?>"/></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="details__price">
                                <div class="price price_details">
                                    <div class="price__value"><span><?=$thisTour["Price"]?>р</span></div>
                                </div><a class="button button_full" href="/tour/?SourceId=<?=$thisTour["SourceId"]?>&OfferId=<?=$thisTour["OfferId"]?>&RequestId=<?=$requestId?>"><span>Подробнее</span></a>
                            </div>
                        </li>
                    <?endforeach;?>
                </ul>
            <?endif;?>
        </div>
        <?if(count($arrTur)>3):?>
        <div class="section__footer"><a class="button button_highlight button_size-wide-l showAllTour" onclick="showAllTour()"><span>Показать все <?=count($arrTur);?></span></a>
        </div>
        <? endif;?>
        <div class="block block_reviews" id="reviews">
            <h3 class="caption">Отзывы
            </h3>
            <div class="reviews" data-limit="3">
                <ul class="reviews__list" data-limit-list="data-limit-list">
                    <?if(!is_array($HotelComment) || count($HotelComment)==0):?>
                        <li class="reviews__item">
                            <div class="review">
                                <p>Отзывов нет.</p>
                            </div>
                        </li>
                    <?else:?>
                        <?foreach ($HotelComment as $itemHotel):?>
                            <li class="reviews__item">
                                <div class="review">
                                    <div class="review__header">
                                        <div class="review__rating"><span><?=$itemHotel["Rate"]?></span>
                                        </div>
                                        <div class="review__author"><span><?=$itemHotel["UserName"]?></span>
                                        </div>
                                        <div class="review__date"><span><?=$itemHotel["CreateDateFormatted"]?></span>
                                        </div>
                                    </div>
                                    <div class="review__content">
                                        <div class="review__line">
                                            <div class="review__block review__block_title"><span>Достоинства:</span>
                                            </div>
                                            <div class="review__block review__block_text">
                                                <p><?=$itemHotel["Positive"]?></p>
                                            </div>
                                        </div>
                                        <div class="review__line">
                                            <div class="review__block review__block_title"><span>Недостатки:</span>
                                            </div>
                                            <div class="review__block review__block_text">
                                                <p><?=$itemHotel["Negative"]?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <? endforeach;?>
                    <?endif;?>
                </ul>
                <?if(count($HotelComment)>3):?>
                    <button class="button button_highlight" data-limit-disable="data-limit-disable"><span>Показать еще <?=count($HotelComment)-3;?> отзыва</span>
                    </button>
                <? endif;?>
            </div>
        </div>
    </div>

    <div class="page__modals">
        <div class="modal modal_size-s modal__feedback" data-modal="order_tour">
        <button class="modal__close">
        </button>
        <header class="modal__head"><span class="caption caption_size-l">Оставить заявку</span>
        </header>
        <div class="modal__content">
            <form class="form form_modal form__order_zakaz">
                <?=bitrix_sessid_post();?>
                <div class="form__feedback-error field__caption"></div>
                <div class="form__line">
                    <label class="field field_size-full"><span class="field__caption">Ваше имя</span>
                        <div class="field__input"><input type="text" placeholder="Иванов Иван Иванович" name="NAME" required/>
                        </div>
                    </label>
                </div>
                <div class="form__line">
                    <label class="field field_size-full"><span class="field__caption">Ваш e-mail</span>
                        <div class="field__input"><input type="email" placeholder="example@mail.com" name="EMAIL"/>
                        </div>
                    </label>
                </div>
                <div class="form__line">
                    <label class="field field_size-full"><span class="field__caption">Контактный телефон</span>
                        <div class="field__input"><input type="tel" placeholder="+7 ( ___ ) - ___ - __ - __" data-mask="+7 (###) ###-####" name="PHONE" required/>
                        </div>
                    </label>
                </div>
                <div class="hidden_inputs">
                    <?
                    $ticketInlud="";
                    switch ($hotel["TicketsIncluded"]){
                        case "NotIncluded":
                            $ticketInlud="НЕТ";
                            break;
                        case "Included":
                            $ticketInlud="ДА";
                            break;
                        case "Unknown":
                            $ticketInlud="нет данных";
                            break;
                        default:
                            $ticketInlud="нет данных";
                            break;
                    }
                    ?>

                    <?
                        $turDescript="Тур на Слетать.ру:  \r\n";
                        $turDescript.="Туроператор: ".$turOperator." \r\n";
                        $turDescript.="Вылет из: ".$hotel["CityFromName"]." \r\n";
                        $turDescript.="Направление: ".$hotel["CountryName"]." \r\n";
                        $turDescript.="Курорт/регион: ".$hotel["ResortName"]." \r\n";
                        $turDescript.="Отель: ".$hotel["HotelName"]." \r\n";
                        $turDescript.="Питание: ".$hotel["MealName"]." \r\n";
                        $turDescript.="Размещение:  ".$hotel["HtPlaceName"]." \r\n";
                        $turDescript.="Номер:  \r\n";
                        $turDescript.="Дата заезда: ".$hotel["CheckIn"]." \r\n";
                        $turDescript.="Ночей в туре: ".$hotel["Nights"]." \r\n";
                        $turDescript.="Конечная цена: ".$hotel["Price"]." \r\n";
                        $turDescript.="Наличие мест в отеле: ".$havePlase." \r\n";
                        $turDescript.="Цена билетов включена в цену тура: ".$ticketInlud." \r\n";
                        $turDescript.="Наличие билетов эконом класса туда/обратно: ".$bizTuda." / ".$bizOttuda." \r\n";
                        $turDescript.="Наличие билетов бизнес класса туда/обратно: ".$econTuda." / ".$econOtTuda." \r\n";
                        $turDescript.="Взрослых: ".$hotel["Adults"]." \r\n";
                        $turDescript.="Детей: ".$hotel["Kids"]." \r\n";
                        $turDescript.="Номер тура: ".$RandomNumber." \r\n";
                    ?>
                    <input type="hidden" name="turDeskript" value="<?=$turDescript?>"><!--tur -->
                </div>
                <div class="form__text">
                    <p>
                        Отправка заявки ни к чему не обязывает и не является бронированием.
                        Получив заявку, менеджер свяжется с Вами для уточнения деталей
                    </p>
                </div>
                <div class="form__submit">
                    <button type="submit" class="button button_highlight button_full form__order_zakaz-submit"><span>Отправить заявку</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
    </div>
    <script>
        function showAllTour() {
            //$(".details_offer_none").toggle();
            $(".details_offer_none").toggleClass("details_offer_flex");
            var textr=$(".showAllTour").text();
            if(textr=="Скрыть"){
                $(".showAllTour").text("Показать все");
            }else{
                $(".showAllTour").text("Скрыть");
            }

        }
        $( document ).ready(function() {
            /*$.ajax({
                url: "test.html",
                cache: false
            }).done(function( html ) {
                $("#results_tour").append(html);
            });*/
        });
    </script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>