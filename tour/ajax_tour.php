<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require_once $_SERVER["DOCUMENT_ROOT"].'/sletat/lib/Autoloader.php';
//инициируем новый объект xml сервиса
$xml = new \sletatru\XmlGate([
    'login' => 'sletat@6468822.ru',
    'password' => 'xml@sletattest2019zp03',
]);
if(!empty($_REQUEST["SourceId"]) && !empty($_REQUEST["OfferId"]) && !empty($_REQUEST["requestId"])):
    $SourceId=(int)$_REQUEST["SourceId"];//
    $OfferId=$_REQUEST["OfferId"];//
    $requestId=(int)$_REQUEST["requestId"];//
    $resToursHotel = $xml->ActualizePrice($SourceId, $OfferId, $requestId);
    $hotel=$resToursHotel["TourInfo"];
    $HotelInfo = $xml->GetHotelInformation($hotel["SysHotelId"]);
    $HotelComment = $xml->GetHotelComments($hotel["SysHotelId"]);


    for($i=0; $i<100; $i++){

        $array_of_tours = $xml->GetRequestState((int)$requestId);
        $flag = false;
        foreach($array_of_tours as $tour){
            if($tour['IsProcessed']){
                $flag = true;
            } else {
                $flag = false;
            }
        }
        if($flag){
            $arrTurResult = $xml->GetRequestResult($requestId);
            break;
        }
        sleep(1.5);
    }
    $arrTur=array();
    foreach ($arrTurResult["Rows"] as $itemTurResult){
        if($hotel["SysHotelId"]==$itemTurResult["HotelId"]){
            $hotelId=$itemTurResult["HotelId"];
            $arrTur[]=$itemTurResult;
        }
    }
    $RandomNumber=$resToursHotel["RandomNumber"];

endif;
?>

<div class="card card_hotel">
    <div class="card__header">
        <h2 class="caption">
            <?=$hotel["HotelName"]?> <?=$hotel["SysStarName"]?>
        </h2>
        <div class="rating rating_trip">
            <div class="rating__value"><span><?=$hotel["Rating"]?></span>
            </div>
            <div class="rating__list" data-value="<?=$hotel["Rating"]/2;?>">
                <div class="rating__progress">
                </div>
                <div class="rating__circle">
                </div>
                <div class="rating__circle">
                </div>
                <div class="rating__circle">
                </div>
                <div class="rating__circle">
                </div>
                <div class="rating__circle">
                </div>
                <div class="rating__circle">
                </div>
                <div class="rating__circle">
                </div>
                <div class="rating__circle">
                </div>
                <div class="rating__circle">
                </div>
                <div class="rating__circle">
                </div>
            </div>
        </div>
    </div>
    <div class="card__content">
        <div class="card__block card__block_gallery">
            <div class="gallery" data-slider="gallery">
                <div class="gallery__controls" data-slider-controls="data-slider-controls">
                    <button class="arrow arrow_prev" data-slider-controls-prev="data-slider-controls-prev">
                        <svg>
                            <use xlink:href="img/symbols.svg#svg-ico-arrow"></use>
                        </svg>
                    </button>
                    <button class="arrow arrow_next" data-slider-controls-next="data-slider-controls-next">
                        <svg>
                            <use xlink:href="img/symbols.svg#svg-ico-arrow"></use>
                        </svg>
                    </button>
                </div>
                <div class="gallery__view" data-modal-open="gallery"><img src="#"/>
                </div>
                <ul class="gallery__list" data-slider-slides="data-slider-slides">
                    <?
                    for ($i = 0; $i <= $hotel["HotelPhotosCount"]-1; $i++):?>
                        <li class="gallery__item" data-img="https://hotels.sletat.ru/i/f/<?=$hotel["SysHotelId"]?>_<?=$i?>.jpg"><img src="https://hotels.sletat.ru/i/f/<?=$hotel["SysHotelId"]?>_<?=$i?>.jpg"/>
                        </li>
                    <? endfor;?>
                </ul>
            </div>
        </div>
        <div class="card__block">
            <div class="card__block card__block_info">
                <ul class="info info_horizontal">
                    <?$CheckIn = $DB->FormatDate($hotel["CheckIn"], "DD.MM.YYYY", "DD.MM");?>
                    <?$CheckOut = $DB->FormatDate($hotel["CheckOut"], "DD.MM.YYYY", "DD.MM");?>
                    <li class="info__cell"><span class="info__label">Даты:</span><span class="info__value"><?=$CheckIn?> - <?=$CheckOut?> (<?=$hotel["Nights"]?> ночей)</span>
                    </li>
                    <li class="info__cell"><span class="info__label">Вылет из:</span><span class="info__value"><?=$hotel["CityFromName"]?></span>
                    </li>
                    <li class="info__cell"><span class="info__label">Гости:</span>
                        <span class="info__value">
                                    <?if($hotel["Adults"]!==0):?><?=$hotel["Adults"]?> (взрослых)<? endif;?>
                            <?if($hotel["Kids"]!==0):?><?=$hotel["Kids"]?> <?if($hotel["Kids"]==1):?>(ребенок)<?else:?>(детей)<? endif;?><? endif;?>
                                </span>
                    </li>
                    <li class="info__cell"><span class="info__label">Питание:</span><span class="info__value"><?=$hotel["MealName"]?></span>
                    </li>
                    <li class="info__cell"><span class="info__label">Тип номера:</span><span class="info__value"><?=$hotel["HtPlaceName"]?></span>
                    </li>
                    <li class="info__cell"><span class="info__label">Туроператор:</span>
                        <div class="info__value info__value_image"><img src="pictures/operators/vit-tour.png"/>
                        </div>
                    </li>
                </ul>
                <ul class="info info_horizontal">
                    <li class="info__cell"><span class="info__label">Авиакомпания:</span>
                        <div class="info__value info__value_image"><img src="pictures/avia/wizz.png"/>
                        </div>
                    </li>
                    <li class="info__cell"><span class="info__label">Топливный сбор:</span><span class="info__value">Не включен</span>
                    </li>
                    <li class="info__cell"><span class="info__label">Перелет:</span><span class="info__value info__value_event" data-droping="info">Информация о рейсе
                      <div class="drop drop_info" data-dropped="info">
                        <div class="flight">
                          <ul class="flight__list">
                            <li class="flight__item">
                              <div class="flight__date"><span class="flight__time">22:00</span>
                                <p><strong>Москва</strong> 9 Марта, сб</p>
                              </div>
                              <div class="flight__info">
                                <div class="flight__path"><span class="flight__path-name">Прямой перелет</span>
                                  <div class="flight__path-line">
                                    <div class="flight__path-plain">
                                      <svg>
                                        <use xlink:href="img/symbols.svg#svg-ico-plain"></use>
                                      </svg>
                                    </div>
                                    <div class="flight__path-point" data-code="SVO">
                                    </div>
                                    <div class="flight__path-point" data-code="HKT">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="flight__date"><span class="flight__time">11:00</span>
                                <p><strong>Вьетнам</strong> 9 Марта, сб</p>
                              </div>
                            </li>
                            <li class="flight__item">
                              <div class="flight__date"><span class="flight__time">12:00</span>
                                <p><strong>Москва</strong> 9 Марта, сб</p>
                              </div>
                              <div class="flight__info">
                                <div class="flight__path flight__path_back"><span class="flight__path-name">Прямой перелет</span>
                                  <div class="flight__path-line">
                                    <div class="flight__path-plain">
                                      <svg>
                                        <use xlink:href="img/symbols.svg#svg-ico-plain"></use>
                                      </svg>
                                    </div>
                                    <div class="flight__path-point" data-code="HKT">
                                    </div>
                                    <div class="flight__path-point" data-code="SVO">
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="flight__date"><span class="flight__time">11:00</span>
                                <p><strong>Вьетнам</strong> 9 Марта, сб</p>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div></span>
                    </li>
                </ul>
            </div>
            <div class="card__block">
                <div class="ticket">
                    <div class="ticket__number"><span>Номер тура: <?=$RandomNumber?></span>
                    </div>
                    <div class="price price_ticket">
                        <div class="price__block">
                            <p class="price__text">Общая стоимость тура
                            </p>
                        </div>
                        <div class="price__block">
                            <div class="price__value price__value_old"><span> 14 208р</span>
                            </div>
                            <div class="price__value"><span><?=$hotel["Price"]?>р.</span>
                            </div>
                        </div>
                    </div>
                    <button class="button button_highlight button_full" data-modal-open="order"><span>Оставить заявку</span>
                    </button>
                    <button class="button button_full"><span>Купить онлайн</span>
                    </button>
                    <div class="ticket__line">
                        <div class="ticket__cell">
                            <p>Купить тур <strong>в рассрочку</strong></p>
                        </div>
                        <div class="ticket__cell">
                            <div class="ticket__logo"><img src="pictures/banks/hcb.png" alt="Home Credit Bank"/>
                            </div>
                        </div>
                        <div class="ticket__cell">
                            <div class="ticket__logo"><img src="pictures/banks/russtandart.png" alt="Банк Русский Стандарт"/>
                            </div>
                        </div>
                    </div>
                    <div class="ticket__line">
                        <p>Купить тур в <a href="#">офисах продаж</a></p>
                    </div>
                </div>
            </div>
            <?if(count($arrTur)>0):?>
                <a class="button button_highlight button_full button_size-l" href="#tours">Еще <?=count($arrTur);?> вариантов</a>
            <?endif;?>
        </div>
    </div>
</div>
<h3 class="caption caption_padding" id="mapSection">Расположение на карте
</h3>
<script type="text/javascript">
    ymaps.ready(init);
    function init(){
        var myMap = new ymaps.Map("map", {
            center: [<?=$HotelInfo["Latitude"]?>, <?=$HotelInfo["Longitude"]?>],
            zoom: 15,
            control: []
        });
        var myPlacemark = new ymaps.Placemark(
            // Координаты метки
            [<?=$HotelInfo["Latitude"]?>, <?=$HotelInfo["Longitude"]?>]
        );
        myMap.geoObjects.add(myPlacemark);
        myMap.behaviors.disable('scrollZoom');
    }
</script>
<div class="map" id="map">
</div>
<div class="block block_section block_hotel" id="information">
    <div class="block__header">
        <h3 class="caption caption_block">Информация об отеле
        </h3>
        <ul class="info info_hotel">
            <li class="info__cell"><span class="info__label">Название:</span>
                <span class="caption"><?=$hotel["HotelName"]?> <?=$hotel["SysStarName"]?></span>
                <div class="rating rating_no-more">
                    <ul class="rating__list" data-value="<?=$HotelInfo["Longitude"]?>3">
                        <li class="rating__star">
                            <svg>
                                <use xlink:href="img/symbols.svg#svg-ico-rating-star"></use>
                            </svg>
                        </li>
                        <li class="rating__star">
                            <svg>
                                <use xlink:href="img/symbols.svg#svg-ico-rating-star"></use>
                            </svg>
                        </li>
                        <li class="rating__star">
                            <svg>
                                <use xlink:href="img/symbols.svg#svg-ico-rating-star"></use>
                            </svg>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="info__cell"><span class="info__label">Ссылка:</span>
                <a class="link" href="<?=$HotelInfo["Site"]?>#"><?=$HotelInfo["Site"]?></a>
            </li>
            <li class="info__cell">
                <ul class="info info_horizontal">
                    <li class="info__cell"><span class="info__label">Адрес:</span>
                        <span class="info__value"><?=$HotelInfo["Resort"]?> <?if($HotelInfo["Resort"]!=""):?>,<?endif;?>
                            <?=$HotelInfo["Street"]?><?if($HotelInfo["Street"]!=""):?>,<?endif;?>
                            <?=$HotelInfo["HouseNumber"]?></span>
                    </li>
                    <li class="info__cell"><span class="info__label">Телефон:</span><span class="info__value"><?=$HotelInfo["Phone"]?></span>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="block__content">
        <?if(!empty($HotelInfo["HotelFacilities"])):?>
            <div class="feautures feautures_quadruple" data-limit="4">
                <ul class="feautures__list" data-limit-list="data-limit-list">
                    <?foreach ($HotelInfo["HotelFacilities"] as $HotelFacilities):?>
                        <li class="feautures__item">
                            <div class="feauture feauture_hotel">
                                <div class="feauture__header">
                                    <div class="feauture__ico">
                                        <svg width="30" height="30">
                                            <use xlink:href="img/symbols.svg#svg-ico-feauture-wifi"></use>
                                        </svg>
                                    </div><span class="feauture__caption"><?=$HotelFacilities['Name']?></span>
                                </div>
                                <?foreach ($HotelFacilities['Facilities'] as $Facilities):?>
                                    <div class="feauture__content">
                                        <p><?=$Facilities["Name"]?></p>
                                    </div>
                                <?endforeach;?>
                            </div>
                        </li>
                    <?endforeach;?>
                </ul>
                <?if(count($HotelInfo["HotelFacilities"])>4):?>
                    <div class="feautures__more" data-limit-disable="data-limit-disable">
                        <button class="button button_transparent-dark button_size-wide"><span>Подробнее</span>
                        </button>
                    </div>
                <?endif;?>
            </div>
        <?endif;?>
        <div class="content content_description" id="description">
            <?=$HotelInfo["Description"]?>
        </div>
    </div>
</div>
<?$APPLICATION->IncludeFile('/include/form_search.php',array(),array());?>
<h3 class="caption caption_padding" id="tours">Все варианты туров в отель Galaxy 3 Hotel 3*
</h3>
<div class="block block_content">
    <?if(count($arrTur)>0):?>
        <ul class="items">
            <?foreach ($arrTur as $thisTour):?>
                <li class="details details_offer">
                    <div class="details__info">
                        <ul class="info info_details">
                            <li class="info__cell"><span class="info__label">Даты:</span>
                                <span class="info__value"><?=$thisTour["CheckInDate"]?></span>
                                <span class="info__value"> <?=$thisTour["Nights"]?> ночей</span>
                            </li>
                            <li class="info__cell"><span class="info__label">Размещение:</span><span class="info__value"><?=$thisTour["OriginalMealName"]?></span>
                                <span class="info__value"><?=$thisTour["OriginalMealName"]?> + <?=$thisTour["OriginalHtPlaceName"]?> </span>
                            </li>
                            <li class="info__cell"><span class="info__label">Авиакомпания:</span><span class="info__value info__value_image"> <img src="/pictures/avia/aviaflot.png"/></span><span class="info__value">Топливный сбор включен</span>
                            </li>
                            <li class="info__cell"><span class="info__label">Туроператор:</span><span class="info__value info__value_image"><img src="/pictures/operators/biblio.png"/></span>
                            </li>
                        </ul>
                    </div>
                    <div class="details__price">
                        <div class="price price_details">
                            <div class="price__value"><span><?=$thisTour["Price"]?>р</span>
                            </div>
                        </div><a class="button button_full" href="/tour/?SourceId=<?=$thisTour["SourceId"]?>&OfferId=<?=$thisTour["OfferId"]?>&requestId=<?=$requestId?>"><span>Подробнее</span></a>
                    </div>
                </li>
            <?endforeach;?>
        </ul>
    <?endif;?>
</div>
<div class="section__footer"><a class="button button_highlight button_size-wide-l"><span>Показать все 18</span></a>
</div>
<div class="block block_reviews" id="reviews">
    <h3 class="caption">Отзывы
    </h3>
    <div class="reviews" data-limit="3">
        <ul class="reviews__list" data-limit-list="data-limit-list">
            <?if(empty($HotelComment)):?>
                <li class="reviews__item">
                    <div class="review">
                        <p>Отзывов нет.</p>
                    </div>
                </li>
            <?else:?>
                <?foreach ($HotelComment as $itemHotel):?>
                    <li class="reviews__item">
                        <div class="review">
                            <div class="review__header">
                                <div class="review__rating"><span><?=$itemHotel["Rate"]?></span>
                                </div>
                                <div class="review__author"><span><?=$itemHotel["UserName"]?></span>
                                </div>
                                <div class="review__date"><span><?=$itemHotel["CreateDateFormatted"]?></span>
                                </div>
                            </div>
                            <div class="review__content">
                                <div class="review__line">
                                    <div class="review__block review__block_title"><span>Достоинства:</span>
                                    </div>
                                    <div class="review__block review__block_text">
                                        <p><?=$itemHotel["Positive"]?></p>
                                    </div>
                                </div>
                                <div class="review__line">
                                    <div class="review__block review__block_title"><span>Недостатки:</span>
                                    </div>
                                    <div class="review__block review__block_text">
                                        <p><?=$itemHotel["Negative"]?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                <? endforeach;?>
            <?endif;?>
        </ul>
        <?if(count($HotelComment)>3):?>
            <button class="button button_highlight" data-limit-disable="data-limit-disable"><span>Показать еще <?=count($HotelComment)-3;?> отзыва</span>
            </button>
        <? endif;?>
    </div>
</div>